/* tslint:disable */

declare var Object: any;
export interface JobInterface {
  "title"?: string;
  "weight"?: string;
  "source"?: any;
  "destination"?: any;
  "valueOfLoad"?: number;
  "isApproved"?: boolean;
  "budget"?: number;
  "netPrice"?: number;
  "typeOfVehicle"?: string;
  "date"?: Date;
  "description"?: string;
  "didCustGiveRate"?: boolean;
  "didDriveGiveRate"?: boolean;
  "natureOfGoods"?: string;
  "status"?: string;
  "jobApproveStatus"?: string;
  "selectedRoute"?: string;
  "requiredType"?: string;
  "id"?: any;
  "creatorId"?: any;
  "ownerId"?: any;
  "driverId"?: any;
  "vehicleTypeId"?: any;
  creator?: any;
  owner?: any;
  driver?: any;
  vehicleType?: any;
  jobApplies?: any[];
}

export class Job implements JobInterface {
  "title": string;
  "weight": string;
  "source": any;
  "destination": any;
  "valueOfLoad": number;
  "isApproved": boolean;
  "budget": number;
  "netPrice": number;
  "typeOfVehicle": string;
  "date": Date;
  "description": string;
  "didCustGiveRate": boolean;
  "didDriveGiveRate": boolean;
  "natureOfGoods": string;
  "status": string;
  "jobApproveStatus": string;
  "selectedRoute": string;
  "requiredType": string;
  "id": any;
  "creatorId": any;
  "ownerId": any;
  "driverId": any;
  "vehicleTypeId": any;
  creator: any;
  owner: any;
  driver: any;
  vehicleType: any;
  jobApplies: any[];
  constructor(data?: JobInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Job`.
   */
  public static getModelName() {
    return "Job";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Job for dynamic purposes.
  **/
  public static factory(data: JobInterface): Job{
    return new Job(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Job',
      plural: 'Jobs',
      path: 'Jobs',
      idName: 'id',
      properties: {
        "title": {
          name: 'title',
          type: 'string'
        },
        "weight": {
          name: 'weight',
          type: 'string'
        },
        "source": {
          name: 'source',
          type: 'any'
        },
        "destination": {
          name: 'destination',
          type: 'any'
        },
        "valueOfLoad": {
          name: 'valueOfLoad',
          type: 'number'
        },
        "isApproved": {
          name: 'isApproved',
          type: 'boolean',
          default: false
        },
        "budget": {
          name: 'budget',
          type: 'number'
        },
        "netPrice": {
          name: 'netPrice',
          type: 'number'
        },
        "typeOfVehicle": {
          name: 'typeOfVehicle',
          type: 'string'
        },
        "date": {
          name: 'date',
          type: 'Date'
        },
        "description": {
          name: 'description',
          type: 'string'
        },
        "didCustGiveRate": {
          name: 'didCustGiveRate',
          type: 'boolean',
          default: false
        },
        "didDriveGiveRate": {
          name: 'didDriveGiveRate',
          type: 'boolean',
          default: false
        },
        "natureOfGoods": {
          name: 'natureOfGoods',
          type: 'string'
        },
        "status": {
          name: 'status',
          type: 'string'
        },
        "jobApproveStatus": {
          name: 'jobApproveStatus',
          type: 'string'
        },
        "selectedRoute": {
          name: 'selectedRoute',
          type: 'string'
        },
        "requiredType": {
          name: 'requiredType',
          type: 'string',
          default: 'Vehicle'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "creatorId": {
          name: 'creatorId',
          type: 'any'
        },
        "ownerId": {
          name: 'ownerId',
          type: 'any'
        },
        "driverId": {
          name: 'driverId',
          type: 'any'
        },
        "vehicleTypeId": {
          name: 'vehicleTypeId',
          type: 'any'
        },
      },
      relations: {
        creator: {
          name: 'creator',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'creatorId',
          keyTo: 'id'
        },
        owner: {
          name: 'owner',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'ownerId',
          keyTo: 'id'
        },
        driver: {
          name: 'driver',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'driverId',
          keyTo: 'id'
        },
        vehicleType: {
          name: 'vehicleType',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'vehicleTypeId',
          keyTo: 'id'
        },
        jobApplies: {
          name: 'jobApplies',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'jobId'
        },
      }
    }
  }
}

/* tslint:disable */

declare var Object: any;
export interface JobApplyInterface {
  "id"?: any;
  "jobId"?: any;
  "peopleId"?: any;
  job?: any;
  people?: any;
}

export class JobApply implements JobApplyInterface {
  "id": any;
  "jobId": any;
  "peopleId": any;
  job: any;
  people: any;
  constructor(data?: JobApplyInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `JobApply`.
   */
  public static getModelName() {
    return "JobApply";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of JobApply for dynamic purposes.
  **/
  public static factory(data: JobApplyInterface): JobApply{
    return new JobApply(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'JobApply',
      plural: 'JobApplies',
      path: 'JobApplies',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "jobId": {
          name: 'jobId',
          type: 'any'
        },
        "peopleId": {
          name: 'peopleId',
          type: 'any'
        },
      },
      relations: {
        job: {
          name: 'job',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'jobId',
          keyTo: 'id'
        },
        people: {
          name: 'people',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'peopleId',
          keyTo: 'id'
        },
      }
    }
  }
}

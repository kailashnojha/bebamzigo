/* tslint:disable */

declare var Object: any;
export interface PeopleInterface {
  "realm"?: string;
  "isProfileComplete"?: boolean;
  "username"?: string;
  "email"?: string;
  "companyName"?: string;
  "emailVerified"?: boolean;
  "licenceIds"?: Array<any>;
  "governmentIds"?: Array<any>;
  "image"?: Array<any>;
  "createdAt"?: Date;
  "updatedAt"?: Date;
  "profileImage"?: string;
  "isVehicleAlloted"?: boolean;
  "adminVerifiedStatus"?: string;
  "newNotification"?: number;
  "reason"?: string;
  "rating"?: any;
  "mobOtp"?: any;
  "id"?: any;
  "vehicleTypeIds"?: Array<any>;
  "password"?: string;
  accessTokens?: any[];
  vehicles?: any[];
  vehicleType?: any[];
  job?: any[];
  requestfordrivers?: any[];
  roleMappings?: any[];
  identities?: any[];
  credentials?: any[];
}

export class People implements PeopleInterface {
  "realm": string;
  "isProfileComplete": boolean;
  "username": string;
  "email": string;
  "companyName": string;
  "emailVerified": boolean;
  "licenceIds": Array<any>;
  "governmentIds": Array<any>;
  "image": Array<any>;
  "createdAt": Date;
  "updatedAt": Date;
  "profileImage": string;
  "isVehicleAlloted": boolean;
  "adminVerifiedStatus": string;
  "newNotification": number;
  "reason": string;
  "rating": any;
  "mobOtp": any;
  "id": any;
  "vehicleTypeIds": Array<any>;
  "password": string;
  accessTokens: any[];
  vehicles: any[];
  vehicleType: any[];
  job: any[];
  requestfordrivers: any[];
  roleMappings: any[];
  identities: any[];
  credentials: any[];
  constructor(data?: PeopleInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `People`.
   */
  public static getModelName() {
    return "People";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of People for dynamic purposes.
  **/
  public static factory(data: PeopleInterface): People{
    return new People(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'People',
      plural: 'People',
      path: 'People',
      idName: 'id',
      properties: {
        "realm": {
          name: 'realm',
          type: 'string'
        },
        "isProfileComplete": {
          name: 'isProfileComplete',
          type: 'boolean',
          default: false
        },
        "username": {
          name: 'username',
          type: 'string'
        },
        "email": {
          name: 'email',
          type: 'string'
        },
        "companyName": {
          name: 'companyName',
          type: 'string',
          default: ''
        },
        "emailVerified": {
          name: 'emailVerified',
          type: 'boolean'
        },
        "licenceIds": {
          name: 'licenceIds',
          type: 'Array&lt;any&gt;'
        },
        "governmentIds": {
          name: 'governmentIds',
          type: 'Array&lt;any&gt;'
        },
        "image": {
          name: 'image',
          type: 'Array&lt;any&gt;'
        },
        "createdAt": {
          name: 'createdAt',
          type: 'Date'
        },
        "updatedAt": {
          name: 'updatedAt',
          type: 'Date'
        },
        "profileImage": {
          name: 'profileImage',
          type: 'string',
          default: ''
        },
        "isVehicleAlloted": {
          name: 'isVehicleAlloted',
          type: 'boolean',
          default: false
        },
        "adminVerifiedStatus": {
          name: 'adminVerifiedStatus',
          type: 'string',
          default: 'pending'
        },
        "newNotification": {
          name: 'newNotification',
          type: 'number',
          default: 0
        },
        "reason": {
          name: 'reason',
          type: 'string'
        },
        "rating": {
          name: 'rating',
          type: 'any',
          default: <any>null
        },
        "mobOtp": {
          name: 'mobOtp',
          type: 'any',
          default: <any>null
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "vehicleTypeIds": {
          name: 'vehicleTypeIds',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "password": {
          name: 'password',
          type: 'string'
        },
      },
      relations: {
        accessTokens: {
          name: 'accessTokens',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'userId'
        },
        vehicles: {
          name: 'vehicles',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'peopleId'
        },
        vehicleType: {
          name: 'vehicleType',
          type: 'any[]',
          model: '',
          relationType: 'referencesMany',
                  keyFrom: 'vehicleTypeIds',
          keyTo: 'id'
        },
        job: {
          name: 'job',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'driverId'
        },
        requestfordrivers: {
          name: 'requestfordrivers',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'driverId'
        },
        roleMappings: {
          name: 'roleMappings',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'principalId'
        },
        identities: {
          name: 'identities',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'peopleId'
        },
        credentials: {
          name: 'credentials',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'peopleId'
        },
      }
    }
  }
}

/* tslint:disable */

declare var Object: any;
export interface VehicleInterface {
  "type"?: string;
  "vehicleNumber"?: string;
  "registrationNumber"?: string;
  "vehicleMake"?: Date;
  "ton"?: number;
  "bodyType"?: string;
  "logBook"?: Array<any>;
  "ntsaIns"?: Array<any>;
  "insuranceCopy"?: Array<any>;
  "approveStatus": string;
  "isDriverAlloted"?: boolean;
  "id"?: any;
  "ownerId"?: any;
  "driverId"?: any;
  "peopleId"?: any;
  "vehicleTypeId"?: any;
  owner?: any;
  driver?: any;
  vehicleType?: any;
}

export class Vehicle implements VehicleInterface {
  "type": string;
  "vehicleNumber": string;
  "registrationNumber": string;
  "vehicleMake": Date;
  "ton": number;
  "bodyType": string;
  "logBook": Array<any>;
  "ntsaIns": Array<any>;
  "insuranceCopy": Array<any>;
  "approveStatus": string;
  "isDriverAlloted": boolean;
  "id": any;
  "ownerId": any;
  "driverId": any;
  "peopleId": any;
  "vehicleTypeId": any;
  owner: any;
  driver: any;
  vehicleType: any;
  constructor(data?: VehicleInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Vehicle`.
   */
  public static getModelName() {
    return "Vehicle";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Vehicle for dynamic purposes.
  **/
  public static factory(data: VehicleInterface): Vehicle{
    return new Vehicle(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Vehicle',
      plural: 'Vehicles',
      path: 'Vehicles',
      idName: 'id',
      properties: {
        "type": {
          name: 'type',
          type: 'string',
          default: ''
        },
        "vehicleNumber": {
          name: 'vehicleNumber',
          type: 'string',
          default: ''
        },
        "registrationNumber": {
          name: 'registrationNumber',
          type: 'string',
          default: ''
        },
        "vehicleMake": {
          name: 'vehicleMake',
          type: 'Date'
        },
        "ton": {
          name: 'ton',
          type: 'number',
          default: 0
        },
        "bodyType": {
          name: 'bodyType',
          type: 'string',
          default: ''
        },
        "logBook": {
          name: 'logBook',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "ntsaIns": {
          name: 'ntsaIns',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "insuranceCopy": {
          name: 'insuranceCopy',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "approveStatus": {
          name: 'approveStatus',
          type: 'string',
          default: 'pending'
        },
        "isDriverAlloted": {
          name: 'isDriverAlloted',
          type: 'boolean',
          default: false
        },
        "id": {
          name: 'id',
          type: 'any'
        },
        "ownerId": {
          name: 'ownerId',
          type: 'any'
        },
        "driverId": {
          name: 'driverId',
          type: 'any'
        },
        "peopleId": {
          name: 'peopleId',
          type: 'any'
        },
        "vehicleTypeId": {
          name: 'vehicleTypeId',
          type: 'any'
        },
      },
      relations: {
        owner: {
          name: 'owner',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'ownerId',
          keyTo: 'id'
        },
        driver: {
          name: 'driver',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'driverId',
          keyTo: 'id'
        },
        vehicleType: {
          name: 'vehicleType',
          type: 'any',
          model: '',
          relationType: 'belongsTo',
                  keyFrom: 'vehicleTypeId',
          keyTo: 'id'
        },
      }
    }
  }
}

/* tslint:disable */
import { Injectable, Inject, Optional } from '@angular/core';
import { Http, Response } from '@angular/http';
import { SDKModels } from './SDKModels';
import { BaseLoopBackApi } from '../core/base.service';
import { LoopBackConfig } from '../../lb.config';
import { LoopBackAuth } from '../core/auth.service';
import { LoopBackFilter,  } from '../../models/BaseModels';
import { JSONSearchParams } from '../core/search.params';
import { ErrorHandler } from '../core/error.service';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Rx';
import { Job } from '../../models/Job';
import { SocketConnection } from '../../sockets/socket.connections';


/**
 * Api services for the `Job` model.
 */
@Injectable()
export class JobApi extends BaseLoopBackApi {

  constructor(
    @Inject(Http) protected http: Http,
    @Inject(SocketConnection) protected connection: SocketConnection,
    @Inject(SDKModels) protected models: SDKModels,
    @Inject(LoopBackAuth) protected auth: LoopBackAuth,
    @Inject(JSONSearchParams) protected searchParams: JSONSearchParams,
    @Optional() @Inject(ErrorHandler) protected errorHandler: ErrorHandler
  ) {
    super(http,  connection,  models, auth, searchParams, errorHandler);
  }

  /**
   * Patch attributes for a model instance and persist it into the data source.
   *
   * @param {any} id Job id
   *
   * @param {object} data Request data.
   *
   *  - `data` – `{object}` - An object of model property name/value pairs
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * <em>
   * (The remote method definition does not provide any description.
   * This usually means the response is a `Job` object.)
   * </em>
   */
  public patchAttributes(id: any, data: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "PATCH";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Jobs/:id";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {
      data: data
    };
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {string} vehicleName 
   *
   * @param {number} tonnage 
   *
   * @param {number} distanceInMeter 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` - 
   */
  public getJobPricing(vehicleName: any, tonnage: any = {}, distanceInMeter: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Jobs/getJobPricing";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof vehicleName !== 'undefined' && vehicleName !== null) _urlParams.vehicleName = vehicleName;
    if (typeof tonnage !== 'undefined' && tonnage !== null) _urlParams.tonnage = tonnage;
    if (typeof distanceInMeter !== 'undefined' && distanceInMeter !== null) _urlParams.distanceInMeter = distanceInMeter;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {object} data Request data.
   *
   *  - `req` – `{object}` - 
   *
   *  - `requesterId` – `{string}` - 
   *
   *  - `title` – `{string}` - 
   *
   *  - `weight` – `{string}` - 
   *
   *  - `source` – `{object}` - 
   *
   *  - `destination` – `{object}` - 
   *
   *  - `valueOfGoods` – `{number}` - 
   *
   *  - `budget` – `{number}` - 
   *
   *  - `netPrice` – `{number}` - 
   *
   *  - `typeOfVehicle` – `{string}` - 
   *
   *  - `date` – `{date}` - 
   *
   *  - `endDate` – `{date}` - 
   *
   *  - `description` – `{string}` - 
   *
   *  - `natureOfGoods` – `{string}` - 
   *
   *  - `typeOfJob` – `{string}` - 
   *
   *  - `selectedRoute` – `{string}` - 
   *
   *  - `distance` – `{object}` - 
   *
   *  - `duration` – `{object}` - 
   *
   *  - `vehicleTonnage` – `{number}` - 
   *
   *  - `vehicleBodyType` – `{string}` - 
   *
   *  - `requiredType` – `{string}` - 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` - 
   */
  public createJob(req: any = {}, requesterId: any = {}, title: any = {}, weight: any = {}, source: any = {}, destination: any = {}, valueOfGoods: any = {}, budget: any = {}, netPrice: any = {}, typeOfVehicle: any = {}, date: any = {}, endDate: any = {}, description: any = {}, natureOfGoods: any = {}, typeOfJob: any = {}, selectedRoute: any = {}, distance: any = {}, duration: any = {}, vehicleTonnage: any = {}, vehicleBodyType: any = {}, requiredType: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Jobs/createJob";
    let _routeParams: any = {};
    let _postBody: any = {
      data: {
        requesterId: requesterId,
        title: title,
        weight: weight,
        source: source,
        destination: destination,
        valueOfGoods: valueOfGoods,
        budget: budget,
        netPrice: netPrice,
        typeOfVehicle: typeOfVehicle,
        date: date,
        endDate: endDate,
        description: description,
        natureOfGoods: natureOfGoods,
        typeOfJob: typeOfJob,
        selectedRoute: selectedRoute,
        distance: distance,
        duration: duration,
        vehicleTonnage: vehicleTonnage,
        vehicleBodyType: vehicleBodyType,
        requiredType: requiredType
      }
    };
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {string} jobId 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` - 
   */
  public getJobById(jobId: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Jobs/getJobById";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof jobId !== 'undefined' && jobId !== null) _urlParams.jobId = jobId;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {object} req 
   *
   * @param {string} realm 
   *
   * @param {string} jobApproveStatus 
   *
   * @param {string} status 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` - 
   */
  public getJobs(req: any = {}, realm: any, jobApproveStatus: any = {}, status: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Jobs/getJobs";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof realm !== 'undefined' && realm !== null) _urlParams.realm = realm;
    if (typeof jobApproveStatus !== 'undefined' && jobApproveStatus !== null) _urlParams.jobApproveStatus = jobApproveStatus;
    if (typeof status !== 'undefined' && status !== null) _urlParams.status = status;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {object} req 
   *
   * @param {GeoPoint} location 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` - 
   */
  public getNearJobs(req: any = {}, location: any, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Jobs/getNearJobs";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof location !== 'undefined' && location !== null) _urlParams.location = location;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {object} data Request data.
   *
   *  - `filter` – `{object}` - 
   *
   *  - `req` – `{object}` - 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * <em>
   * (The remote method definition does not provide any description.
   * This usually means the response is a `Job` object.)
   * </em>
   */
  public getDriverJob(filter: LoopBackFilter, req: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Jobs/getDriverJob";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof filter !== 'undefined' && filter !== null) _urlParams.filter = filter;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {object} data Request data.
   *
   *  - `jobId` – `{string}` - 
   *
   *  - `req` – `{object}` - 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` - 
   */
  public cancelJob(jobId: any, req: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Jobs/cancelJob";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof jobId !== 'undefined' && jobId !== null) _urlParams.jobId = jobId;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {object} data Request data.
   *
   *  - `req` – `{object}` - 
   *
   *  - `jobId` – `{string}` - 
   *
   *  - `jobOtp` – `{number}` - 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` - 
   */
  public startJob(req: any = {}, jobId: any, jobOtp: any, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Jobs/startJob";
    let _routeParams: any = {};
    let _postBody: any = {
      data: {
        jobId: jobId,
        jobOtp: jobOtp
      }
    };
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {object} data Request data.
   *
   *  - `jobId` – `{string}` - 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` - 
   */
  public endJob(jobId: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Jobs/endJob";
    let _routeParams: any = {};
    let _postBody: any = {
      data: {
        jobId: jobId
      }
    };
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {object} data Request data.
   *
   *  - `req` – `{object}` - 
   *
   *  - `jobId` – `{string}` - 
   *
   *  - `jobApproveStatus` – `{string}` - 
   *
   *  - `reason` – `{string}` - 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` - 
   */
  public approveByAdmin(req: any = {}, jobId: any, jobApproveStatus: any, reason: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Jobs/approveByAdmin";
    let _routeParams: any = {};
    let _postBody: any = {
      data: {
        jobId: jobId,
        jobApproveStatus: jobApproveStatus,
        reason: reason
      }
    };
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {string} jobApproveStatus 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{object}` - 
   */
  public getJobsByAdmin(jobApproveStatus: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Jobs/getJobsByAdmin";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof jobApproveStatus !== 'undefined' && jobApproveStatus !== null) _urlParams.jobApproveStatus = jobApproveStatus;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Find all instances of the model matched by filter from the data source.
   *
   * @param {object} filter Filter defining fields, where, aggregate, order, offset, and limit
   *
   * @param {object} options options
   *
   * @returns {object[]} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * <em>
   * (The remote method definition does not provide any description.
   * This usually means the response is a `Job` object.)
   * </em>
   */
  public aggregate(filter: LoopBackFilter = {}, options: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Jobs/aggregate";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof filter !== 'undefined' && filter !== null) _urlParams.filter = filter;
    if (typeof options !== 'undefined' && options !== null) _urlParams.options = options;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * The name of the model represented by this $resource,
   * i.e. `Job`.
   */
  public getModelName() {
    return "Job";
  }
}

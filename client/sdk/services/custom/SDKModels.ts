/* tslint:disable */
import { Injectable } from '@angular/core';
import { Email } from '../../models/Email';
import { AccessToken } from '../../models/AccessToken';
import { People } from '../../models/People';
import { Transaction } from '../../models/Transaction';
import { Vehicle } from '../../models/Vehicle';
import { Container } from '../../models/Container';
import { VehicleType } from '../../models/VehicleType';
import { Job } from '../../models/Job';
import { JobApply } from '../../models/JobApply';
import { Setting } from '../../models/Setting';
import { Route } from '../../models/Route';
import { Rating } from '../../models/Rating';
import { Requestfordriver } from '../../models/Requestfordriver';
import { Notification } from '../../models/Notification';
import { Otp } from '../../models/Otp';
import { Wallet } from '../../models/Wallet';

export interface Models { [name: string]: any }

@Injectable()
export class SDKModels {

  private models: Models = {
    Email: Email,
    AccessToken: AccessToken,
    People: People,
    Transaction: Transaction,
    Vehicle: Vehicle,
    Container: Container,
    VehicleType: VehicleType,
    Job: Job,
    JobApply: JobApply,
    Setting: Setting,
    Route: Route,
    Rating: Rating,
    Requestfordriver: Requestfordriver,
    Notification: Notification,
    Otp: Otp,
    Wallet: Wallet,
    
  };

  public get(modelName: string): any {
    return this.models[modelName];
  }

  public getAll(): Models {
    return this.models;
  }

  public getModelNames(): string[] {
    return Object.keys(this.models);
  }
}

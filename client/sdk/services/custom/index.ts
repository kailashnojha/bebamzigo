/* tslint:disable */
export * from './Email';
export * from './AccessToken';
export * from './People';
export * from './Transaction';
export * from './Vehicle';
export * from './Container';
export * from './VehicleType';
export * from './Job';
export * from './JobApply';
export * from './Setting';
export * from './Route';
export * from './Rating';
export * from './Requestfordriver';
export * from './Notification';
export * from './Otp';
export * from './Wallet';
export * from './SDKModels';
export * from './logger.service';

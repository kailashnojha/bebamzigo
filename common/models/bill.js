'use strict';
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var msg = require("../messages/bill-msg.json");
var dateFormat = require('dateformat');
let date = require('date-and-time');
var pdf = require('html-pdf');
var fs = require('fs');
var path = require("path");
var loopback = require('loopback');
var config = require("../../server/config.json");
module.exports = function(Bill) {
	disableAllMethods(Bill, ['find']);


	Bill.createBill =function(obj,cb){
      
         let data = {};
         data = obj;
         let pdfName =data.jobId+"Beba-Invoice";
         
         data.generateAt =new Date().toLocaleString('en-US', { timeZone: 'Asia/Kolkata' })
          /*if(data.generatedBy =="vendor"){
             pdfName = "-Invoice";
          }
          else{
            pdfName = data.customerInvoiceNo+"-Invoice";
          }*/


          
          /*Bill.app.models.Job.getJobById(data.jobId,function(err,jobInst){
            if(err) return cb(err,null)
            if(!jobInst)
              return cb(new Error("job not found,can't create bill!"),null)*/
            //else{
              //console.log("job :",jobInst.venToAdmin);
              //jobInst.venToAdmin=true;
              //jobInst.billCreated = true;
              /*jobInst.save(function(err,updated){
                if(err) return cb(err,null)
                console.log("job :",updated);
              });*/
              let optionBody = {
                template : path.resolve(__dirname,'../../server/views/billToCust.ejs'),
                bill :  data 
              }
              createVerificationEmailBody(optionBody, undefined, function(err, html) {
                if(err) return cb(err,null);
                var options = { 
                 format: "Tabloid" ,
                 height: "20in",        // allowed units: mm, cm, in, px
                 width: "15in",            // allowed units: mm, cm, in, px
                 orientation: "portrait", // portrait or landscape
                 border: {
                      "top": "1in",            // default is 0, units: mm, cm, in, px
                      "right": "1in",
                      "bottom": "1in",
                      "left": "1in"
                    }
                };
                
                pdf.create(html, options).toFile(path.resolve(__dirname,'../../server/storage/pdf/'+pdfName+'.pdf'), function(err, res) {
                    if (err) return cb(err,null);
                    Bill.create(data,function(err,bill){
                      if(err)
                        return cb(err,null)

                      cb(null, {data:bill,msg: "Invoice generated"});
                      //Bill.sendReceipt(bill,function(){});
                      Bill.app.models.Job.findById(data.jobId,function(err,jobInst){
                        if(err) return cb(err,null)
                        if(!jobInst)
                          return cb(new Error("job not found"),null)
                        else{
                           let url = "/api/Containers/container/download/";
                          jobInst.billToCustURL = url+pdfName+".pdf";
                          jobInst.save(function(){})
                        }

                      }) 
                      console.log("result pdf :--",res); // { filename: '/app/businesscard.pdf' }
                    });
               

                })
              // var html = fs.readFileSync(path.resolve(__dirname,'../../server/views/vendorToadmin.ejs'), 'utf8');
             
              
              })
            //}  
            
           // })
         
        }

        Bill.remoteMethod("createBill",{
	      accepts : [
	        { arg: 'obj', type: 'object',required:true,http: { source: 'form' }}
	        
	        ], 
	        returns : {arg:"success",type:"object"}
	        
	    })


      Bill.getBill =function(cb){
    
        Bill.find(function(err,success){
          if(err)
            return cb(err,null)
          cb(null,{data:success,msg:"all bills"});
        })
      }


      Bill.remoteMethod('getBill',{
        accepts:[
        //{ arg: 'generatedBy', type: 'string',http:{source:'query'}}
         ],
        returns :{arg:"success",type :"object"},
        http :{verb:'get'}

      })



    function createVerificationEmailBody(verifyOptions, options, cb) {
      var template = loopback.template(verifyOptions.template);
      var body = template(verifyOptions);
      cb(null, body);
		}


}
'use strict';
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var msg = require("../messages/fuelrequest-msg.json");
module.exports = function(Fuelrequest) {
	disableAllMethods(Fuelrequest, ['find']);

	Fuelrequest.makeFuelRequest =function(req,jobId,mobileNumber,amount,cb){
		let driverId = req.accessToken.userId;
		let data = {
			driverId : driverId,
			jobId   : jobId,
			mobileNumber  :  mobileNumber,
			amount   :   amount,
			status   :   "requested",
			createdAt : new Date(),
			updatedAt : new Date()
		}
		Fuelrequest.app.models.Job.findById(jobId,function(err,jobInst){
			if(err) return cb(err,null)
				if(!jobInst) return cb(new Error("job not found"),null); 
					if(amount>jobInst.availableFuelAmount) return cb(new Error("You can not make request more than available fuel amount "),null)
					Fuelrequest.app.models.Wallet.findOne({where:{peopleId:jobInst.ownerId}},function(err,ownerWallet){
						if(err) return cb(err,null)
							if(ownerWallet.walletAmount<amount) {
								//put notification for owner
								Fuelrequest.app.models.Notification.lessWalletAmtOwner(jobInst,function(error,success){});
								return cb(new Error("Owner does not have enough amount in wallet!"),null)}
								Fuelrequest.create(data,function(err,success){
									if(err) return cb(err,null)
									else{

										cb(null, { data: success, msg: msg.makeFuelRequest });

										jobInst.usedFuelAmount = success.amount;
										jobInst.save(function(err,updatedJob){})	
									}
								})
					})
			
		})
	}

	Fuelrequest.remoteMethod("makeFuelRequest", {
      accepts: [
	      { arg: "req", type: "object", http: { source: "req" } },
	      { arg: "jobId", type: "string", http: { source: "form" } },
	      { arg: "mobileNumber", type: "string", http: { source: "form" } },
	      { arg: "amount", type: "number", http: { source: "form" } }
      ],
      returns: { arg: "success", type: "object" }
    })

    //**********************************************confirm request***********************************************


    Fuelrequest.confirmFuelRequest =function(fuelrequestId,cb){
		//let driverId = req.accessToken.userId;
		
		Fuelrequest.findById(fuelrequestId,function(err,freqInst){
			if(err) return cb(err,null)
			if(!freqInst) return cb(new Error("fuel request not found"),null);
			Fuelrequest.app.models.Transaction.create({
														type  :  "fuelRequest",
														createdAt : new Date(),
														updatedAt : new Date(),
														status    : "requested",
														peopleId  : freqInst.driverId,
														jobId     : freqInst.jobId,
														amount    : freqInst.amount,
														mobileNumber : freqInst.mobileNumber
													},
				function(err,successT){
					if(err) return cb(err,null)
					if(successT){
						console.log("Transaction successT:::",successT);
						Fuelrequest.app.models.Transaction.b2c(500,successT.mobileNumber,successT.id, function(err,txn) {
							if(err) return cb(err,null)
							if(txn.ResponseCode==0){
								cb(null, { data: successT, msg: msg.confirmFuelRequest });
							}else{
								Fuelrequest.app.models.Job.findById(freqInst.jobId,function(err,jobInst){
									if(jobInst)
										jobInst.usedFuelAmount = 0;
										jobInst.save(function(err,updatedJob){})
								})
								cb(null, { data: successT, msg: msg.confirmFuelRequestFail });
							}
						});
						
					}
				})
		})
		
	}

	Fuelrequest.remoteMethod("confirmFuelRequest", {
      accepts: [
	      { arg: "fuelrequestId", type: "string", http: { source: "form" } }
	     
      ],
      returns: { arg: "success", type: "object" }
    })


    //**********************************get fuel requests of a job************************************

    Fuelrequest.getFuelRequest =function(jobId,cb){
    	//console.log("jobId",jobId);
		
		//console.log(peopleId);
		let filter ={};
		if (!filter.where)
	        filter.where = {};
	        filter.where.jobId = jobId;
	        filter.order = "createdAt DESC";
	        console.log("filter",filter);
		
		Fuelrequest.find(filter,function(err,success){
			if(err) return cb(err,null)
			
				cb(null, { data: success, msg: msg.getFuelRequest });
				console.log("success:::",success);
			
			
		})
	}

	Fuelrequest.remoteMethod("getFuelRequest", {
      accepts: [
	      //{ arg: "req", type: "object", http: { source: "req" } },
	      { arg: "jobId", type: "string",required:true}
	  ],
      returns: { arg: "success", type: "object" },
      http:{verb:"get"}
    })



    



};

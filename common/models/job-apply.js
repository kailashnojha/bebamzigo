'use strict';
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var msg = require("../messages/job-apply-msg.json");
module.exports = function(Jobapply) {
  disableAllMethods(Jobapply, ['find']);

  Jobapply.applyForJob = function(req, jobId, cb) {
    let driverId = req.accessToken.userId;

    Jobapply.app.models.People.findById(driverId,function(error,peopleInst){
    	if(error) return cb(error,null);
    	
    	if(peopleInst.jobId)
			return cb(new Error("You already have a job"),null);    		

    	if(peopleInst.adminVerifiedStatus!= "approved")
    		return cb(new Error("Your account is not verified by admin"),null);

    	if(!peopleInst.vehicleId)
			return cb(new Error("You do not have any vehicle yet"),null);    		

		

		Jobapply.app.models.Vehicle.findById(peopleInst.vehicleId,function(errorV,vehicleInst){
			if(errorV) return cb(errorV,null);

			if(!vehicleInst.assessmentEnd || (new Date(vehicleInst.assessmentEnd)< new Date()) )
				return cb(new Error("Your vehicle assessment is pending please contact with owner"),null);
	    	Jobapply.app.models.Job.findById(jobId, function(err, jobInst) {
		        if (err) return cb(err, null);

		        // if (jobInst) {
		        if (!jobInst) 
		        	return cb(new Error("No Job found"), null);	

		        if (jobInst.status !== "requested" )
		          	return cb(new Error("Already applied on this job"), null);
		            
	            let jobApplyObj = {
	            	driverId  : driverId,
	            	jobId     : jobInst.id,
	            	createdAt : new Date()
	            }

	            Jobapply.create(jobApplyObj,function(errorJA, jobApplyInst){
	            	if(errorJA) return cb(errorJA,null);

	            	jobInst.applied = true;
	            	jobInst.updatedAt = new Date();
	            	jobInst.driverId = driverId;
	            	jobInst.ownerId  = vehicleInst.ownerId,
	            	jobInst.status = "accepted";

	            	jobInst.save(function(errorJU,success){
	            		if(errorJU) return cb(errorJU,null);
	            		peopleInst.jobId = jobId;
	            		peopleInst.save(function(error,userInfo){
	            			if(error) return cb(error,null);
	            			
	            			Jobapply.app.models.Notification.applyJobNoty(success,function(){});
	            			cb(null, { data: success, msg: msg.jobApplySuccess });	
	            		})
	            		
	            	})
	            })
		            
		         
		    })
		})    
    })
  }

  Jobapply.remoteMethod("applyForJob", {
    accepts: [
      { arg: "req", type: "object", http: { source: "req" } },
      { arg: "jobId", type: "string", http: { source: "form" } }
    ],
    returns: { arg: "success", type: "object" }
  })
};

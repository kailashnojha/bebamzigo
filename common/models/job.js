'use strict';
var msg = require("../messages/job-msg.json");
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var gf = require("./../services/global-function");
module.exports = function(Job) {

  disableAllMethods(Job, ['find']);
  Job.validatesInclusionOf('status', { in: ['pending','requested', 'accepted', 'canceled', 'on trip','end', 'completed','dispute'] });
  Job.validatesPresenceOf('title', { message: 'title can not be blank' });
  Job.validatesPresenceOf('source', { message: 'source can not be blank' });
  // Job.validatesPresenceOf('budget', { message: 'budget can not be blank' });
  Job.validatesPresenceOf('netPrice', { message: 'netprice can not be blank' });
  Job.validatesPresenceOf('date', { message: 'date can not be blank' });

  Job.validatesPresenceOf('typeOfJob', { message: 'type of job can not be blank' });
  Job.validatesInclusionOf('typeOfJob', { in: ['standard rate','on demand'] });

  Job.validatesPresenceOf('jobApproveStatus', { message: 'Job approval can not be blank' });
  Job.validatesInclusionOf('jobApproveStatus', { in: ['pending','reject',"approved"] });

  Job.validatesPresenceOf('requiredType', { message: 'required type can not be blank' });
  Job.validatesInclusionOf('requiredType', { in: ['Vehicle','Machinery'] });

   
  Job.validate('endDate', endDatePOf, { message: 'end date can not be blank' }); 
  Job.validate('weight', weightPOf, { message: 'weight can not be blank' });
  Job.validate('destination', destinationPOf, { message: 'destination can not be blank' });
  Job.validate('valueOfGoods', valueOfGoodsPOf, { message: 'value of goods can not be blank' });
  Job.validate('typeOfVehicle', typeOfVehiclePOf, { message: 'type of vehicle can not be blank' });
  Job.validate('natureOfGoods', natureOfGoodsPOf , { message: 'nature of goods can not be blank' });
  Job.validate('status', statusPOf , { message: 'status can not be blank' });

  Job.getJobPricing = function(vehicleName, tonnage, distanceInMeter, cb) {
    let distanceInKM = distanceInMeter / 1000;
    tonnage = tonnage + " T";
    Job.app.models.Setting.findOne({ where: { type: "Pricing" } }, function(err, pricingInst) {
      if (err)
        cb(err, null);
      else {
        if (pricingInst) {
          let priceObj;
          for (let i = 0; i < pricingInst.pricing.length; i++) {
            if ((pricingInst.pricing[i].name == tonnage) || (pricingInst.pricing[i].name.toLowerCase() == vehicleName.toLowerCase())) {
              priceObj = pricingInst.pricing[i];
              break;
            }
          }

          if (priceObj) {
            if (distanceInKM <= priceObj.baseMileage.value) {
              cb(null, { data: priceObj.standardCharge, msg: msg.getJobPricingSuccess });
            } else {
              let extraKM = Math.ceil(distanceInKM - priceObj.baseMileage.value);
              let extraPay = priceObj.extraCharge.value * extraKM;
              cb(null, { data: (priceObj.standardCharge + extraPay), msg: msg.getJobPricingSuccess });
            }
          } else {
            cb(new Error("No pricing found"), null);
          }

        } else {
          cb(new Error("No pricing found"), null);
        }
      }
    })
  }

  Job.remoteMethod("getJobPricing", {
    accepts: [
      { arg: "vehicleName", type: "string", required: true },
      { arg: "tonnage", type: "number" },
      { arg: "distanceInMeter", type: "number", required: true }
    ],
    returns: { arg: "success", type: "object" },
    http: { verb: "get" }
  })

  Job.createJob = function(
    req, requesterId, title, weight, source, destination,
    valueOfGoods, budget, netPrice, typeOfVehicle,
    date, endDate, description, natureOfGoods, typeOfJob, selectedRoute,
    distance, duration, vehicleTonnage, vehicleBodyType, requiredType, cb
  ) {

    let creatorId = requesterId!= undefined ? requesterId : req.accessToken.userId;
    let creatorAdminId  = requesterId!= undefined ? req.accessToken.userId: undefined;
    let creator,creatorAdmin;

    if(requesterId!= undefined && creatorId.toString() == creatorAdminId.toString()){
      return cb(new Error("Admin can not create own job"),null);
    }else{
      if(requesterId!= undefined && creatorId.toString() != creatorAdminId.toString()){
        Job.app.models.People.find({where:{id:{inq:[creatorId.toString(),creatorAdminId.toString()]}}},function(error,peoples){
          if(error) return cb(error,null);
          if(peoples.length != 2){
            return cb(new Error("Requester not found"),null);
          }else{
            if(peoples[0].id.toString() == creatorId.toString()){
              creator = peoples[0];
              creatorAdmin = peoples[1]; 
            }else{
              creator = peoples[1];
              creatorAdmin = peoples[0]; 
            }

            if(["admin","super_admin"].indexOf(creatorAdmin.realm) == -1){
              return cb(new Error("Unauthorized"),null);
            }
            createNewJob();
          }
        })
      }else{
        createNewJob();
        // Job.app.models.People.findById()
      }
    }


    /*
      if (obj.typeOfJob == "on demand") {
        obj.budget = obj.netPrice;
      } else {
        obj.netPrice = obj.budget;
      }
    */

    function createNewJob(){
      let obj = {
        creatorId       : creatorId,
        creatorAdminId  : creatorAdminId,
        title           : title,
        weight          : weight,
        source          : gf.getLocationObj(source),
        // destination     : gf.getLocationObj(destination),
        valueOfGoods    : valueOfGoods,
        budget          : budget,
        netPrice        : netPrice,
        typeOfVehicle   : typeOfVehicle,
        date            : new Date(date),
        description     : description,
        natureOfGoods   : natureOfGoods,
        typeOfJob       : typeOfJob,
        selectedRoute   : selectedRoute,
        distance        : distance,
        duration        : duration,
        vehicleTonnage  : vehicleTonnage,
        vehicleBodyType : vehicleBodyType,
        requiredType    : requiredType,
        jobApproveStatus: "approved",//"pending",
        // status          : 'requested',
        status          : 'pending',
        jobOtp          : gf.getOTP(),
        createdAt       : new Date(),
        updatedAt       : new Date()
      }
          
      obj.availableFuelAmount = (obj.netPrice*90)/100;
      obj.usedFuelAmount  =  0;
      obj.adminCommission = (obj.netPrice*10)/100;


      if(requiredType == "Vehicle"){
        obj.destination = gf.getLocationObj(destination);
      }else{
        if(endDate)
          obj.endDate = new Date(endDate);
      }

      Job.app.models.Wallet.getWalletById(creatorId,function(error,success){
        if(error) return cb(error,null);
        let wallet = success.data;

        Job.app.models.VehicleType.findOne({where:{name:obj.typeOfVehicle}},function(errorV,vehicleType){
          if(errorV) return cb(error,null);
    
          if(!vehicleType) return cb(new Error("No vehicle found"),null);
          obj.vehicleTypeId = vehicleType.id;
    
          if(obj.netPrice > wallet.walletAmount)
            return cb(new Error("Unsufficient fund"),null);

          Job.create(obj, function(err, jobInst) {
            if (err){
              wallet.save(function(){});
              cb(err, null);
            }
            else {
              wallet.walletAmount -= obj.netPrice;
              wallet.save(function(error,success){
                if(error) return cb(error,null);
                Job.app.models.Transaction.create({
                  method        : "wallet",
                  type          : "send",
                  peopleId      : creatorId,
                  amount        : jobInst.netPrice,
                  paymentStatus : "completed",
                  reason        : "add job",
                  jobId         : jobInst.id,
                  createdAt     : new Date(),
                  updatedAt     : new Date()  
                },function(errorT, trans){
                  if(errorT) return cb(error,null);
                  jobInst.status = 'requested';

                  jobInst.save(function(errorJU,jobInstUp){
                    if(errorJU)  return cb(errorJU,null);
                    cb(null, { data: jobInstUp, msg: msg.jobCreateSuccess });
                    
                  })
                })
              })
            }
          })        
        })          
      })

    }  
  }

  Job.remoteMethod("createJob", {
    accepts: [
      { arg: "req", type: "object", http: { source: "req" } },
      { arg: "requesterId", type: "string", http: { source: "form" } },
      { arg: "title", type: "string", http: { source: "form" } },
      { arg: "weight", type: "string", http: { source: "form" } },
      { arg: "source", type: "object", http: { source: "form" } },
      { arg: "destination", type: "object", http: { source: "form" } },
      { arg: "valueOfGoods", type: "number", http: { source: "form" } },
      { arg: "budget", type: "number", http: { source: "form" } },
      { arg: "netPrice", type: "number", http: { source: "form" } },
      { arg: "typeOfVehicle", type: "string", http: { source: "form" } },
      { arg: "date", type: "date", http: { source: "form" } },
      { arg: "endDate", type: "date", http: { source: "form" } },
      { arg: "description", type: "string", http: { source: "form" } },
      { arg: "natureOfGoods", type: "string", http: { source: "form" } },
      { arg: "typeOfJob", type: "string", http: { source: "form" } },
      { arg: "selectedRoute", type: "string", http: { source: "form" } },
      { arg: "distance", type: "object", http: { source: "form" } },
      { arg: "duration", type: "object", http: { source: "form" } },
      { arg: "vehicleTonnage", type: "number", http: { source: "form" } },
      { arg: "vehicleBodyType", type: "string", http: { source: "form" } },
      { arg: "requiredType", type: "string", http: { source: "form" } }
    ],
    returns: { arg: "success", type: "object" }
  })


  Job.getJobById = function(jobId, cb) {
    Job.findById(jobId, {
      include: [{
        relation: "creator"
      },{
        relation:"vehicleType"
      },
      {
        relation:"driver",
        scope:{
          include:{
            relation: "vehicle"
          }
        }
        
      }
      ]
    }, function(error, jobInst) {
      if (error) return cb(error, null);
      cb(null, { data: jobInst, msg: msg.getJobById })
    })
  }

  Job.remoteMethod("getJobById", {
    accepts: [
      { arg: "jobId", type: "string", required: true }
    ],
    returns: { arg: "success", type: "object" },
    http: { verb: "get" }
  })

  //****************************************get Jobs********************************************************

  Job.getJobs = function(req,realm,cb) {
    let peopleId = req.accessToken.userId;
    let filter = {};
    let inqStatus =['canceled','end', 'completed','dispute'];
    if (!filter.where)
      filter.where = {};
    if(realm=="customer")
        filter.where = {creatorId : peopleId};
    else if(realm == "owner")
        filter.where = {ownerId : peopleId,jobApproveStatus:"approved"};
    else
        filter.where = {driverId : peopleId,jobApproveStatus:"approved",status:{inq:inqStatus}};

    filter.order = "date DESC";
    filter.include = [{
      relation: "creator",
      scope: {
        fields: { realm: true, email: true, name: true, mobile: true }
      }
    },{
      relation: "owner",
      scope: {
        fields: { realm: true, email: true, name: true, mobile: true }
      }
    },{
      relation: "vehicleType"
    },{
      relation: "driver"
    }]

        Job.find(filter, function(err, jobs) {
          if (err) {
            cb(err, null)
          } else {
            cb(null, { data: jobs, msg: msg.getJobsSuccess });
          }
        })

  }

  Job.remoteMethod('getJobs', {
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'realm', type: "string", required: true }
    ],
    returns: { arg: "success", type: "object" },
    http: { verb: 'get' }
  })


  //********************************************get receipt jos************************************************************
  Job.getReceiptJobs = function(req,cb) {
    let peopleId = req.accessToken.userId;
    let filter = {};
    
    if (!filter.where)
      filter.where = {};
      filter.where = {or: [{creatorId:peopleId }, {ownerId: peopleId}],jobApproveStatus:"approved",status:"completed"};
      filter.order = "date DESC";
      filter.include = [{
        relation: "creator",
        scope: {
          fields: { realm: true, email: true, name: true, mobile: true }
        }
      },{
        relation: "owner",
        scope: {
          fields: { realm: true, email: true, name: true, mobile: true }
        }
      },{
        relation: "vehicleType"
      },{
        relation: "driver"
      }]

        Job.find(filter, function(err, jobs) {
          if (err) {
            cb(err, null)
          } else {
            cb(null, { data: jobs, msg: msg.getJobsSuccess });
          }
        })

  }

  Job.remoteMethod('getReceiptJobs', {
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } }
    ],
    returns: { arg: "success", type: "object" },
    http: { verb: 'get' }
  })



  Job.getNearJobs = function(req,location,cb){

    let peopleId = req.accessToken.userId;
    console.log("job location::",location);

    Job.app.models.People.findById(peopleId,
    {
      include:"vehicle"
    },function(errorU,peopleInst){
      if(errorU) return cb(errorU,null);
      var p = peopleInst.toJSON();
      peopleInst.location = gf.getLocationObj(location);
      console.log("driver location object:::",peopleInst);
      peopleInst.save(function(){});
      if(!p.vehicle) return cb(new Error("You have no vehicle!!"),null);
      console.log("peopleInst::::",p.vehicle.type);
      let typeOfVehicle = p.vehicle.type;
        let aggregate = {
          aggregate : [
            {
              $geoNear: {
                  near: { type: "Point", coordinates: [ location.lng , location.lat ] },
                  distanceField: "dist.calculated",
                  query: { 
                    "status":"requested",
                    "jobApproveStatus" : "approved",
                    "typeOfVehicle"    :  typeOfVehicle
                  },
                  num: 5,
                  spherical: true
               }
            },
            {
               $lookup:
                 {
                   from: "VehicleType",
                   localField: "vehicleTypeId",
                   foreignField: "_id",
                   as: "vehicleType"
                 }
            },
            {
              $unwind:
                {
                  path: "$vehicleType",
                  preserveNullAndEmptyArrays: true
                }
            },
            {
               $lookup:
                 {
                   from: "People",
                   localField: "creatorId",
                   foreignField: "_id",
                   as: "creator"
                 }
            },
            {
              $unwind:
                {
                  path: "$creator",
                  preserveNullAndEmptyArrays: true
                }
            },
            {
              $project:{
                "title"             : 1,
                "weight"            : 1,
                "source"            : 1,
                "destination"       : 1,
                "netPrice"          : 1,
                "typeOfVehicle"     : 1,
                "date"              : 1,
                "description"       : 1,
                "didCustGiveRate"   : 1,
                "didCustGiveRate"   : 1,
                "natureOfGoods"     : 1,
                "status"            : 1,
                "jobApproveStatus"  : 1,
                "endDate"           : 1,
                "selectedRoute"     : 1,
                "requiredType"      : 1,
                "id"                : "$_id",
                "creatorId"         : 1,
                "driverId"          : 1,
                "valueOfGoods"      : 1,
                "typeOfJob"         : 1,
                "distance"          : 1,
                "distance"          : 1,
                "vehicleTonnage"    : 1,
                "jobOtp"            : 1,
                "createdAt"         : 1,
                "adminId"           : 1,
                "applied"           : 1,
                "vehicleType.name"  : 1,
                "vehicleType.type"  : 1,
                "vehicleType.imgUrl": 1,
                "creator.email"     : 1,
                "creator.mobile"    : 1,
                "creator.profileImage": 1,
                "creator.name"      : 1,
                "creator.id"        : 1
              }
            }
          ]
        }

        Job.aggregate(aggregate,{},function(error,jobs){
          if(error) return cb(error,null);

          cb(null,{data:jobs,msg:msg.getNearJobs});
        })

       /* peopleInst.location = gf.getLocationObj(location);
        console.log("driver location object:::",peopleInst);
        peopleInst.save(function(){});*/

    })



    /*Job.find({
      fields:{
        "title":true,
        "weight": true,
        "source": true,
        "destination":true,
        "budget":true,
        "netPrice":true,
        "typeOfVehicle":true,
        "date":true,
        "description":true,
        "natureOfGoods":true,
        "id":true,
        "creatorId":true,
        "valueOfGoods":true,
        "typeOfJob":true,
        "distance":true,
        "duration":true,
        "vehicleTonnage":true,
        "status":true,
        "jobOtp":true,
        "createdAt":true,
        "updatedAt":true,
        "selectedRoute":true
      },
      where:{
        "source.location": { near: location },
        "status":"requested"
      }
    },function(error,jobs){
      if(error) return cb(error,null);
      cb(null,{data:jobs,msg:msg.getNearJobs});
    })*/
  }

  Job.remoteMethod("getNearJobs",{
    accepts : [
      {arg:"req",type:"object",http:{source:"req"}},
      {arg:"location",type:"GeoPoint",required:true}
    ],
    returns : {arg:"success",type:"object"},
    http:{verb:"get"}
  })

  Job.getDriverJob = function(filter, req, cb) {
    if (!filter)
      filter = {};

    if (!filter.where)
      filter.where = {};

    filter.where.driverId = req.accessToken.userId;

    filter.include = [{
      relation: "driver",
      scope: {
        fields: { driverId: true }
      }
    }]

    Job.findById(filter, function(err, job) {
      if (err) {
        cb(err, null)
      } else {
        cb(null, job)
      }
    })
  }

  Job.remoteMethod('getDriverJob', {
    accepts: [
      { arg: 'filter', type: 'object', required: true },
      { arg: 'req', type: 'object', required: true, 'http': { source: 'req' } }

    ],
    returns: { arg: "job", type: "object", "root": true },
    http: { verb: 'post' }
  })

/*  Job.createJobByAdmin = function(
    customerId, title, weight, source, destination,
    valueOfGoods, budget, netPrice, typeOfVehicle,
    date, description, natureOfGoods, typeOfJob, selectedRoute,
    distance, duration, vehicleTonnage, vehicleBodyType, cb) {

    let obj = {
      customerId: customerId,
      title: title,
      weight: weight,
      source: gf.getLocationObj(source),
      destination: gf.getLocationObj(destination),
      valueOfGoods: valueOfGoods,
      budget: budget,
      netPrice: netPrice,
      typeOfVehicle: typeOfVehicle,
      date: date,
      description: description,
      natureOfGoods: natureOfGoods,
      typeOfJob: typeOfJob,
      selectedRoute: selectedRoute,
      distance: distance,
      duration: duration,
      jobApproveStatus     : "pending",
      vehicleTonnage: vehicleTonnage,
      vehicleBodyType: vehicleBodyType
    }

    if (obj.typeOfJob == "on demand") {
      obj.budget = obj.netPrice;
    } else {
      obj.netPrice = obj.budget;
    }

    //Job.createJobByAdmin(obj);

    Job.create(obj, function(err, jobInst) {
      if (err)
        cb(err, null);
      else
        //cb(null, { data: jobInst, msg: msg.jobCreateSuccess });
        cb(null, jobInst)
    })
  }

  Job.remoteMethod("createJobByAdmin", {
    accepts: [
      { arg: "customerId", type: "string", http: { source: "form" } },
      { arg: "title", type: "string", http: { source: "form" } },
      { arg: "weight", type: "string", http: { source: "form" } },
      { arg: "source", type: "object", http: { source: "form" } },
      { arg: "destination", type: "object", http: { source: "form" } },
      { arg: "valueOfGoods", type: "number", http: { source: "form" } },
      { arg: "budget", type: "number", http: { source: "form" } },
      { arg: "netPrice", type: "number", http: { source: "form" } },
      { arg: "typeOfVehicle", type: "string", http: { source: "form" } },
      { arg: "date", type: "date", http: { source: "form" } },
      { arg: "description", type: "string", http: { source: "form" } },
      { arg: "natureOfGoods", type: "string", http: { source: "form" } },
      { arg: "typeOfJob", type: "string", http: { source: "form" } },
      { arg: "selectedRoute", type: ["object"], http: { source: "form" } },
      { arg: "distance", type: "object", http: { source: "form" } },
      { arg: "duration", type: "object", http: { source: "form" } },
      { arg: "vehicleTonnage", type: "number", http: { source: "form" } },
      { arg: "vehicleBodyType", type: "string", http: { source: "form" } }
    ],
    returns: { arg: "success", type: "object" }
  })*/

  Job.cancelJob = function(jobId, req, comment, cb) {
    let peopleId = req.accessToken.userId;
    Job.findById(jobId, function(err, jobInst) {
      if (err)  return cb(err, null)
      if (!jobInst) return cb(new Error("No instance found"), null);
      if (jobInst.status == "accepted") {
            jobInst.status = 'requested';
            jobInst.updatedAt = new Date();
            jobInst.driverId = null;
            jobInst.applied = false;
            
            jobInst.save(function(err, inst) {
              if (err)
                cb(err, null)
              else{
                cb(null, inst);
                Job.app.models.People.findById(peopleId,function(err,peopleInst){
                  if(err) return cb(err,null)
                  if(peopleInst.realm == "driver"){
                      peopleInst.updateAttributes({jobId:null},function(err,done){})
                  }else{
                      Job.app.models.People.findById(jobInst.driverId,function(err,driverInst){
                        if(err) return cb(err,null)
                        driverInst.updateAttributes({jobId: null},function(err,done){}) 
                      })
                  }
                })

              }
            })
      }else if(jobInst.status=="on trip"){
            jobInst.status = 'canceled';
            jobInst.canceledDate = new Date();
            jobInst.comment  = comment;
            jobInst.save(function(err, inst) {
              if (err)
                cb(err, null)
              else{
                cb(null, inst);
                Job.app.models.People.findById(peopleId,function(err,peopleInst){
                  if(err) return cb(err,null)
                  if(peopleInst.realm =="driver"){
                      peopleInst.updateAttributes({jobId:null},function(err,done){})

                  }else{
                        Job.app.models.People.findById(jobInst.driverId,function(err,driverInst){
                          if(err) return cb(err,null)
                          driverInst.updateAttributes({jobId: null},function(err,done){}) 
                        })
                  }
                  
                })

              }
            })
      } 
    })
  }

  Job.remoteMethod('cancelJob', {
    accepts: [
      { arg: "jobId", type: "string", required: true },
      { arg: "req", type: "object", http: { source: "req" } },
      { arg: "comment", type: "string", http: { source: "form" } }
    ],
    returns: { arg: "success", type: "object" }
  })

  Job.startJob = function(req, jobId, jobOtp, cb){
    let driverId = req.accessToken.userId;
    Job.findById(jobId,{
      include:[
        {
          relation:"driver"
        },
        {
          relation:"creator"
        }
      ]
    },function(error,jobInst){
      if(error) return cb(error,null);
      if(!jobInst) return cb(new Error("No job found"),null);

      if(jobInst.driverId.toString() == driverId.toString()){
        if(jobInst.jobOtp == jobOtp){
          jobInst.status = "on trip";
          jobInst.jobOtp = null;
          jobInst.updatedAt = new Date();

          let driverInst = jobInst.driver();
          let customerInst = jobInst.creator();

          jobInst.save(function(error,success){
            if(error) return cb(error,null);
            Job.app.models.Notification.startTripNoty(customerInst,driverInst,jobInst, function() {});
            cb(null,{data:success,msg:msg.startJob});
          })
        }else{
          cb(new Error("Invalid otp"),null);
        }
      }else{
        cb(new Error("Unauthorized"),null);
      }
    })
  }

  Job.remoteMethod("startJob",{
    accepts : [
      {arg:"req", type:"object", http:{source:"req"}},
      {arg:"jobId", type:"string", required:true, http:{source:"form"}},
      {arg:"jobOtp", type:"number", required:true, http:{source:"form"}}
    ],
    returns : {arg:"success",type:"object"}
  })

  Job.endJob = function(jobId, cb){
    Job.findById(jobId,{
      include:[{
        relation:"driver"
      },{
        relation:"creator"
      }]
    },function(error,jobInst){
      if(error) return cb(error,null);
      if(!jobInst) return cb(new Error("No job found"),null);
      if(jobInst.status != "on trip" ) return cb(new Error("something went wrong"));

      let driverInst = jobInst.driver();
      let customerInst = jobInst.creator();

      Job.app.models.Wallet.getWalletById(driverInst.ownerId,function(errorW,transWallet){
        if(errorW) return cb(errorW,null);

        transWallet = transWallet.data;

        jobInst.status = "end";
        jobInst.save(function(error,success){
          if(error) return cb(error,null);
          
          cb(null,{data:success,msg:msg.endJob});

         // driverInst.jobId = null;
         // driverInst.save(function(){});

          transWallet.walletAmount += jobInst.netPrice;
          transWallet.save(function(error,success){
            if(success){
              Job.app.models.Transaction.create({
                method        : "wallet",
                type          : "received",
                peopleId      : driverInst.ownerId,
                amount        : jobInst.netPrice,
                paymentStatus : "completed",
                reason        : "job completed",
                jobId         : jobInst.id,
                createdAt     : new Date(),
                updatedAt     : new Date()  
              },function(errorT,trans){
                if(trans){
                  Job.app.models.Notification.jobCompTransNoty(customerInst,driverInst,jobInst,trans, function() {});              
                }
              })    
            }
          })

          Job.app.models.Notification.endTripNoty(customerInst,driverInst,jobInst, function() {});
        })

      })

              
    })
  }

  Job.remoteMethod("endJob",{
    accepts : [
      {arg:"jobId",type:"string",http:{source:"form"}}
    ],
    returns : {arg:"success",type:"object"}
  })


  //*************************************************Complete job api****************************

  Job.completeJob = function(jobId,status,comment, cb){
    Job.findById(jobId,{
      include:[{
        relation:"driver"
      },{
        relation:"creator"
      },
      {
        relation:"vehicle"
      }]
    },function(error,jobInst){
      if(error) return cb(error,null);
      if(!jobInst) return cb(new Error("No job found"),null);
      if(jobInst.status != "end" ) return cb(new Error("something went wrong"));

      let driverInst = jobInst.driver();
      let customerInst = jobInst.creator();
      
      let subTotal = jobInst.netprice-jobInst.adminCommission;
      let billObj = {
        jobId:jobInst.id,
        title :jobInst.title,
        vehicleNumber:jobInst.vehicle.vehicleNumber,
        typeOfVehicle:jobInst.typeOfVehicle,
        pickupLocation :jobInst.source,
        destLocation: jobInst.destination,
        distance    :jobInst.distance.text,
        duration   : jobInst.duration.text,
        typeOfJob  : jobInst.typeOfJob,
        netPrice :jobInst.netPrice,
        budget : jobInst.budget,
        driverName:driverInst.name,
        weight :jobInst.weight,
        createdDate:jobInst.createdAt,
        jobStartDate :jobInst.date,
        subTotal: jobInst.subTotal,
        commission:jobInst.adminCommission,
        total:jobInst.netprice

      }
      if(status =="completed"){

        jobInst.updateAttributes({status:status,comment:comment},function(error,success){
          if(error) return cb(error,null);
          else{
            Job.app.models.Wallet.findOne({where:{peopleId:jobInst.ownerId}},function(err,walletInst){
              if(walletInst){
                  let amt = walletInst.walletAmount+subTotal;
                  walletInst.walletAmount = amt;
                  walletInst.save(function(err,updatedW){});
                  Job.app.models.Bill.createBill(billObj,function(){});
              }
            })

            cb(null,{data:success,msg:msg.endJob});
          }
          

          driverInst.jobId = null;
          driverInst.save(function(){});

        })
      }else{
         jobInst.updateAttributes({status:status,comment:comment},function(error,success){
          if(error) return cb(error,null);
          cb(null,{data:success,msg:msg.endJob});
          driverInst.jobId = null;
          driverInst.save(function(){});

        })
      }
      

          //Job.app.models.Notification.endTripNoty(customerInst,driverInst,jobInst, function() {});
    })
  }

  Job.remoteMethod("completeJob",{
    accepts : [
      {arg:"jobId",type:"string",required:true,http:{source:"form"}},
      {arg:"status",type:"string", required:true,http:{source:"form"}},
      {arg:"comment",type:"string",http:{source:"form"}}
    ],
    returns : {arg:"success",type:"object"}
  })



// ***************************** Admin Api's *********************************************

  Job.approveByAdmin = function(req,jobId,jobApproveStatus,reason,cb){
    let adminId = req.accessToken.userId;

    Job.findById(jobId,{include:"creator"},function(error,jobInst){
      if(error) return cb(error,null);
      if(!jobInst) return cb(new Error("No job found"),null);

      let peopleInst = jobInst.creator();
      jobInst.adminId = adminId;
      jobInst.jobApproveStatus = jobApproveStatus;
      jobInst.reason = reason;

      Job.app.models.People.findById(adminId,function(errorA,adminInst){
        if(errorA) return cb(errorA,null);

        jobInst.save(function(error,jobInst){
          if(error) return cb(error,null);
          cb(null,{data:jobInst, msg:msg.approveByAdmin});
          Job.app.models.Notification.jobApprovalNoty(jobInst,peopleInst,adminInst,function(){});
          if(jobApproveStatus === "approved");
            Job.app.models.Notification.createJobNoty(jobInst, function() {});      
        })
      })

    })
  }

  Job.remoteMethod("approveByAdmin",{
    accepts : [
      {arg:"req",type:"object",http:{source:"req"}},
      {arg:"jobId",type:"string",required:true,http:{source:"form"}},
      {arg:"jobApproveStatus",type:"string",required:true,http:{source:"form"}},
      {arg:"reason",type:"string",http:{source:"form"}}
    ],
    returns : {arg:"success",type:"object"}
  })


  Job.getJobsByAdmin = function(jobApproveStatus,cb){
    Job.find({
      where:{
        jobApproveStatus:jobApproveStatus
      }
    },function(error,success){
      if(error) return cb(error,null);

      cb(null,{data:success,msg:msg.getJobsByAdmin});
    })
  }

  Job.remoteMethod("getJobsByAdmin",{
    accepts : [
      {arg:"jobApproveStatus",type:"string"}
    ],
    returns:{arg:"success",type:"object"},
    http:{verb:"get"}
  })


// *********************************** Validations*****************************************

  function weightPOf(err){
    if(this.requiredType == "Vehicle" && (this.weight == undefined || this.weight == ""))
      err();
  }

  function destinationPOf(err){
    if(this.requiredType == "Vehicle" && (this.destination == undefined || this.destination == ""))
      err();
  }

  function valueOfGoodsPOf(err){
    if(this.requiredType == "Vehicle" && (this.valueOfGoods == undefined || this.valueOfGoods == ""))
      err();
  }

  function typeOfVehiclePOf(err){
    if(this.requiredType == "Vehicle" && (this.typeOfVehicle == undefined || this.typeOfVehicle == ""))
      err();
  }

  function natureOfGoodsPOf(err){
    if(this.requiredType == "Vehicle" && (this.natureOfGoods == undefined || this.natureOfGoods == ""))
      err();
  }

  function statusPOf(err){
    if(this.requiredType == "Vehicle" && (this.status == undefined || this.status == ""))
      err();
  }

  function endDatePOf(err){
    
    if(this.requiredType == "Machinery" && (this.endDate == undefined || this.endDate == ""))
      err();
  }

};

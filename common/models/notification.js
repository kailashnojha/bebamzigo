'use strict';

var admin = require("firebase-admin");

var serviceAccount = require("../../server/firebaseToken.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://bebamzego.firebaseio.com"
});
var msg = require("../messages/notification-msg.json");
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var notyMsg = require("./../services/notifications-msg");
module.exports = function(Notification) {

  disableAllMethods(Notification, ['find']);

  // This notification send after create job to near by drivers.

  function updateNotyCount(peopleId){
    Notification.app.models.People.updateAll(
      {id:peopleId.toString()},
      {$inc:{newNotification:1}},
      {allowExtendedOperators: true},
    function(error,success){})
  }


  Notification.createJobNoty = function(jobInst, cb) {

    Notification.app.models.People.find({
      where: {
        location: { near: jobInst.source.location },
        realm: "driver",
        vehicleId:{exists:true},
        jobId:{exists:false}
      },
      include: {
        relation: "accessTokens"
      },
      limit: 20
    }, function(error, drivers) {
      if (error)
        cb(error, null);
      else {
        drivers.forEach(function(driver) {
          Notification.create({
            type:"new_job",
            peopleId:driver.id,
            jobId:jobInst.id,
            creatorId:jobInst.creatorId,
            createdAt:new Date(),
            updatedAt:new Date()            
          },function(error,success){
            if(error) return cb(error,null);

            updateNotyCount(driver.id);
            console.log("******************* createJobNoty ***************************");

            driver.accessTokens.forEach(function(token) {
              if (!token.firebaseToken) {
                return;
              }

              let obj = notyMsg.addJobNoty(jobInst, driver, token.ln);
              var payload = {
                data: obj,
                notification: obj
              };

              console.log(payload);

              payload.notification.click_action =  ".Activity.NotificationJobActivity"; 

              admin.messaging().sendToDevice(token.firebaseToken, payload)
              .then(function(response) {
                cb(null, response);
              })
              .catch(function(error) {
                console.log(error)
                cb(error, null);
              });
            })

            console.log("******************* createJobNoty ***************************");

          })
        })
      }
    })
  }

  Notification.remoteMethod("createJobNoty", {
    accepts: { arg: "jobInst", type: "object" },
    returns: { arg: "success", type: "object" }
  })

  // Send notification driver apply for job.

  Notification.applyJobNoty = function(jobInst, cb) {
    Notification.app.models.People.findById(jobInst.creatorId, {
      include: {
        relation: "accessTokens"
      }
    }, function(error, userInst) {
      if (error) return cb(error, null);
      let owner = userInst.toJSON();
      
      Notification.create({
        type:"apply_job",
        peopleId:owner.id,
        creatorId:jobInst.driverId,
        jobId:jobInst.id,
        createdAt:new Date(),
        updatedAt:new Date()
      },function(error,success){

        if(error) return cb(error,null);

        updateNotyCount(owner.id);
        console.log("********************* applyJobNoty **************************");
        owner.accessTokens.forEach(function(token) {
          if (!token.firebaseToken)
            return;

          let obj = notyMsg.applyJobNoty(jobInst, owner, token.ln);
          var payload = {
            data: obj,
            notification: obj
          };

          console.log(payload);

          payload.notification.click_action =  ".Activity.NotificationJobActivity";

          admin.messaging().sendToDevice(token.firebaseToken, payload)
          .then(function(response) {
            // console.log(response.results);
            cb(null, response);
          })
          .catch(function(error) {
            // console.log(error)
            cb(error, null);
          });
        })
        console.log("********************* applyJobNoty **************************");
      })

    })
  }

  Notification.remoteMethod("applyJobNoty", {
    accepts: [
      { arg: "jobInst", type: "object" }
    ],
    returns: { arg: "success", type: "object" }
  })


  // when owner request for a driver then this api call

  Notification.makeRequestNoty = function(driverInst, vehicleInst, ownerId, cb) {
    // Notification.app.models.People.find(driverInst.)
    Notification.app.models.AccessToken.find({
      where: {
        userId: driverInst.id
        //realm: "driver"
      }
    }, function(err, tokens) {
      if (err)
        cb(err, null);
      else {
        Notification.create({
          type:"owner_request",
          peopleId:driverInst.id,
          creatorId:ownerId,
          vehicleId:vehicleInst.id,
          createdAt:new Date(),
          updatedAt:new Date()            
        },function(error,success){

          if(error) return cb(error,null);

          updateNotyCount(driverInst.id);
          
          console.log("******************** makeRequestNoty **************************");

          tokens.forEach(function(token) {
            if (!token.firebaseToken) {
              return;
            }

            let obj = notyMsg.makeRequestNoty(driverInst,vehicleInst,ownerId, token.ln);
            var payload = {
              data: obj,
              notification: obj
            };

            console.log(payload);

            payload.notification.click_action =  ".Activity.NotificationJobActivity";

            admin.messaging().sendToDevice(token.firebaseToken, payload)
              .then(function(response) {
                cb(null, response);
              })
              .catch(function(error) {
                cb(error, null);
              });
          })

          console.log("******************** makeRequestNoty **************************");
        })
      }
    })

  }

  Notification.remoteMethod("makeRequestNoty", {
    accepts: [
      { arg: "driverInst", type: "object" },
      { arg: "vehicleInst", type: "object" },
      { arg: "ownerId", type: "string" },
    ],
    returns: { arg: "success", type: "object" }
  })

  // when driver confrim request of owner then this api call

  Notification.confirmRequestNoty = function(ownerInst, driverInst, vehicleInst, cb) {
    Notification.app.models.AccessToken.find({
      where:{
        userId:ownerInst.id
      }
    }, function(err, tokens) {
      if (err)
        cb(err, null);
      else {
        Notification.create({
          type:"driver_confirmation",
          peopleId:ownerInst.id,
          creatorId:driverInst.id,
          vehicleId:vehicleInst.id,
          createdAt:new Date(),
          updatedAt:new Date()
        },function(error,success){
          if(error) return cb(error,null);

          updateNotyCount(ownerInst.id);

          console.log("********************** confirmRequestNoty ***********************");

          tokens.forEach(function(token) {
            if (!token.firebaseToken) {
              return;
            }

            let obj = notyMsg.confirmByDriver(ownerInst, token.ln);
            var payload = {
              data: obj,
              notification: obj
            };

            console.log(payload);

            payload.notification.click_action =  ".Activity.NotificationJobActivity";

            admin.messaging().sendToDevice(token.firebaseToken, payload)
              .then(function(response) {
                cb(null, response);
              })
              .catch(function(error) {
                cb(error, null);
              });
          })

          console.log("********************** confirmRequestNoty ***********************");

        })    
      }
    })
  }

  Notification.remoteMethod("confirmRequestNoty", {
    accepts: [
      { arg: "ownerInst", type: "object" },
      { arg: "driverInst", type: "object" },
      { arg: "vehicleInst", type: "object" }
    ],
    returns: { arg: "success", type: "object" }
  })

  // when trip start then this api call

  Notification.startTripNoty = function(customerInst, driverInst, jobInst, cb) {
    Notification.app.models.AccessToken.find({
      where:{
        userId:customerInst.id
      }
    }, function(err, tokens) {
      if (err)
        cb(err, null);
      else {
        Notification.create({
          type:"end_trip",
          peopleId:customerInst.id,
          creatorId:driverInst.id,
          jobId:jobInst.id,
          createdAt:new Date(),
          updatedAt:new Date()
        },function(error,success){
          if(error) return cb(error,null);
          
          updateNotyCount(customerInst.id);

          

          tokens.forEach(function(token) {
            if (!token.firebaseToken) {
              return;
            }

            let obj = notyMsg.startTripNoty(customerInst, driverInst, jobInst, token.ln);
            var payload = {
              data: obj,
              notification: obj
            };

            payload.notification.click_action =  ".Activity.NotificationJobActivity";

            admin.messaging().sendToDevice(token.firebaseToken, payload)
              .then(function(response) {
                cb(null, response);
              })
              .catch(function(error) {
                cb(error, null);
              });
          })
        })    
      }
    })
  }

  Notification.remoteMethod("startTripNoty", {
    accepts: [
      { arg: "customerInst", type: "object" },
      { arg: "driverInst", type: "object" },
      { arg: "jobInst", type: "object" }
    ],
    returns: { arg: "success", type: "object" }
  })

  // when trip end then this api call

  Notification.endTripNoty = function(customerInst, driverInst, jobInst, cb) {
    Notification.app.models.AccessToken.find({
      where:{
        userId:customerInst.id
      }
    }, function(err, tokens) {
      if (err)
        cb(err, null);
      else {
        Notification.create({
          type:"end_trip",
          peopleId:customerInst.id,
          creatorId:driverInst.id,
          jobId:jobInst.id,
          createdAt:new Date(),
          updatedAt:new Date()
        },function(error,success){
          if(error) return cb(error,null);
          
          updateNotyCount(customerInst.id);

          tokens.forEach(function(token) {
            if (!token.firebaseToken) {
              return;
            }

            let obj = notyMsg.endTripNoty(customerInst, driverInst, jobInst, token.ln);
            var payload = {
              data: obj,
              notification: obj
            };

            payload.notification.click_action =  ".Activity.NotificationJobActivity";

            admin.messaging().sendToDevice(token.firebaseToken, payload)
              .then(function(response) {
                cb(null, response);
              })
              .catch(function(error) {
                cb(error, null);
              });
          })
        })    
      }
    })
  }

  Notification.remoteMethod("endTripNoty", {
    accepts: [
      { arg: "customerInst", type: "object" },
      { arg: "driverInst", type: "object" },
      { arg: "jobInst", type: "object" }
    ],
    returns: { arg: "success", type: "object" }
  })

  // when job complete then this api call
 
  Notification.jobCompTransNoty = function(customerInst, driverInst, jobInst, transInst, cb){
    Notification.app.models.AccessToken.find({
      where:{
        userId:driverInst.ownerId
      }
    }, function(err, tokens) {
      if (err)
        cb(err, null);
      else {
        Notification.create({
          type:"trip_complete_trans",
          peopleId:driverInst.ownerId,
          creatorId:customerInst.id,
          jobId:jobInst.id,
          transactionId:transInst.id,
          createdAt:new Date(),
          updatedAt:new Date()
        },function(error,success){
          if(error) return cb(error,null);
            
          updateNotyCount(customerInst.id);  
          console.log("*************** jobCompTransNoty *****************");
          tokens.forEach(function(token) {
            if (!token.firebaseToken) {
              return;
            }

            let obj = notyMsg.jobCompTransNoty(customerInst, driverInst, jobInst, transInst, token.ln);
            var payload = {
              data: obj,
              notification: obj
            };
            console.log(payload);
            payload.notification.click_action =  ".Activity.NotificationJobActivity";

            admin.messaging().sendToDevice(token.firebaseToken, payload)
              .then(function(response) {
                cb(null, response);
              })
              .catch(function(error) {
                cb(error, null);
              });
          })
          console.log("*************** jobCompTransNoty *****************");
        })    
      }
    })
  }

  Notification.remoteMethod("jobCompTransNoty",{
    accepts: [
      { arg: "customerInst", type: "object" },
      { arg: "driverInst", type: "object" },
      { arg: "jobInst", type: "object" }
    ],
    returns: { arg: "success", type: "object" }
  })

  // when user add  payment in wallet and payment will completed then this api call

  Notification.addPaymentSuccess = function(peopleInst,transInst,cb){
    Notification.app.models.AccessToken.find({
      where:{
        userId:peopleInst.id
      }
    }, function(err, tokens) {
      if (err)
        cb(err, null);
      else {
        Notification.create({
          type:"add_payment_success",
          peopleId:peopleInst.id,
          transactionId:transInst.id,
          createdAt:new Date(),
          updatedAt:new Date()
        },function(error,success){
          if(error) return cb(error,null);

          updateNotyCount(peopleInst.id); 
          console.log("********************** addPaymentSuccess ***********************");
          tokens.forEach(function(token) {
            if (!token.firebaseToken) {
              return;
            }

            let obj = notyMsg.addPaymentSuccess(peopleInst, transInst, token.ln);
            var payload = {
              data: obj,
              notification: obj
            };

            console.log(payload);

            payload.notification.click_action =  ".Activity.NotificationJobActivity";

            admin.messaging().sendToDevice(token.firebaseToken, payload)
              .then(function(response) {
                cb(null, response);
              })
              .catch(function(error) {
                cb(error, null);
              });
          })
          console.log("********************** addPaymentSuccess ***********************");
        })    
      }
    })    
  }

  Notification.remoteMethod("addPaymentSuccess",{
    accepts : [
      {arg:"peopleInst",type:"object"},
      {arg:"transInst",type:"object"}
    ],
    returns : {arg:"success",type:"object"}
  })


  // when user add  payment in wallet and payment will failed then this api call

  Notification.addPaymentFail = function(peopleInst,transInst,cb){
    Notification.app.models.AccessToken.find({
      where:{
        userId:peopleInst.id
      }
    }, function(err, tokens) {
      if (err)
        cb(err, null);
      else {
        Notification.create({
          type:"add_payment_fail",
          peopleId:peopleInst.id,
          transactionId:transInst.id,
          createdAt:new Date(),
          updatedAt:new Date()
        },function(error,success){
          if(error) return cb(error,null);

          updateNotyCount(peopleInst.id); 
          console.log("******************** addPaymentFail *********************");
          tokens.forEach(function(token) {
            if (!token.firebaseToken) {
              return;
            }

            let obj = notyMsg.addPaymentFail(peopleInst, transInst, token.ln);
            var payload = {
              data: obj,
              notification: obj
            };
            console.log(payload);
            payload.notification.click_action =  ".Activity.NotificationJobActivity";

            admin.messaging().sendToDevice(token.firebaseToken, payload)
              .then(function(response) {
                cb(null, response);
              })
              .catch(function(error) {
                cb(error, null);
              });
          })
          console.log("******************** addPaymentFail *********************");
        })    
      }
    })    
  }

  Notification.remoteMethod("addPaymentFail",{
    accepts : [
      {arg:"peopleInst",type:"object"},
      {arg:"transInst",type:"object"}
    ],
    returns : {arg:"success",type:"object"}
  })


  // when user withdraw from wallet and payment will completed then this api call

  Notification.withdrawPaymentSuccess = function(peopleInst,transInst,cb){
    Notification.app.models.AccessToken.find({
      where:{
        userId:peopleInst.id
      }
    }, function(err, tokens) {
      if (err)
        cb(err, null);
      else {
        Notification.create({
          type:"withdrawel_success",
          peopleId:peopleInst.id,
          transactionId:transInst.id,
          createdAt:new Date(),
          updatedAt:new Date()
        },function(error,success){
          if(error) return cb(error,null);

          updateNotyCount(peopleInst.id); 
          console.log("********************** withdrawPaymentSuccess ***********************");
          tokens.forEach(function(token) {
            if (!token.firebaseToken) {
              return;
            }

            let obj = notyMsg.withdrawPaymentSuccess(peopleInst, transInst, token.ln);
            var payload = {
              data: obj,
              notification: obj
            };

            console.log(payload);

            payload.notification.click_action =  ".Activity.NotificationJobActivity";

            admin.messaging().sendToDevice(token.firebaseToken, payload)
              .then(function(response) {
                cb(null, response);
              })
              .catch(function(error) {
                cb(error, null);
              });
          })
          console.log("********************** withdrawPaymentSuccess ***********************");
        })    
      }
    })    
  }

  Notification.remoteMethod("withdrawPaymentSuccess",{
    accepts : [
      {arg:"peopleInst",type:"object"},
      {arg:"transInst",type:"object"}
    ],
    returns : {arg:"success",type:"object"}
  })


  // when user withdraw from wallet and payment will failed then this api call

  Notification.withdrawPaymentFail = function(peopleInst,transInst,cb){
    Notification.app.models.AccessToken.find({
      where:{
        userId:peopleInst.id
      }
    }, function(err, tokens) {
      if (err)
        cb(err, null);
      else {
        Notification.create({
          type:"withdrawel_success",
          peopleId:peopleInst.id,
          transactionId:transInst.id,
          createdAt:new Date(),
          updatedAt:new Date()
        },function(error,success){
          if(error) return cb(error,null);

          updateNotyCount(peopleInst.id); 
          console.log("********************** withdrawPaymentFail ***********************");
          tokens.forEach(function(token) {
            if (!token.firebaseToken) {
              return;
            }

            let obj = notyMsg.withdrawPaymentFail(peopleInst, transInst, token.ln);
            var payload = {
              data: obj,
              notification: obj
            };

            console.log(payload);

            payload.notification.click_action =  ".Activity.NotificationJobActivity";

            admin.messaging().sendToDevice(token.firebaseToken, payload)
              .then(function(response) {
                cb(null, response);
              })
              .catch(function(error) {
                cb(error, null);
              });
          })
          console.log("********************** withdrawPaymentFail ***********************");
        })    
      }
    })    
  }

  Notification.remoteMethod("withdrawPaymentFail",{
    accepts : [
      {arg:"peopleInst",type:"object"},
      {arg:"transInst",type:"object"}
    ],
    returns : {arg:"success",type:"object"}
  })


  Notification.userApprovalNoty = function(peopleInst,adminInst,cb){
    Notification.app.models.AccessToken.find({
      where:{
        userId:peopleInst.id
      }
    }, function(err, tokens) {
      if (err)
        cb(err, null);
      else {
        Notification.create({
          type:peopleInst.adminVerifiedStatus == "approved"? "user_approved":"user_rejected",
          peopleId:peopleInst.id,
          creatorId:adminInst.id,
          createdAt:new Date(),
          updatedAt:new Date()
        },function(error,success){
          if(error) return cb(error,null);

          updateNotyCount(peopleInst.id); 
          console.log("************* userApprovalNoty ***************");
          tokens.forEach(function(token) {
            
            if (!token.firebaseToken) {
              return;
            }

            let obj = notyMsg.userApprovalNoty(peopleInst, adminInst, success, token.ln);
            var payload = {
              data: obj,
              notification: obj
            };
            console.log(payload);
            payload.notification.click_action =  ".Activity.NotificationJobActivity";

            admin.messaging().sendToDevice(token.firebaseToken, payload)
              .then(function(response) {
                cb(null, response);
              })
              .catch(function(error) {
                cb(error, null);
              });
          })
          console.log("************* userApprovalNoty ***************");
        })    
      }
    })    
  }

  Notification.remoteMethod("userApprovalNoty",{
    accepts : [
      {arg:"peopleInst",type:"object"},
      {arg:"adminInst",type:"object"}
    ],
    returns : {arg:"success",type:"object"}
  })

  Notification.jobApprovalNoty = function(jobInst,peopleInst,adminInst,cb){
    Notification.app.models.AccessToken.find({
      where:{
        userId:peopleInst.id
      }
    }, function(err, tokens) {
      if (err)
        cb(err, null);
      else {
        Notification.create({
          type:jobInst.jobApproveStatus == "approved"? "job_approved":"job_rejected",
          peopleId:peopleInst.id,
          creatorId:adminInst.id,
          createdAt:new Date(),
          updatedAt:new Date()
        },function(error,success){
          if(error) return cb(error,null);

          updateNotyCount(peopleInst.id); 
          console.log("********************* jobApprovalNoty ****************************");
          tokens.forEach(function(token) {
            if (!token.firebaseToken) {
              return;
            }

            let obj = notyMsg.jobApprovalNoty(jobInst,peopleInst, adminInst, success, token.ln);
            var payload = {
              data: obj,
              notification: obj
            };
            console.log(payload);
            payload.notification.click_action =  ".Activity.NotificationJobActivity";

            admin.messaging().sendToDevice(token.firebaseToken, payload)
              .then(function(response) {
                cb(null, response);
              })
              .catch(function(error) {
                cb(error, null);
              });
          })
          console.log("********************* jobApprovalNoty ****************************");
        })    
      }
    })    
  }

  Notification.remoteMethod("jobApprovalNoty",{
    accepts : [
      {arg:"jobInst",type:"object"},
      {arg:"peopleInst",type:"object"},
      {arg:"adminInst",type:"object"}
    ],
    returns : {arg:"success",type:"object"}
  })


  //******************************************************less wallet amount in owner in fuel request**************************


  Notification.lessWalletAmtOwner = function(jobInst, cb) {
    Notification.app.models.People.findById(jobInst.ownerId, {
      include: {
        relation: "accessTokens"
      }
    }, function(error, userInst) {
      if (error) return cb(error, null);
      let owner = userInst.toJSON();
      
      Notification.create({
        type:"fuel_request_low_balance",
        peopleId:owner.id,
        creatorId:jobInst.driverId,
        jobId:jobInst.id,
        createdAt:new Date(),
        updatedAt:new Date()
      },function(error,success){

        if(error) return cb(error,null);

        updateNotyCount(owner.id);
        console.log("********************* lessWalletAmtOwner **************************");
        owner.accessTokens.forEach(function(token) {
          if (!token.firebaseToken)
            return;

          let obj = notyMsg.applyJobNoty(jobInst, owner, token.ln);
          var payload = {
            data: obj,
            notification: obj
          };

          console.log(payload);

          payload.notification.click_action =  ".Activity.NotificationJobActivity";

          admin.messaging().sendToDevice(token.firebaseToken, payload)
          .then(function(response) {
            // console.log(response.results);
            cb(null, response);
          })
          .catch(function(error) {
            // console.log(error)
            cb(error, null);
          });
        })
        console.log("********************* lessWalletAmtOwner **************************");
      })

    })
  }

  Notification.remoteMethod("lessWalletAmtOwner", {
    accepts: [
      { arg: "jobInst", type: "object" }
    ],
    returns: { arg: "success", type: "object" }
  })



  
  // this api call for getting all type of notification 

  Notification.getNotifications = function(filter,req,cb){
    let peopleId = req.accessToken.userId;
    let ln = req.accessToken.ln || "en";
    if(!filter)
      filter = {};
    
    if(!filter.where)
      filter.where = {};

    filter.include = [
      {
        relation:"vehicle"
      },
      {
        relation:"creator"
      },
      {
        relation:"people"
      },
      {
        relation:"job"
      },
      {
        relation:"transaction"
      }
    ]

    filter.order = "createdAt DESC";
    filter.where.peopleId = peopleId;  
     
    Notification.find(filter,function(error,notifications){
      if(error)
        cb(error,null);
      else{
        let notyArr = [];
        notifications.forEach(function(value){
          let noty = value.toJSON();
          noty.job = noty.job || {};
          noty.vehicle = noty.vehicle || {}; 
          noty.creator = noty.creator || {}; 
          noty.people = noty.people || {}; 
          noty.transaction = noty.transaction || {}; 

          if(noty.type == "new_job"){
            notyArr.push(notyMsg.addJobNoty(noty.job,noty.creator,ln));
          }else if(noty.type == "apply_job"){
              notyArr.push(notyMsg.applyJobNoty(noty.job,noty.creator,ln));
          }else if(noty.type == "owner_request"){
            notyArr.push(notyMsg.makeRequestNoty(noty.people,noty.vehicle,noty.creator.id,ln));
          }else if(noty.type == "driver_confirmation"){
            notyArr.push(notyMsg.confirmByDriver(noty.creator,ln));
          }else if(noty.type == "start_trip"){
            notyArr.push(notyMsg.startTripNoty(noty.people,noty.creator,noty.job,ln));
          }else if(noty.type == "end_trip"){
            notyArr.push(notyMsg.endTripNoty(noty.people,noty.creator,noty.job,ln));
          }else if(noty.type == "trip_complete_trans"){
            notyArr.push(notyMsg.jobCompTransNoty(noty.people,noty.creator,noty.job,noty.transaction,ln));
          }else if(noty.type == "add_payment_success"){
            notyArr.push(notyMsg.addPaymentSuccess(noty.people,noty.transaction,ln));
          }else if(noty.type == "add_payment_fail"){
            notyArr.push(notyMsg.addPaymentFail(noty.people,noty.transaction,ln));
          }else if(noty.type == "user_approved" || noty.type == "user_rejected"){
            notyArr.push(notyMsg.addPaymentFail(noty.people,noty.creator,noty,ln));
          }else if(noty.type == "job_approved" || noty.type == "job_rejected"){
            notyArr.push(notyMsg.addPaymentFail(noty.job,noty.people,noty.creator,noty,ln));
          }else if(noty.type == "fuel_request_low_balance"){
            notyArr.push(notyMsg.lessAmountFuel(noty.job,noty.people,ln));
          }
        })
        cb(null,{data:notyArr,msg:msg.getNotifications});
      }
    })
  }

  Notification.remoteMethod("getNotifications",{
    accepts : [
      {arg:"filter",type:"object",http:{source:"body"}},
      {arg:"req",type:"object",http:{source:"req"}}
    ],
    returns : {arg:"success",type:"object"},
    http:{verb:"get"}
  })

  // this api call for read notification count put 0

  Notification.readAllNoty = function(req,cb){
    let peopleId = req.accessToken.userId;
    Notification.app.models.People.findById(peopleId,function(error,peopleInst){
      if(error) return cb(error,null);
      peopleInst.newNotification = 0;
      peopleInst.save(function(errorV,success){
        if(errorV) return cb(error,null);
        cb(null,{data:success,msg:msg.readAllNoty});
      })
    })
  }

  Notification.remoteMethod("readAllNoty",{
    accepts : [
      {arg:"req",type:"object",http:{source:"req"}}
    ],
    returns : {arg:"success",type:"object"},
    http:{verb:"get"}
  })

};

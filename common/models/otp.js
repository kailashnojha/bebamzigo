'use strict';

var globalFunction = require("../services/global-function");
var msg = require("../messages/otp-msg.json");
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var gf = require("../services/global-function");
const request = require("request");
module.exports = function(Otp) {

	disableAllMethods(Otp, ["find"]);

	

	Otp.sendSMS = function(smsData,cb){
		var msg = "Your OTP is "+smsData.otp;
		var data = JSON.stringify({
		  "from": "BEBAMZIGO",
		  "to": smsData.mobile,
		  "text": msg
		});
		var apiKey = "App 940b3faf63a58529f75c114aeff7f758-7b9a0c75-804d-4ec9-bd0c-2b71f209d1f7";
		var url = "https://8p3w9.api.infobip.com/sms/1/text/single"
		url = encodeURI(url);
		
		
		//request.body = data;
		//request.setRequestHeader("authorization", apiKey);
		/*request.post(url,function(error, response, body){
				if(error)
					return cb(error,null);
				else
					cb(null,{success:body});
			}
		)*/	
		request({
		    url: url,
		    method: "POST",
		    headers: {
		        "content-type": "application/json",
		        "Authorization": apiKey
		        },
		    json : true,
		    body : data
		
		    },function(error, response, body){
				if(error)
					return cb(error,null);
				else
					cb(null,{success:body});
					console.log("sms api response:::", response);
					console.log("sms api body :::", body);
			}
		)

		
		
	}

	Otp.remoteMethod("sendSMS",{
		accepts : [
		  	{"arg":"smsData","type":"object",http:{source:"body"}}
		],
		returns : {arg:"success",type:"object"}
	})


	Otp.resendOtp = function(peopleId,type,cb){
		type == "signup";
		Otp.app.models.People.findById(peopleId,function(err, peopleInstance){
			if(err)
				cb(err,null);
			else{

				if(!peopleInstance) return cb(new Error("No user found"),null);

				if(type === "signup"){
					peopleInstance.mobOtp = {
						createdAt : new Date(),
						expireAt  : new Date(),
						otp       : gf.getOTP()
					}
					peopleInstance.mobOtp.expireAt.setMinutes(peopleInstance.mobOtp.expireAt.getMinutes()+5);
				}else{
					if(type === "reset"){
						peopleInstance.resetOtp = {
							createdAt : new Date(),
							expireAt  : new Date(),
							otp       : gf.getOTP()
						}
						peopleInstance.resetOtp.expireAt.setMinutes(peopleInstance.resetOtp.expireAt.getMinutes()+5);
					}
					else
						return cb(new Error("Invalid type"),null);
				}
				peopleInstance.save(function(err, peopleInst){
					if(err)
						cb(err,null);
					else{
						Otp.sendSMS({
                          otp:type === "reset"?peopleInstance.resetOtp:peopleInstance.otp,
                          mobile:peopleInst.mobile,
                          type:"mobileVerified"
                        },function(err,success){
                        	if(err)
                        		cb(err,null);
                        	else
                        		cb(null,{data:peopleInst,msg:msg.resendOtp});
                      	})
					}
				})
			}
		})	
	}

	Otp.remoteMethod("resendOtp",{
		accepts : [
			{arg:'peopleId',type:"string",required:true},
			{arg:'type',type:"string"}
		],
		returns : {arg:"success",type:"object"}
	})

	Otp.checkResetOtp = function(peopleId,otp,cb){
		Otp.app.models.People.findById(peopleId,function(errorP,peopleInst){
			if(errorP) return cb(errorP,null);
			if(!peopleInst) return cb(null,peopleInst);

			if(peopleInst.resetOtp.otp == otp)
				cb(null,{data:peopleInst,msg:msg.checkResetOtp});
			else
				cb(new Error("Otp doesn't match"),null);
		})
	}

	Otp.remoteMethod("checkResetOtp",{
		accepts : [
			{arg:"peopleId",type:"string",required:true},
			{arg:"otp",type:"number",required:true}
		],
		returns : {arg:"success",type:"object"},
		http:{verb:"get"}
	})

	// this api used for verification of mobile number
	Otp.verifyMobile = function(peopleId,otp,cb){
		Otp.app.models.People.findById(peopleId,function(errorP,peopleInst){
			if(errorP) return cb(errorP,null);
			
			if(!peopleInst) return cb(new Error("No user found"),null);
			peopleInst.mobOtp = peopleInst.mobOtp || {}; 
			let currentDate = new Date();
			if(peopleInst.mobOtp.expireAt > currentDate){
				if(peopleInst.mobOtp.otp == otp){
					peopleInst.mobOtp = {};
					peopleInst.mobileVerified = true;
					peopleInst.save(function(errorUP,updatedInst){
						if(errorUP) return cb(errorUP,null);
						
						cb(null,{data:updatedInst,msg:msg.verifyMobile})
					})		
				}else{
					cb(new Error("Otp doesn't match"),null);
				}				
			}else{
				cb(new Error("otp expired, please resend otp"),null);
			}

		})
	}

	Otp.remoteMethod("verifyMobile",{
		accepts : [
			{arg:"peopleId",type:"string",required:true,http:{source:"form"}},
			{arg:"otp",type:"number",required:true,http:{source:"form"}}
		],
		returns : {arg:"success",type:"object"}
	})

	Otp.checkOTP = function(userId,otp,obj,cb){
		Otp.app.models.User.findById(obj.userId,function(error,userInstance){
			if(error)
				cb(error,null);
			else{
				if(userInstance){
					if(obj.otp){
						mobileVerified(userInstance);
					}else{
						if(obj.resetOtp){
							resetOtpMatch(userInstance);
						}else{
							cb(new Error("Otp doesn't match"),null);
						}
					}
				}
				else{
					cb(new Error("No user found"),null);
				}				
			}	

			function mobileVerified(userInstance){
				if(userInstance.otp && userInstance.otp.toString() === obj.otp.toString()){
					userInstance.updateAttributes({mobileVerified:true,otp:null},function(error,success){
						if(error)
							cb(error,null);
						else{
							cb(null,{data: success, msg: msg.checkOtpSuccess});
						}
					})
					/*var options = {
						ln: obj.ln,
						offset:obj.offset
					}*/
					// userInstance.createAccessToken(-1, options, tokenHandler);
				}
				else{
					cb(new Error("Otp doesn't match"),null);
				}		
			}

			/*function resetOtpMatch(userInstance){
				if(userInstance.resetOtp && userInstance.resetOtp.toString() === obj.resetOtp.toString()){
					cb(null,{data:userInstance,msg:msg.checkResetOTPSuccess});
				}
				else{
					cb(new Error("Otp not matched"),null);
				}		
			}*/

			/*function tokenHandler(err, token) {
		        if (err) return cb(err,null);
		     
		        if (obj.referenceToken && obj.deviceId) {
		            if (err)
		              cb(err, null);
		            else {
		              token.deviceId = obj.deviceId;
		              token.referenceToken = obj.referenceToken;
		              token.save(function(error, tokenWithFCM) {
		                if (err) {
		                  fn(err, null);
		                } else {
		                  tokenWithFCM.userDetails = userInstance;
		                  updateUser(tokenWithFCM);	  
		                }
		              })
		            }
		        } else{
		          token.userDetails = userInstance;
		          updateUser(token);
		        }
		    }

		    function updateUser(token){
		    	userInstance.updateAttributes({mobileVerified:true,otp:null},function(error,success){
					if(error)
						cb(error,null);
					else{
						cb(null,{data:token,msg:msg.checkOTPSuccess});

						Otp.app.models.Notification.find({
							where:{
								sortedDate   : {gt:new Date()},
								userMobile   : userInstance.mobile	
							},
							include : {relation:"post",scope:{fields: {"isPrivate":true}}} 
						},function(err,noties){
							if(err)
								cb(err,null);
							else{
								noties.forEach(function(noty){
									updateNoty(noty)
								})
							}
						})


						
						function updateNoty(noty){
							let obj = {};
							let notyJSON = noty.toJSON();
							obj = {
								$set:{
									userId:token.userId,
									isRegistered:true
								}
							}
							noty.updateAttributes(
								obj,{allowExtendedOperators:true},
							function(err,success){
								if(success){
									Otp.app.models.ChatGroup.findOne({where:{postId:noty.postId}},function(err,chatGroup){
										if(chatGroup){
											chatGroup.addNewUsers([token.userId],function(){});
										}	
									})
								}
							})
						}
					}
				})
		    }*/
		})
	}

	Otp.remoteMethod("checkOTP",{
		accepts : {arg: "obj", type: 'object',http:{source:"body"}},
		returns : {arg:'success',type: 'object'}
	})
};

'use strict';
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var gf = require("../services/global-function");
var msg = require("../messages/people-msg.json");
var loopback = require("loopback");
var path = require("path");
var config = require("../../server/config.json");
module.exports = function(People) {
disableAllMethods(People, ['find', "login", "logout", "confirm", "changePassword","deleteById"]);
People.validatesInclusionOf('realm', { in: ['super_admin', 'admin', 'owner', 'customer', 'driver'] });
People.validatesLengthOf('password', { min: 8, message: { min: 'Password is too short' } });
People.validatesInclusionOf('adminVerifiedStatus', { in: ['pending', 'rejected', 'approved'] });

People.prototype.addFirebaseToken = function(token, ln, cb) {
  let self = this;
  if (typeof ln == 'function') {
    cb = ln;
    ln == "en";
  }
  cb(null, self);

  /*if(token){
    self.firebaseTokens.push({
      token:token,
      ln:ln
    });
    self.save(function(err,userInst){
      if(err) return cb(err,null);
      cb(null,userInst);
    })  
  }else{
    cb(null,self);
  }*/
}

People.updateToken = function(req, firebaseToken, cb) {
  let accessToken = req.accessToken;
  accessToken.firebaseToken = firebaseToken;
  accessToken.save(function(error, success) {
    if (error) return cb(error, null);
    cb(null, { data: success, msg: msg.accessToken });
  })
}

People.remoteMethod("updateToken", {
  accepts: [
    { arg: "req", type: "object", http: { source: "req" } },
    { arg: "firebaseToken", type: "string", required: true, http: { source: "form" } }
  ],
  returns: { arg: "success", type: "object" }
})

People.updateResetPassword = function(peopleId,otp,newPassword,cb){
  People.findById(peopleId,function(errorP,peopleInst){
    if(errorP) return cb(errorP,null);
    if(!peopleInst) return cb(new Error("No user found"),null);

    if(peopleInst.resetOtp.otp == otp){
      peopleInst.resetOtp = {};
      peopleInst.password = newPassword;
      peopleInst.save(function(error,success){
        if(error) return cb(error,null);
        cb(null,{data:peopleInst,msg:msg.updateResetPassword});
      })
    }else{
      cb(new Error("OTP does not match"),null);
    }
  })
}

People.remoteMethod("updateResetPassword",{
  accepts : [
    {arg:"peopleId",type:"string",required:true,http:{source:"form"}},
    {arg:"otp",type:"number",required:true,http:{source:"form"}},
    {arg:"newPassword",type:"string",required:true,http:{source:"form"}}
  ],
  returns : {arg:"success",type:"object"}
})

People.resetPasswordRequest = function(mobile,type,cb){

  People.findOne({
      where:{
       "mobile" : mobile,
       "realm"   : type 
    }
  },function(errorP,peopleInst){
    if(errorP) return cb(errorP,null);
    if(!peopleInst) return cb(new Error("Account does not exist"),null);

    peopleInst.resetOtp = {
      createdAt : new Date(),
      expireAt  : new Date(),
      otp       : gf.getOTP()
    }

    peopleInst.resetOtp.expireAt.setMinutes(peopleInst.resetOtp.expireAt.getMinutes()+5);

    peopleInst.save(function(errorUP,updateInst){
      if(errorUP) return cb(errorUP,null);
      People.app.models.Otp.sendSMS({otp:peopleInst.resetOtp.otp,mobile:peopleInst.mobile},function(error,success){});
      cb(null,{data:updateInst,msg:msg.resetPasswordRequest});
    })

  })
}

People.remoteMethod("resetPasswordRequest",{
  accepts : [
    {arg:"mobile",type:"string",http:{source : "form"}},
    {arg:"type",type:"string",http:{source : "form"}}
  ],
  returns : {arg:"success",type:"object"}
})

People.getDriverCurrentLoc = function(driverId, cb){
  People.findById(driverId,function(error,driver){
    if(error) return cb(error,null);
    if(driver){
      cb(null,{data:driver.location,msg:msg.getDriverCurrentLoc});  
    }else{
      cb(new Error("No driver found"),null);
    }
    
  })
}

People.remoteMethod("getDriverCurrentLoc",{
  accepts : [
    {arg: "driverId",type:"string",required:true}
  ],
  returns : {arg:"success", type:"object"},
  http:{verb:"get"}
})

People.editProfile = function(name, mobile, email, req, cb) {

  let obj = {
    name: name,
    mobile: mobile,
    email: email
  }

  let peopleId = req.accessToken.userId;
  People.findById(peopleId, function(err, peopleInst) {
    if (err) return cb(err, null);

    if(!peopleInst.mobileVerified){
      obj.mobOtp = {
        createdAt : new Date(),
        expireAt  : new Date(),
        otp       : gf.getOTP()
      }
      obj.mobOtp.expireAt.setMinutes(obj.mobOtp.expireAt.getMinutes()+5);
    }

    peopleInst.updateAttributes(obj, function(err, success) {
      if (err) return cb(err, null);

      cb(null, { data: success, msg: msg.editProfile });
    })

  })
}

People.remoteMethod("editProfile", {
  accepts: [
    { arg: "name", type: "string", http: { source: "form" } },
    { arg: "mobile", type: "string", http: { source: "form" } },
    { arg: "email", type: "string", http: { source: "form" } },
    { arg: "req", type: "object", http: { source: "req" } }
  ],
  returns: { arg: "success", type: "object" }
})


People.afterRemote("editProfile",function(ctx, userData, next){
  let modelInstance = userData.success.data;

  if(!modelInstance.mobileVerified){
    People.app.models.Otp.sendSMS({otp:modelInstance.mobOtp.otp,mobile:modelInstance.mobile},function(){});
  }

  var options = {
    type: 'email',
    to: modelInstance.email,
    from: 'noreply@loopback.com',
    subject: 'Verification Link',
    redirect: '/user-verify/verified',
    user: People,
    template: path.resolve(__dirname, '../../server/views/verify-email.ejs')
  };

  if (!modelInstance.emailVerified) {
    modelInstance.verify(options, function(err, response, next) {
      if (err) return nextUser(err);
      console.log('> verification email sent:', response);
    });
  }
  next();
})

People.updateDriverLoc = function(req, location, cb) {
  let driverId = req.accessToken.userId;
  People.findById(driverId, function(error, peopleInst) {
    if (error) return cb(error, null);

    peopleInst.location = gf.getLocationObj(location);
    peopleInst.save(function(err, updateInst) {
      if (err) return cb(err, null);
      cb(null, { data: updateInst, msg: msg.updateDriverLoc });
    })
  })
}

People.remoteMethod("updateDriverLoc", {
  accepts: [
    { arg: "req", type: "object", http: { source: "req" } },
    { arg: "location", type: "GeoPoint", required: true, http: { source: "form" } }
  ],
  returns: { arg: "success", type: "object" }
})

People.uploadProfilePic = function(req, res, cb){
  let peopleId = req.accessToken.userId;
  function uploadFile() {
    People.app.models.Container.imageUpload(req, res, { container: 'images' }, function(err, success) {
      if (err)
        cb(err, null);
      else {
        updateUser(success);
      }
    })
  }

  function updateUser(passObj) {
    var data = {};
    if (passObj.files) {
      if (passObj.files.profileImage)
        data.profileImage = passObj.files.profileImage[0].url;
    }

    if(!data.profileImage)
      return cb(new Error("No image found"),null);

    People.findById(peopleId,function(error, peopleInst){
      if(error) return cb(error,null);

      peopleInst.updateAttributes(data, function(err, success) {
        if (err) return cb(err, null);
        cb(null, { data: success, msg: msg.uploadProfilePic });
      })
    })
  }  

  uploadFile();

}

People.remoteMethod("uploadProfilePic",{
  accepts : [
    { arg:"req", type:"object", http:{source:"req"}},
    { arg:"res", type:"object", http:{source:"res"}}
  ],
  returns : {arg:"success", type:"object"}
})

People.uploadDriverDoc = function(req, res, cb) {
  let peopleId = req.accessToken.userId;

  function uploadFile() {
    People.app.models.Container.imageUpload(req, res, { container: 'images' }, function(err, success) {
      if (err)
        cb(err, null);
      else {
        updateUser(success);
      }
    })
  }

  function updateUser(passObj) {
    People.findById(peopleId, function(err, userInst) {
      if (err) return cb(err, null);
      let obj = {
        licenceIds: userInst.licenceIds ? userInst.licenceIds.slice() : [],
        governmentIds: userInst.governmentIds ? userInst.governmentIds.slice() : []
      }

      if (passObj.files) {
        if (passObj.files.licenceIds || passObj.files.governmentIds) {
          if (passObj.files.licenceIds) {
            passObj.files.licenceIds.forEach(function(value) {
              obj.licenceIds.push(value.url);
            })
          }

          if (passObj.files.governmentIds) {
            passObj.files.governmentIds.forEach(function(value) {
              obj.governmentIds.push(value.url);
            })
          }
        } else {
          cb(new Error("No document uploaded"), null);
        }

      } else {
        return cb(new Error("No document uploaded"), null);
      }
      userInst.updateAttributes(obj, function(err, userData) {
        if (err) return cb(err, null);
        // console.log(passObj)
        cb(null, { data: userData, msg: msg.driverDocUpdate });
      })
    })
  }
  uploadFile();
}

People.remoteMethod("uploadDriverDoc", {
  accepts: [
    { arg: "req", type: "object", http: { source: "req" } },
    { arg: "res", type: "object", http: { source: "res" } }
  ],
  returns: { arg: "success", type: "object" }
})


People.getMyInfo = function(req, cb) {
  let peopleId = req.accessToken.userId;
  People.findById(peopleId,{include:"vehicle"}, function(err, success) {
    if (err)
      cb(err, null);
    else
      cb(null, { data: success, msg: msg.getMyInfo });
  })
}

People.remoteMethod("getMyInfo", {
  accepts: [
    { arg: "req", type: "object", http: { source: "req" } }
  ],
  returns: { arg: "success", type: "object" },
  http: { verb: "get" }
})

People.getPeopleInfo = function(peopleId, cb) {
  People.findById(peopleId,{include:"vehicle"}, function(err, success) {
    if (err)
      cb(err, null);
    else
      cb(null, { data: success, msg: msg.getPeopleInfo });
  })
}

People.remoteMethod("getPeopleInfo", {
  accepts: [
    { arg: "peopleId", type: "string", required: true }
  ],
  returns: { arg: "success", type: "object" },
  http: { verb: "get" }
})


People.createAdmin = function(realm, name, email, mobile, address, password, cb) {

  var data = {
    realm: realm,
    name: name,
    email: email,
    mobile: mobile,
    address: address,
    password: password
  }

  data.createdAt = new Date();
  data.updatedAt = new Date();
  data.mobileVerified = false;
  data.emailVerified = false;

  People.create(data, function(err, admin_Created) {
    if (err) {
      cb(err, null)
    } else {
      cb(null, { data: admin_Created, msg: msg.adminCreateSuccess });
    }
  })
}

People.remoteMethod('createAdmin', {
  accepts: [
    { arg: 'realm', type: 'string', required: true, 'http': { source: 'form' } },
    { arg: 'name', type: 'string', required: true, 'http': { source: 'form' } },
    { arg: 'email', type: 'string', required: true, 'http': { source: 'form' } },
    { arg: 'mobile', type: 'string', required: true, 'http': { source: 'form' } },
    { arg: 'address', type: 'string', required: true, 'http': { source: 'form' } },
    { arg: 'password', type: 'string', required: true, 'http': { source: 'form' } }
  ],
  returns: { arg: "success", type: "object", "root": true },
  http: { verb: 'post' }
})

People.signup = function(req, res, cb) {
  
  function uploadFile() {
    People.app.models.Container.imageUpload(req, res, { container: 'images' }, function(err, success) {
      if (err)
        cb(err, null);
      else {
        if (success.data) {
          success.data = JSON.parse(success.data);
          createUser(success);
        } else {
          cb(new Error("data not found"), null);
        }
      }
    })
  }

  function createUser(passObj) {
    
    var data = {
      realm               : passObj.data.realm,
      name                : passObj.data.name,
      email               : passObj.data.email,
      mobile              : passObj.data.mobile,
      address             : passObj.data.address,
      password            : passObj.data.password,
      adminVerifiedStatus : "approved",//"pending",
      isProfileComplete   : true
    };

    data.mobOtp = {
      createdAt : new Date(),
      expireAt  : new Date(),
      otp       : gf.getOTP()
    }
    data.mobOtp.expireAt.setMinutes(data.mobOtp.expireAt.getMinutes()+5); 

    if (passObj.files) {
      if (passObj.files.profileImage)
        data.profileImage = passObj.files.profileImage[0].url;
    }

    if (passObj.data.realm == 'owner') {
  
      data.companyName = passObj.data.companyName;
      
      if (passObj.files.logBook)
        data.logBook = gf.getImageArray(passObj.files.logBook);

      if (passObj.files.copyOfPin)
        data.copyOfPin = gf.getImageArray(passObj.files.copyOfPin);

      if (passObj.files.companyCertificate)
        data.companyCertificate = gf.getImageArray(passObj.files.companyCertificate);
    } else {
      if (passObj.data.realm == 'driver') {
       
        data.isVehicleAlloted = false;
        data.canDrive = passObj.data.canDrive;

        if (passObj.files) {
          if (passObj.files.licenceIds)
            data.licenceIds = gf.getImageArray(passObj.files.licenceIds);

          if (passObj.files.governmentIds)
            data.governmentIds = gf.getImageArray(passObj.files.governmentIds);

        }
      }
    }

    if (passObj.data.realm == 'customer') {
      data.adminVerifiedStatus = "approved";
    }

    data.emailVerified = true;
    data.createdAt = new Date();
    data.updatedAt = new Date();
    data.mobileVerified = false;

    //People.createAllUsers(passObj);

    // This script for store ids

    if(data.realm=='driver'){
      if(data.canDrive && data.canDrive.length > 0 ){
        People.app.models.VehicleType.find({where:{name:{inq:data.canDrive}}},function(error,success){
          if(error) return cb(error,null);
          data.canDrive = [];
          data.vehicleTypeIds = [];
          success.forEach(function(value){
            data.canDrive.push(value.name);
            data.vehicleTypeIds.push(value.id);
          })

          if(data.vehicleTypeIds.length > 0){
            isAdminCreated();
            // createOrUpdateUser();
          }else{
            return cb(new Error("Please select vehicles"),null);
          }
        })
      }else{
        return cb(new Error("Please select vehicles"),null);
      }

    }else{
      isAdminCreated();
      // createOrUpdateUser();
    }
    // createOrUpdateUser()

    function isAdminCreated(){
      if(req.accessToken){
        let adminId = req.accessToken.userId;

        People.findById(adminId,function(error,adminInst){
          if(error) return cb(error,null);
          if(["admin","super_admin"].indexOf(adminInst.realm)!=-1){
            data.adminId = adminInst.id;
            data.adminVerifiedStatus = "approved";
            data.emailVerified = true;
            data.mobileVerified = true;
            createOrUpdateUser();
          }else{
            createOrUpdateUser();
          }
        })
      }else{
        createOrUpdateUser();
      }
    }

    function createOrUpdateUser(){
      if (!passObj.data.peopleId) {
        People.create(data, function(err, success) {
          if (err)
            cb(err, null);
          else
            cb(null, { data: success, msg: msg.signupSuccess });
        })
      } else {
        People.findById(passObj.data.peopleId, function(error, peopleInst) {
          if (error) return cb(error, null);
          if (!peopleInst) return cb(new Error("No user found"), null);
          data.emailVerified = data.email == peopleInst.email ? true : false;
          peopleInst.updateAttributes(data, function(err, success) {
            if (err) return cb(err, null);
            cb(null, { data: success, msg: msg.signupSuccess });
          })
        })
      }      
    }
  }
  uploadFile();
}

People.afterRemote('signup', function(context, instance, nextUser) {

  var userInstance = instance.success.data;

  var options = {
    type: 'email',
    to: userInstance.email,
    from: 'noreply@loopback.com',
    subject: 'Thanks for registering.',
    redirect: '/user-verify/verified',
    user: People,
    template: path.resolve(__dirname, '../../server/views/verify.ejs')
  };

  userInstance.mobOtp = userInstance.mobOtp || {};
  
  userInstance.addRole(userInstance.realm, function(error, success) {

    if(!userInstance.mobileVerified)
      People.app.models.Otp.sendSMS({otp:userInstance.mobOtp.otp,mobile:userInstance.mobile},function(error,success){});

    if (!userInstance.emailVerified) {
      userInstance.verify(options, function(err, response, next) {
        if (err) return nextUser(err);
        console.log('> verification email sent:', response);
        nextUser();
      });
    } else {
      nextUser();
    }
  });    

});

People.remoteMethod(
  'signup', {
    description: 'Signup users',
    accepts: [
      { arg: 'req', type: 'object', http: { source: 'req' } },
      { arg: 'res', type: 'object', http: { source: 'res' } }
    ],
    returns: {
      arg: 'success',
      type: 'object'
    },
    http: { verb: 'post' },
  }
);
//**************************************************************** new code************************************************

People.resendOtp = function(peopleId,cb){
   
    People.findById(peopleId,function(err, peopleInstance){
      if(err)
        cb(err,null);
      else{

        if(!peopleInstance) return cb(new Error("No user found"),null);
            if(peopleInstance.mobileNumber)
            {
            peopleInstance.mobOtp = {
                createdAt : new Date(),
                expireAt  : new Date(),
                otp       : gf.getOTP()
              }

              peopleInstance.mobOtp.expireAt.setMinutes(peopleInstance.mobOtp.expireAt.getMinutes()+5);

              People.app.models.Otp.sendSMS({otp:peopleInstance.mobOtp.otp,mobile:peopleInstance.mobileNumber},function(error,success){});
            
              peopleInstance.save(function(err, peopleInst){
              if(err)
                cb(err,null);
              else{
               
                cb(null,{data:peopleInst,msg:msg.resendOtp});
              }
            })
      }

      else{
        peopleInstance.mobOtp = {
          createdAt : new Date(),
          expireAt  : new Date(),
          otp       : gf.getOTP()
      }

          peopleInstance.mobOtp.expireAt.setMinutes(peopleInstance.mobOtp.expireAt.getMinutes()+5);

          People.app.models.Otp.sendSMS({otp:peopleInstance.mobOtp.otp,mobile:peopleInstance.mobile},function(error,success){});
        
          peopleInstance.save(function(err, peopleInst){
          if(err)
            cb(err,null);
          else{
           
            cb(null,{data:peopleInst,msg:msg.resendOtp});
          }
         })
        }
          
      }
    })  
  }

  People.remoteMethod("resendOtp",{
    accepts : [
      {arg:'peopleId',type:"string",required:true},
      ],
    returns : {arg:"success",type:"object"}
  })


  //**************************************************************** new code************************************************



People.adminCreateUser = function(req,res,cb){
  People.signup(req,res,cb);
}


People.remoteMethod(
  'adminCreateUser', {
    description: 'Create User By Admin and Super Admin Signup users',
    accepts: [
      { arg: 'req', type: 'object', http: { source: 'req' } },
      { arg: 'res', type: 'object', http: { source: 'res' } }
    ],
    returns: {
      arg: 'success',
      type: 'object'
    },
    http: { verb: 'post' },
  }
);



People.prototype.addRole = function(role, cb) {
  let self = this;
  People.app.models.Role.findOne({ where: { name: role } }, function(error, roleInst) {
    if (error)
      cb(err, null);
    else {
      if (roleInst) {
        function createRole() {
          People.app.models.RoleMapping.destroyAll({ principalId: self.id }, function(err, success) {
            if (err)
              return cb(err, null);

            roleInst.principals.create({
              principalType: People.app.models.RoleMapping.USER,
              principalId: self.id
            }, function(err, principal) {
              if (err) return cb(err, null);
              cb(null, principal);
            });
          })
        }
        createRole();
      } else {
        cb(new Error("No role found"), null);
      }
    }
  })
}

function createVerificationEmailBody(verifyOptions, options, cb) {
  var template = loopback.template(verifyOptions.template);
  var body = template(verifyOptions);
  cb(null, body);
}

People.viewProfile = function(req, res, cb) {
  var userInst;
  var self = this;
  self.findById(req.accessToken.userId, function(error, userInstance) {
    if (error)
      cb(error, null);
    else {
      userInst = userInstance;
      cb(null, userInst)
    }
  })
}

People.remoteMethod('viewProfile', {
  description: 'profile details',
  accepts: [
    { arg: 'req', type: 'object', required: true, 'http': { source: 'req' } },
    { arg: 'res', type: 'object', required: true, 'http': { source: 'res' } }
  ],
  returns: { arg: "userInst", type: "object", "root": true },
  http: { verb: 'post' }
})

People.userApproval = function(req,peopleId,adminVerifiedStatus,reason, cb) {
  let adminId = req.accessToken.userId;
  People.findById(peopleId, function(error, peopleInst) {

    if (error) return cb(error, null);
    if(!peopleInst) return cb(new Error("No user found"),null);

    peopleInst.adminVerifiedStatus = adminVerifiedStatus;
    peopleInst.reason = reason;
    peopleInst.approvedBy = adminId;
    People.findById(adminId,function(errorA,adminInst){
      if(errorA) return cb(errorA,null);
    
      peopleInst.save(function(err, success) {
        if (err) return cb(error, null);

        People.app.models.Notification.userApprovalNoty(peopleInst,adminInst, function() {});      

        cb(null, { data: success, msg: msg.userApproval });
      })

    })  

  })
}

People.remoteMethod("userApproval", {
  accepts: [
    { arg: "req", type: "object", http: { source: "req" } },
    { arg: "peopleId", type: "string",required:true, http: { source: "form" } },
    { arg: "adminVerifiedStatus", type: "string", required:true, http: { source: "form" } },
    { arg: "reason", type: "string", http: { source: "form" } }
  ],
  returns: { arg: "success", type: "object" }
})

People.findNearDriver = function(req, position, vehicleType, vehicleId, skip, limit, cb) {
  console.log("position::",position)
  let loc ={ lat :position.lat, lng: position.lng };

  
  let ownerId = req.accessToken.userId;
  skip = skip || 0;
  limit = limit || 15;
  People.find({
    fields: {
      id: true,
      realm: true,
      email: true,
      name: true,
      mobile: true,
      location: true,
      isVehicleAlloted:true,
      profileImage:true,
      rating:true,
      canDrive:true,
      licenceIds:true,
      governmentIds:true
    },
    where: {
      location: { near: loc,maxDistance: 50 ,unit: 'kilometers'},
      realm: "driver",
      canDrive:{inq:[vehicleType]},
      isVehicleAlloted:false,//{neq:true},
      adminVerifiedStatus:"approved"
    },
    include:{
      relation:"requestfordrivers",
      scope:{
        where:{
          ownerId:ownerId,
          vehicleId:vehicleId,
          status:"requested"
        }
      }
    },
    limit: limit,
    skip: skip
  }, function(err, nearbydrivers) {
    if (err) {
      cb(err, null);
    } else {
      let driverArr = [];
      nearbydrivers.forEach(function(driver){
        let d = driver;
        if(d.requestfordrivers.length){
          d.requestStatus = true;
        }else{
          d.requestStatus = false;
        }
        delete d.requestfordrivers;
        driverArr.push(d);
      })

      cb(null, { data: nearbydrivers, msg: msg.findingDrivers });
    }
  })
}

People.remoteMethod("findNearDriver", {
  description: 'route details',
  accepts: [
    { arg: 'req', type: 'object', http: {source:"req"} },
    { arg: 'position', type: 'object', required: true },
    { arg: 'vehicleType', type: 'string', required: true },
    { arg: 'vehicleId', type: 'string', required: true },
    { arg: 'skip', type: 'number' },
    { arg: 'limit', type: 'number' }
  ],
  returns: { arg: 'success', type: 'object' },
  http: { verb: 'get' },
});

People.getPeoplesByAdmin = function(realm,cb){
  console.log(realm)
  People.find({
    where:{
      realm:realm
    }
  },function(error,peoples){
    if(error) return cb(error,null);
    cb(null,{data:peoples,msg:msg.getPeoplesByAdmin});

  })
}

People.remoteMethod("getPeoplesByAdmin",{
  accepts : [
    {arg:"realm",type:"string",required:true}
  ],
  returns : {arg:"success",type:"object"},
  http:{verb:"get"}
})


/* People.driverRequests =function(req,filter,cb)
 {
   let peopleId = req.accessToken.userId;

   if (!filter)
     filter = {};

   if (!filter.where)
     filter.where = {};

   filter.where.peopleId = peopleId;

   filter.include = [{
     relation: "requestfordrivers",
     scope: {
       // fields: { fullName: true, userRating: true, totalUserRat: true }
       fields: { requestfordriverId: true }
     }
   }]

   People.find(filter, function(err, requests) {
     if (err) {
       cb(err, null)
     } else {
      // cb(null, { data: vehicles, msg: msg.vehiclesDetails });
      cb(null,requests)
     }
   })
 }

 People.remoteMethod('driverRequests', {
   description: 'details of all requests',
   accepts: [
     { arg: 'filter', type: 'object' },
     { arg: 'req', type: 'object', 'http': { source: 'req' } }
   ],
   returns: { arg: "success", type: "object" },
   http: { verb: 'get' }
 })*/


/*  People.afterRemote( "logout", function( ctx, modelInstance, next) {

    let accessToken = ctx.req.accessToken;
    if(accessToken.firebaseToken){
      People.updateAll(
        {id:accessToken.userId},
        { $pull: { firebaseTokens: {$in:[accessToken.firebaseToken]} } },
        {allowExtendedOperators:true},
      function(error,success){});
    }
    next();
  });
*/
};

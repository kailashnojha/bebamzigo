'use strict';

var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var msg = require("../messages/rating-msg.json");
module.exports = function(Rating) {
	disableAllMethods(Rating, ['find','deleteById']);
	Rating.validatesInclusionOf('userRating', {in: [1,2,3,4,5]});
	Rating.giveRating = function(req, jobId, userRating, content, cb){
		//console.log("userRating = ",userRating);
        let providerId = req.accessToken.userId;

		Rating.app.models.Job.findById(jobId,function(error,jobInst){
			if(error) return cb(error,null);
			if(!jobInst) return cb(new Error("No job found"),null);

			Rating.findOne({
				where:{
					jobId:jobId,
					providerId:providerId
				}
			},function(error,ratingInst){
				if(error) return cb(error,null);

				if(ratingInst) return cb(new Error("Rating already exist"),null);

				let peopleId = providerId.toString() == jobInst.creatorId.toString() ? jobInst.driverId : jobInst.creatorId;
				let rateBy = providerId.toString() == jobInst.creatorId.toString() ? "didCustGiveRate" : "didDriveGiveRate";
				Rating.app.models.People.findById(peopleId,function(error,peopleInst){
					if(error) return cb(error,null);

					Rating.create({
						jobId       : jobId,
						providerId  : providerId,
						peopleId    : peopleId,
						content     : content,
						userRating  : userRating
					},function(error,success){
						if(error) return cb(error,null);
						
						if(!peopleInst.rating){
							peopleInst.rating = {
								totalUsers  : 0,
								totalRating : 0,
								avgRating   : 0
							}
						}
						
						peopleInst.rating.totalUsers++;
						peopleInst.rating.totalRating += userRating;
						peopleInst.rating.avgRating = peopleInst.rating.totalRating/peopleInst.rating.totalUsers; 

						jobInst[rateBy] = true;
						jobInst.save(function(){});
						
						peopleInst.save(function(error,success){
							cb(null,{data:success,msg:msg.giveRatingSuccess});

						})
						 Rating.app.models.People.findById(providerId,function(err,proInst){
				        	if(err) return cb(err,null)
				        	if(proInst.jobId)
				        	  proInst.jobId = null;
				        	proInst.save(function(err,successP){})
				        })
						
					})	
				})
			})
		})
	}

	Rating.remoteMethod("giveRating",{
		accepts : [
			{arg:"req",type:"object",http:{source:"req"}},
			{arg:"jobId",type:"string", required:true, http:{source:"form"}},
			{arg:"userRating",type:"number", required:true, http:{source:"form"}},
			{arg:"content",type:"string", required:false, http:{source:"form"}}
		],
		returns : {arg: "success",type:"object"}
	})

	Rating.getRatings = function(peopleId, cb){
		Rating.find({
			where:{
				peopleId:peopleId
			},
			include:{
				relation:"provider",
				scope:{
					fields:{name:true,profileImage:true}
				}
			}
		},function(error,ratings){
			if(error) return cb(error,null);
			cb(null,{data:ratings, msg:msg.getRatings});
		})
	}

	Rating.remoteMethod("getRatings",{
		accepts : [
			{arg:"peopleId", type:"string", required:true}
		],
		returns : {arg:"success", type:"object"},
		http:{verb:"get"} 
	})
};

'use strict';

var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var msg = require("../messages/request-for-driver-msg.json");

module.exports = function(Requestfordriver) {

  disableAllMethods(Requestfordriver, ['find']);
  Requestfordriver.validatesInclusionOf('status', { in: ['requested', 'confirmed', 'rejected'] });

  Requestfordriver.makeRequest = function(req, driverId, vehicleId, cb) {
    let ownerId = req.accessToken.userId;
    let Vehicle = Requestfordriver.app.models.Vehicle;
    let People = Requestfordriver.app.models.People;
    Vehicle.findById(vehicleId,function(error,vehicleInst){
      if(error) return cb(error,null);
      if(!vehicleInst.assessmentEnd || (new Date(vehicleInst.assessmentEnd) < new Date()))
        return cb(new Error("Please do assessment first"),null);
      
      /*if(vehicleInst.approveStatus != "approved")
        return cb(new Error("Vehicle not approved"),null);*/

      if(!vehicleInst) return cb(new Error("No vehicle found"),null);
      if(vehicleInst.isDriverAlloted) return cb(new Error("Already assign to a driver"),null);

      People.findById(driverId,function(error,driverInst){
        if(error) return cb(new Error("No driver found"),null);
        if(driverInst.isVehicleAlloted) return cb(new Error("Driver already hired"),null);

        let data = {
          ownerId     : ownerId,
          driverId    : driverId,
          status      : "requested",
          vehicleId   : vehicleId,
          vehicleTypeId: vehicleInst.vehicleTypeId,
          createdAt   : new Date(),
          updatedAt   : new Date()
        }

        Requestfordriver.findOne({
          where: {
            ownerId    : ownerId,
            driverId   : driverId,
            vehicleId  : vehicleId
          }
        }, function(error, requestInst) {
          if (error) return cb(error, null);
          
          /*if (requestInst.status=="requested") {
            cb(new Error("You have already requested"), null);
          }*/
          else {
            Requestfordriver.create(data, function(err, success) {
              if (err) {
                cb(err, null);
              } else {
                cb(null, { data: success, msg: msg.makeRequest });
                Requestfordriver.app.models.Notification.makeRequestNoty(driverInst,vehicleInst,ownerId,function(){});
              }
            })
          }
        })

      })
    })
  }

  Requestfordriver.remoteMethod("makeRequest", {
    description: 'requesting for driver',
    accepts: [
      { arg: 'req', type: 'object', http: { source: 'req' } },
      { arg: 'driverId', type: 'string', required: true, http: { source: 'form' } },
      { arg: 'vehicleId', type: 'string', required: true, http: { source: 'form' } }
    ],
    returns: { arg: 'success', type: 'object' },
    http: { verb: 'post' },
  });

  Requestfordriver.getRequests = function(req, cb){
    let driverId = req.accessToken.userId;
    console.log("driverId::",driverId);
    let where = {
      driverId:driverId,
      status : "requested"
    }

    let aggregate = [
      {
         $lookup:{
           from: "Vehicle",
           localField: "vehicleId",
           foreignField: "_id",
           as: "vehicle"
         }
      },
      {
        $unwind:{
          path: "$vehicle",
          preserveNullAndEmptyArrays: false
        }
      },
      {
       
          $match:{
            $or: [ {"vehicle.driverId": null }, { "vehicle.driverId":{$exists: false}} ]
          }
          
      },
      {
         $lookup:{
           from: "People",
           localField: "ownerId",
           foreignField: "_id",
           as: "owner"
         }
      },
      {
        $unwind:{
          path: "$owner",
          preserveNullAndEmptyArrays: false
        }
      },
      {
         $lookup:{
           from: "VehicleType",
           localField: "vehicleTypeId",
           foreignField: "_id",
           as: "vehicleType"
         }
      },
      {
        $unwind:{
          path: "$vehicleType",
          preserveNullAndEmptyArrays: false
        }
      },
      {
        $project: {
          "_id": 1,
          "status": 1,
          "ownerId": 1,
          "vehicleId": 1,
          "driverId": 1,
          "createdAt": 1,
          "updatedAt": 1,
          "vehicle.id": "$vehicle._id",
          "vehicle.type": "$vehicle.type",
          "vehicle.vehicleNumber": "$vehicle.vehicleNumber",
          "vehicle.registrationNumber": "$vehicle.registrationNumber",
          "vehicle.ton": "$vehicle.ton",
          "vehicle.bodyType": "$vehicle.bodyType",
          "vehicle.logBook": "$vehicle.logBook",
          "vehicle.ntsaIns": "$vehicle.ntsaIns",
          "vehicle.insuranceCopy": "$vehicle.insuranceCopy",
          "vehicle.approveStatus": "$vehicle.approveStatus",
          "vehicle.isDriverAlloted": "$vehicle.isDriverAlloted",
          "vehicle.ownerId": "$vehicle.ownerId",
          "vehicle.driverId": "$vehicle.driverId",
          "vehicle.createdAt": "$vehicle.createdAt",
          "vehicle.assesmentStart": "$vehicle.assesmentStart",
          "vehicle.assessmentEnd": "$vehicle.assessmentEnd",
          "vehicle.adminId": "$vehicle.adminId",
          "owner.id": "$owner._id",
          "owner.realm": "$owner.realm",
          "owner.email": "$owner.email",
          "owner.companyName": "$owner.companyName",
          "owner.emailVerified": "$owner.emailVerified",
          "owner.profileImage": "$owner.profileImage",
          "owner.adminVerifiedStatus": "$owner.adminVerifiedStatus",
          "owner.rating": "$owner.rating",
          "owner.name": "$owner.name",
          "owner.mobile": "$owner.mobile",
          "owner.mobileVerified": "$owner.mobileVerified",
          "vehicleType.name"  : "$vehicleType.name",
          "vehicleType.type"  : "$vehicleType.type",
          "vehicleType.imgUrl": "$vehicleType.imgUrl"
        
      }
  
      }
         
    ]

    Requestfordriver.aggregate({where:where,aggregate:aggregate},{},function(error,data){
      if(error) return cb(error,null);
      cb(null,{data:data,msg:msg.getRequests});
    })



/*    Requestfordriver.find({
      where:{
        driverId:driverId
      },
      include:["owner", "vehicle"]
    },function(error,data){
      if(error) return cb(error,null);
      cb(null,{data:data,msg:msg.getRequests});
    })*/
  }

  Requestfordriver.remoteMethod("getRequests",{
    accepts : [
      {arg:"req", type:"object", http:{source: "req"}}
    ],
    returns : {arg:"success", type:"object"},
    http:{verb:"get"}
  })

  // Driver confirmation api

  Requestfordriver.confirmByDriver = function(req, requestId, cb) {

    let data = {
      driverId    : req.accessToken.userId,
      requestId   : requestId,
      status      : "confirmed",
      updatedAt   : new Date()
    }

    Requestfordriver.findById(requestId,{
      include:["vehicle","owner","driver"]
    },function(error,requestInst){
      if(error) return cb(error,null);
      if(!requestInst) return cb(new Error("No request found"),null);
      
      let driver = requestInst.driver();
      let owner = requestInst.owner();
      let vehicle = requestInst.vehicle();
      
      if(vehicle.isDriverAlloted) return cb(new Error("This vehicle already alloted to a driver"),null);
      if(driver.isVehicleAlloted) return cb(new Error("Already alloted vehicle to you"),null);


      driver.isVehicleAlloted = true;
      driver.vehicleId = vehicle.id;
      driver.ownerId = vehicle.ownerId;
      vehicle.isDriverAlloted = true; 
      vehicle.driverId = driver.id;
      requestInst.status = "confirmed";

      driver.save(function(errorD,driverInst){
        if(error) return cb(errorD,null);
        vehicle.save(function(errorV,vehicleInst){
          if(errorV) return cb(errorV,null);
          requestInst.save(function(errorR,requestInstance){
            if(errorR) return cb(errorR,null);
            cb(null,{data:requestInstance,msg:msg.confirmByDriver});
            Requestfordriver.app.models.Notification.confirmRequestNoty(owner,driver,vehicle,function(){});
          })
        })
      })

    })

  }

  Requestfordriver.remoteMethod("confirmByDriver", {
    description: 'confirming request',
    accepts: [
      { arg: 'req', type: 'object', http: { source: 'req' } },
      { arg: 'requestId', type: 'string', required: true, http: { source: 'form' } },
      // { arg: 'status', type: 'string', required: true, http: { source: 'form' } }
    ],
    returns: { arg: 'success', type: 'object' },
    http: { verb: 'post' },
  });

//**********************************reject request******************************************


Requestfordriver.rejectByDriver = function(req, requestId, cb) {

    Requestfordriver.destroyById(requestId,function(error,requestInst){
      if(error) return cb(error,null);
      cb(null,{data:requestInst,msg:msg.rejectByDriver});
            //Requestfordriver.app.models.Notification.confirmRequestNoty(owner,driver,vehicle,function(){});
    })
      

  }

  Requestfordriver.remoteMethod("rejectByDriver", {
    description: 'rejecting request',
    accepts: [
      { arg: 'req', type: 'object', http: { source: 'req' } },
      { arg: 'requestId', type: 'string', required: true, http: { source: 'form' } },
      
    ],
    returns: { arg: 'success', type: 'object' },
    http: { verb: 'post' }
  });


//******************************vehicle remove api***************************

  Requestfordriver.removeVehicle  = function(req,driverId,vehicleId,cb) {
    let peopleId = req.accessToken.userId;
    
    Requestfordriver.app.models.People.findById(peopleId,{
      include:{
        "relation": "jobOne",
          "include": {
              "relation": "driver"
              
            }
      }
    },function(err,peopleI){
      if(err) return cb(err,null)
        let  peopleInst = peopleI.toJSON();
      console.log("peopleInst:::::",peopleInst)
        if(peopleInst.realm=="driver"){
          if(peopleInst.jobId)
          	
            if(peopleInst.jobOne.status == "accepted" && peopleInst.jobOne.status=="on trip") return cb(new Error("Can not remove vehicle,Currently job is running."),null);
              if(peopleInst.isVehicleAlloted==false && peopleInst.vehicleId==null) return cb(new Error("Already removed vehicle"),null);
                 peopleI.isVehicleAlloted = false;
                 peopleI.vehicleId = null;
                 peopleI.ownerId  = null;
                 peopleI.save(function(err,successP){
                  if(err) return cb(err,null);
                    Requestfordriver.app.models.Vehicle.findById(vehicleId,function(err,vInst){
                    if (vInst)
                     vInst.isDriverAlloted = false;
                     vInst.driverId = null;
                     vInst.save(function(){});
                    //vInst.updateAttributes({isDriverAlloted:false,driverId:null},function(err,successV){
                      //if(err) return cb(err,successV)
                      //cb(null,{data:successV,msg:msg.removeVehicle});
                    //})
                    cb(null,{data:successP,msg:msg.removeVehicle});
                })
                //cb(null,{data:success,msg:msg.removeVehicle});
                
              })
        }else{
              Requestfordriver.app.models.People.findById(driverId,{
                  include: {
                    relation: 'jobOne'
                  }
                },function(err,driverInst){
                if(err) return cb(err,null)
                console.log("driverInst:::",driverInst);
                if(driverInst.jobId)
                  if(driverInst.jobOne.status=="accepted"&& driverInst.jobOne.status=="on trip") return cb(new Error("Can not remove vehicle,Currently job is running."),null); 
                  if(driverInst.isVehicleAlloted==false && driverInst.vehicleId==null) return cb(new Error("Already removed vehicle"),null); 
                  driverInst.isVehicleAlloted = false;
                  driverInst.vehicleId = null;
                  driverInst.ownerId = null; 
                  driverInst.save(function(err,success){
                  if(err) return cb(err,null);
                     Requestfordriver.app.models.Vehicle.findById(vehicleId,function(err,vInst){
                     if (vInst){
                        vInst.isDriverAlloted = false;
                        vInst.driverId = null;
                        vInst.save(function(){});
                     }
                     
                     //vInst.updateAttributes({isDriverAlloted:false,driverId:null},function(err,successV){
                     //if(err) return cb(err,successV)
                      //cb(null,{data:successV,msg:msg.removeVehicle});
                     //})
                     cb(null,{data:success,msg:msg.removeVehicle});


                  })
                  
               
                })
              })
        }
    })
  }

  Requestfordriver.remoteMethod("removeVehicle", {
    description: 'confirming request',
    accepts: [
      { arg: 'req', type: 'object', http: { source: 'req' } },
      { arg: 'driverId', type: 'string', http: { source: 'form' }},
      { arg: 'vehicleId', type: 'string', http: { source: 'form' }}
      // { arg: 'status', type: 'string', required: true, http: { source: 'form' } }
    ],
    returns: { arg: 'success', type: 'object' },
    http: { verb: 'post' },
  });

};

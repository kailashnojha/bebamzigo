'use strict';

var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var msg = require("../messages/route-msg.json");
module.exports = function(Route) {

  disableAllMethods(Route, ['find']);

  Route.updateTrip = function(req, jobId, location, cb) {
    
    Route.app.models.Job.findById(jobId,function(error,jobInst){
    	if(error)
    		cb(error,null);
    	else{
    		if(jobInst){
    			console.log(location)
	    		let data = {
			      jobId     : jobId,
			      location  : [location.lng,location.lat],
			      createdAt : new Date()
			    }
			    
			    Route.create(data, function(err, success) {
			      if (err) {
			        cb(err, null)
			      } else {
			        cb(null, {data:success,msg:msg.updateTripSuccess});
			      }
			    })    			
    		}else{
    			cb(new Error("No job found"),null);
    		}
    	}
    })

  }

  Route.remoteMethod("updateTrip", {
    description: 'route details',
    accepts: [
      { arg: 'req', type: 'object', http: { source: 'req' } },	
      { arg: 'jobId', type: 'string', required: true, http: { source: 'form' } },
      { arg: 'location', type: 'GeoPoint', required: true, http: { source: 'form' } }
    ],
    returns: { arg: 'success', type: 'object' },
    http: { verb: 'post' },
  });

  Route.getTrip = function(jobId, cb) {
    Route.find({
      where:{
      	jobId:jobId
      },
      order: 'createdAt DESC'
    }, function(err, success) {
      if (err) {
        cb(err, null);
      } else {
        cb(null, { data:success, msg:msg.getTripSuccess });
      }
    });
  }

  Route.remoteMethod('getTrip', {
  	accepts : [
  		{ arg: "jobId",type:"string",required:true}
  	],
    returns: { arg: "success", type: "object" },
    http: {verb:"get"}
  })

};

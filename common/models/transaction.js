
'use strict';
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var mpesa = require('mpesa-node-sdk');
var gf = require("../services/global-function");
var mpesaData = require("../../server/mpesa-data.json");
var path = require('path');
var crypto=require('crypto');
var constants = require("constants");
var fs = require("fs");
var msg = require("../messages/transaction-msg.json");
var config = require("../../server/config.json");


/*var path = require('path');
var privatekey = path.resolve(__dirname, '../../server.key');*/
// console.log(privatekey);

module.exports = function(Transaction) {
	
	disableAllMethods(Transaction, ['find','deleteById']);
	Transaction.validatesInclusionOf('type', { in: ['send','received','addMoney','fuelRequest','withdraw_money'] });

	Transaction.getTransaction = function(req,type,cb){
		if(type == "All"){
			type = undefined;
		}
		var peopleId = req.accessToken.userId;
		Transaction.find({
			fields:{
				id:true,
				method:true,
				peopleId:true,
				mobileNumber:true,
				amount:true,
				paymentStatus:true,
				reason:true,
				"type":true,
				"createdAt":true
			},
			where:{
				peopleId:peopleId,
				type:type

			},
			//{
				order : "createdAt DESC"
			//}
			
		},function(error,transactions){
			if(error) return cb(error,null);

			cb(null,{data:transactions,msg:msg.getTransaction});
		})
	}

	Transaction.remoteMethod("getTransaction",{
		accepts : [
			{arg:"req",type:"object",http:{source:"req"}},
			{arg:"type",type:"string"}
		],
		returns : {arg:"success",type:"object"},
		http:{verb:"get"}
	})



	Transaction.getSingleTransaction = function(transactionId,cb){
		
		//var peopleId = req.accessToken.userId;
		Transaction.findById(transactionId,function(error,success){
			if(error) return cb(error,null);
			if(!success)
				return cb(new Error("transaction not found!"),null);
			cb(null,{data:success,msg:msg.getTransaction});
		})
	}

	Transaction.remoteMethod("getSingleTransaction",{
		accepts : [
			//{arg:"req",type:"object",http:{source:"req"}},
			{arg:"transactionId",required: true,type:"string"}
		],
		returns : {arg:"success",type:"object"},
		http:{verb:"get"}
	})


	

	// **************** Business to customer api ******************************

	Transaction.b2c = function(amount,mobileNumber,transactionId,cb){
		//let peopleId = req.accessToken.userId;
		const request_options = {
		    "InitiatorName"        : mpesaData.initiatorName,
		    // "SecurityCredential"   : mpesaData.securityCredential,
		    "SecurityCredential"   : createSecCred(),
		    "CommandID"            : "PromotionPayment",
		    "Amount"               : amount,
		    "PartyA"               : mpesaData.shortCode1,
		    "PartyB"               : mobileNumber,
		    "Remarks"              : "Sending payment",
		    "QueueTimeOutURL"      : config.redirectUrl+"/api/Transactions/queueTimeOutUrl",
		    "ResultURL"            : config.redirectUrl+"/api/Transactions/GET/resultUrl",
		    "Occasion"             : "NA"
		};
		//console.log(request_options);
		let data = {
			      	//responseCode        :  ,//txn.ResponseCode,
			      	//userName            :  request_options.initiatorName,
			      	//amount              :  request_options.amount,
			      	conversationID      :  "",//txn.ConversationID,
			      	responseDescription :  "",//txn.ResponseDescription,
			      	paymentStatus       :  "",       
			      	updatedAt           : new Date()
		
		}	      	
		mpesa.b2c(request_options,function(error,txn){
	      if(error) {
				console.log("txn error::",error);
				return cb(error,null);
	      }
	      else{
	      		console.log("txn::",txn)
	      		//txn.ResponseCode == 0 ? data.paymentStatus = "completed" : data.paymentStatus="incomplete";
	      		data.paymentStatus ="pending"
	      		data.responseCode = txn.ResponseCode;
	      		data.conversationID = txn.ConversationID;
	      		data.responseDescription = txn.ResponseDescription;
				Transaction.findById(transactionId,function(err,tranInst){
			      	if(err) return cb(err,null)
			      	else{
			      		tranInst.updateAttributes(data,function(err,success){})
			      		console.log("txn::",txn);
	      				cb(null,{data:txn,msg:msg.btoc});
			      	}
			      		
			      		/*if(err) return cb(err,null)
			      		    console.log("saved part::: ",success);
			      		})*/
			      		//console.log("saved part::: ",success);
			      	
			      	//return cb(null,{data:success,msg:msg.btoc});
			    })
	      	
	      	

	      }
	      /*if(txn){
	      	console.log("transaction::::",txn);
			Transaction.create({
			      	txnStatus           :  data.ResponseCode,
			      	conversationID      :  data.ConversationID,
			      	responseDescription : data.ResponseDescription,
			      	createdAt           :  new Date()
		        },function(err,success){
		      	if(err) return cb(err,null)
		      	else{
		      		console.log("saved part::: ",success);
		      	}
		      	//return cb(null,{data:success,msg:msg.btoc});
		    })
	      }*/
	      
	      //cb(null,txn);
	  	})		  
	}

	Transaction.remoteMethod("b2c",{
		accepts : [
			{"arg":"amount",type:"number",http:{source:"form"}},
			{"arg":"mobileNumber",type:"string",http:{source:"form"}},
			{"arg":"transactionId",type:"string",http:{source:"form"}}
		],
		returns : {arg:"success",type:"object"}
	})


	Transaction.queueTimeOutUrl = function(req,res){
		console.log("come in queue time out")
		//console.log(JSON.stringify(req.body));
		//res.render('queue-timeout');
		//cb();
	}

	Transaction.remoteMethod("queueTimeOutUrl",{
		accepts:[
			{arg:"req",type:"object",http:{source:"req"}},
			{arg:"res",type:"object",http:{source:"res"}}
		],
		http :{verb:'get',path:"/queue-timeout"}
	})

	Transaction.resultUrl = function(req,res){
		console.log("come in result part")
		
		console.log(req.body.Result);
		//check transaction status and send notification, update wallet 
		 //res.render('result-url',{success:req.body.Result});
		 //Transaction.app.models.Notification.withdrawPaymentSuccess(peopleInst,transInst,function(error,success){});
		//cb();
	}

	Transaction.remoteMethod("resultUrl",{
		accepts:[
			{arg:"req",type:"object",http:{source:"req"}},
			{arg:"res",type:"object",http:{source:"res"}}
		],
		http :{verb:'get',path:"/result-url"}
	})

	// **************** Business to customer api ******************************


	//****************************generate auth accessToken***********************************


	/*Transaction.generateToken = function(cb){
		
		mpesa.generateMpesaToken(function(error,data){
		    if(error) return cb(error,null);
		    else{
		    	console.log("MPESAaccessToken::",data);
		        return cb(null,data);
		    }
		})
	}

	Transaction.remoteMethod("generateToken",{
		accepts : [
			// {"arg":"success",type:""}
		],
		returns : {arg:"success",type:"object"}
	})
*/

	// **************** Account Balance api ***********************************

	Transaction.accountBalance = function(cb){
		const request_options = {
	        "Initiator"           : "advocoso",
	        "SecurityCredential"  : createSecCred(),
	        "CommandID"           : "AccountBalance",
	        "PartyA"              : mpesaData.shortCode1,
	        "IdentifierType"      : "4",
	        "Remarks"             : "nothing",
	        "QueueTimeOutURL"     : config.redirectUrl+"/api/Transactions/balanceQTOutUrl",
	        "ResultURL"           : config.redirectUrl+"/api/Transactions/balanceResultUrl"
	    };
		mpesa.accountBalance(request_options,function(error,data){
		    if(error) return cb(error,null);
		    cb(null,data);
		})
	}

	Transaction.remoteMethod("accountBalance",{
		accepts : [
			// {"arg":"success",type:""}
		],
		returns : {arg:"success",type:"object"}
	})


	Transaction.balanceResultUrl = function(req,res,cb){
		console.log(req.body.Result);
		cb();
	}

	Transaction.remoteMethod("balanceResultUrl",{
		accepts : [
			{arg:"req",type:"object",http:{source:"req"}},
			{arg:"req",type:"object",http:{source:"req"}}
		],
		returns : {arg:"success",type:"object"}
	})

	Transaction.balanceQTOutUrl = function(req,res,cb){
		console.log(req.body.Result);
		cb();
	}

	Transaction.remoteMethod("balanceQTOutUrl",{
		accepts : [
			{arg:"req",type:"object",http:{source:"req"}},
			{arg:"req",type:"object",http:{source:"req"}}
		],
		returns : {arg:"success",type:"object"}
	})	


    // **************** Account Balance api ***********************************

    // **************** Transaction status api ***********************************

	Transaction.transactionStatus = function(cb){
		const request_options={
	       "Initiator"           : " ",
	       "SecurityCredential"  : " ",
	       "CommandID"           : "TransactionStatusQuery",
	       "TransactionID"       : " ",
	       "PartyA"              : " ",
	       "IdentifierType"      : "1",
	       "ResultURL"           : config.redirectUrl+"/api/Transactions/tStatusResultUrl",
	       "QueueTimeOutURL"     : config.redirectUrl+"/api/Transactions/tStatusTimeOutUrl",
	       "Remarks"             : " ",
	       "Occasion"            : " "
	    };
		mpesa.transactionStatus(request_options,function(error,data){
			if(error) return cb(error,null);
	       // console.log(data);
	       cb(null,data);
	    })
	}

	Transaction.remoteMethod("transactionStatus",{
		accepts : [
			// {"arg":"success",type:""}
		],
		returns : {arg:"success",type:"object"}
	})

	Transaction.tStatusResultUrl = function(req,res,cb){
		console.log(req.body.Result);
		cb();
	}

	Transaction.remoteMethod("tStatusResultUrl",{
		accepts : [
			{arg:"req",type:"object",http:{source:"req"}},
			{arg:"req",type:"object",http:{source:"req"}}
		],
		returns : {arg:"success",type:"object"}
	})

	Transaction.tStatusTimeOutUrl = function(req,res,cb){
		console.log(req.body.Result);
		cb();
	}

	Transaction.remoteMethod("tStatusTimeOutUrl",{
		accepts : [
			{arg:"req",type:"object",http:{source:"req"}},
			{arg:"req",type:"object",http:{source:"req"}}
		],
		returns : {arg:"success",type:"object"}
	})	


    // **************** Transaction status api ***********************************


    // **************** c2b status api ***********************************    

	Transaction.c2b = function(mobileNumber,amount,transactionId,cb){
		const request_options = {
	        "ShortCode"     : mpesaData.shortCode1,
	        "CommandID"     : "CustomerBuyGoodsOnline",
	        "Amount"        : amount,
	        "Msisdn"        : mobileNumber,
	        "BillRefNumber" : "account"
	    };
	 	
	    
	   
		let data = {
			      	
			      	conversationID      :  "",//txn.ConversationID,
			      	responseDescription :  "",//txn.ResponseDescription,
			      	paymentStatus       :  "",       
			      	updatedAt           : new Date()
		
		}		      	
		 mpesa.c2b(request_options,function(error,txn){
		    if(error) {
				console.log(error);
				return cb(error,null);
		    }
	      	else{
	      		console.log("c2b txn::",txn)
	      		console.log("txn::",txn)
	      		txn.ResponseCode == 0 ? data.paymentStatus = "completed" : data.paymentStatus="incomplete";
	      		data.responseCode = txn.ResponseCode;
	      		data.conversationID = txn.ConversationID;
	      		data.responseDescription = txn.ResponseDescription;

	      		Transaction.findById(transactionId,function(err,tranInst){
			      	if(err) return cb(err,null)
			      	else{
			      		tranInst.updateAttributes(data,function(err,success){})
			      		console.log("txn::",txn);
	      				cb(null,{data:txn,msg:msg.btoc});
			      	}
			      		
			      		
			    })
			}
	      
	  	})		  
	}

	Transaction.remoteMethod("c2b",{
		accepts : [
			{"arg":"mobileNumber",type:"string",http:{source:"form"}},
			{"arg":"amount",type:"number",http:{source:"form"}},
			{"arg":"transactionId",type:"string",http:{source:"form"}}
		],
		returns : {arg:"success",type:"object"}
	})

	Transaction.c2bValidateUrl = function(req,res,cb){
		console.log(req.body.Result);
		cb();
	}

	Transaction.remoteMethod("c2bValidateUrl",{
		accepts : [
			{arg:"req",type:"object",http:{source:"req"}},
			{arg:"req",type:"object",http:{source:"req"}}
		],
		returns : {arg:"success",type:"object"}
	})

	Transaction.c2bConfirmationUrl = function(req,res,cb){
		console.log(req.body.Result);
		cb();
	}

	Transaction.remoteMethod("c2bConfirmationUrl",{
		accepts : [
			{arg:"req",type:"object",http:{source:"req"}},
			{arg:"req",type:"object",http:{source:"req"}}
		],
		returns : {arg:"success",type:"object"}
	})		

	/*Transaction.c2b = function(mobileNumber,amount,cb){
		const request_options = {
	        "ShortCode"     : mpesaData.shortCode1,
	        "CommandID"     : "CustomerBuyGoodsOnline",
	        "Amount"        : amount,
	        "Msisdn"        : mobileNumber,
	        "BillRefNumber" : ""
	    };
	 	
	    // console.log("come in this function")
	    mpesa.c2b(request_options,function(error,data){
	    	if(error) return cb(error,null);
	    	console.log("*********************************")
	        console.log(data);
	        console.log("*********************************")
	        cb(null,data);

	    })
	}

	Transaction.remoteMethod("c2b",{
		accepts : [
			{"arg":"mobileNumber",type:"string",http:{source:"form"}},
			{"arg":"amount",type:"number",http:{source:"form"}}
		],
		returns : {arg:"success",type:"object"}
	})	*/


    // **************** c2b status api ***********************************    

    // **************** STKPushSimulation status api ***********************************        

	Transaction.STKPushSimulation = function(amount,mobileNumber,accountRef,transDesc,cb_url,cb){
		if(!cb_url)
			cb_url = "/api/Transactions/STKPushCallback";
		/*console.log("mobile Number");
		console.log(mobileNumber);*/
		let today   = new Date();
		var bodyString = {
	      "BusinessShortCode"  : mpesaData.shortCode,
	      "Password"           : getPassword(mpesaData.shortCode),
	      "Timestamp"          : gf.getTimeStamp(),
	      "TransactionType"    : "CustomerPayBillOnline",
	      "Amount"             : amount,
	      "PartyA"             : mobileNumber,
	      "PartyB"             : mpesaData.shortCode,
	      "PhoneNumber"        : mobileNumber,
	      "CallBackURL"        : config.redirectUrl+cb_url,
	      "AccountReference"   : accountRef,
	      "TransactionDesc"    : transDesc
	    }

	    mpesa.STKPushSimulation(bodyString,function(error,data){
	    	if(error) return cb(error,null);
	    	//console.log("STKPushSimulation result :::",data);
	    	cb(null,data)
	    })
	    
	}

	Transaction.remoteMethod("STKPushSimulation",{
		accepts : [
			{arg:"amount",type:"number",http:{source:"form"}},
			{arg:"mobileNumber",type:"string",http:{source:"form"}},
			{arg:"accountRef",type:"string",http:{source:"form"}},
			{arg:"transDesc",type:"string",http:{source:"form"}},
			{arg:"cb_url",type:"string",http:{source:"form"}}
		],
		returns : {arg:"success",type:"object"}
	})

	Transaction.STKPushCallback = function(req,res,cb){
		console.log("stk push callback ==> ",req.body.Body);
		Transaction.STKPushQuery(req.body.Body.stkCallback.CheckoutRequestID,function(error,success){

		})
		cb();
	}

	Transaction.remoteMethod("STKPushCallback",{
		accepts:[
			{arg:"req",type:"object",http:{source:"req"}},
			{arg:"res",type:"object",http:{source:"res"}}
		],
		returns : {arg:"success",type:"object"}
	})

    // **************** STKPushSimulation status api ***********************************        

    // **************** STKPushQuery status api ***********************************        	    

	Transaction.STKPushQuery = function(checkoutRequestID,cb){
		console.log("checkoutRequestID = ",checkoutRequestID);
		let today = new Date();
		
		var bodyString = {
	      "BusinessShortCode"  : mpesaData.shortCode3,
	      "Password"           : getPassword(mpesaData.shortCode3),
	      "Timestamp"          : gf.getTimeStamp(),
	      "CheckoutRequestID"  : checkoutRequestID
	    }

	    console.log("body string = ",bodyString);

	    mpesa.STKPushQuery(bodyString,function(error,data){
	    	if(error) return cb(error,null);
	    	console.log("data =======" ,data)
	    	cb(null,data);
	    })
	}

	Transaction.remoteMethod("STKPushQuery",{
		accepts : [
			{arg:"checkoutRequestID",type:"string",required:true,http:{source:"form"}}
		],
		returns : {arg:"success",type:"object"},
		http:{verb:"post"}
	})

    // **************** STKPushQuery status api ***********************************        	    

    // **************** B2B api ***********************************        	    

	Transaction.b2b = function(amount, accountRef, remark, cb){
		const request_options = {
	      "Initiator": mpesaData.initiatorName,
	      "SecurityCredential": mpesaData.securityCredential,
	      "CommandID": "BusinessBuyGoods",
	      "SenderIdentifierType": "4",
	      "RecieverIdentifierType": "4",
	      "Amount": amount,
	      "PartyA": mpesaData.shortCode1,
	      "PartyB": "123456",
	      "AccountReference": accountRef,
	      "Remarks": remark,
	      "QueueTimeOutURL": config.redirectUrl+"/api/Transactions/b2bQTOutUrl",
	      "ResultURL": config.redirectUrl+"/api/Transactions/b2bResultUrl"
	    }

		mpesa.b2b(request_options,function(error,data){
		  if(error) return cb(error,null);
	      cb(null,data);
	  	})		  
	}

	Transaction.remoteMethod("b2b",{
		accepts : [
			{"arg":"amount",type:"number",http:{source:"form"}},
			{"arg":"accountRef",type:"string",http:{source:"form"}},
			{"arg":"remark",type:"string",http:{source:"form"}}
		],
		returns : {arg:"success",type:"object"}
	})

	Transaction.b2bQTOutUrl = function(req,res,cb){
		console.log("b2b come in queue time out");
		console.log(req.body);
		cb();
	}

	Transaction.remoteMethod("b2bQTOutUrl",{
		accepts:[
			{arg:"req",type:"object",http:{source:"req"}},
			{arg:"res",type:"object",http:{source:"res"}}
		],
		returns: {arg:"success",type:"object"}
	})

	Transaction.b2bResultUrl = function(req,res,cb){
		console.log("b2b come in result part")
		console.log(req.body.Result);
		cb();
	}

	Transaction.remoteMethod("b2bResultUrl",{
		accepts:[
			{arg:"req",type:"object",http:{source:"req"}},
			{arg:"res",type:"object",http:{source:"res"}}
		],
		returns: {arg:"success",type:"object"}
	})



	Transaction.paymentSuccess =function(req,res){

		res.render('payment-success');
	}

	Transaction.remoteMethod('paymentSuccess',{
		accepts:[
	 {arg: 'req', type: 'object', http: { source: 'req' } },
	 {arg: 'res', type: 'object', http: { source: 'res' } }
	  ],
		//returns :{arg:"success",type :"object"},
		http :{verb:'get',path:"/payment-success"}

	})

    // **************** B2B api ***********************************        	    

	function getPassword(shortCode){
		return new Buffer(shortCode +mpesaData.passkey+gf.getTimeStamp()).toString("base64")
	}

	function createSecCred(){

		// return mpesaData.securityCredential;

       //sandbox value = Security Credential (Shortcode 1)
        //production value = api initiator password
        var bufferToEncrypt = new Buffer(mpesaData.secCred);
        //read the sandbox/production certificate data
        // PATH - e.g "./keys/sandbox-cert.cer"
        //var data = fs.readFileSync("./../../sandbox.cer");
        var data = fs.readFileSync(path.resolve(__dirname, './../../sandbox.cer'));
        //var data = fs.readFileSync(path.resolve(__dirname, './../../cert1.cer'));
        
        //convert data to string
        var privateKey = String(data);
        // console.log("my private key == >",privateKey);
        //encrypt the credential using the privatekey
        var encrypted = crypto.publicEncrypt({
            key: privateKey,
            padding: constants.RSA_PKCS1_PADDING
        }, bufferToEncrypt);
        //convert encrypted value to string and encode to base64
        var securityCredential = encrypted.toString("base64");
        //console.log("securityCredential = ",securityCredential);
        //return value to invoking method
        return securityCredential;
	}


	//*******************************************demo api*******************************************


	Transaction.firstApi =function(cb){
		let data = {
			secCredential : createSecCred(),
			passWord      : getPassword(mpesaData.shortCode),
			shortCode     : mpesaData.shortCode,
			ResultURL     : config.redirectUrl+"/api/Transactions/resultUrl",
			timeStamp     : gf.getTimeStamp()
		}
		return cb(null,{success:data})

	}

	Transaction.remoteMethod("firstApi",{
		accepts:[
			//{arg:"req",type:"object",http:{source:"req"}},
			//{arg:"res",type:"object",http:{source:"res"}}
		],
		returns: {arg:"success",type:"object"}
		
	})


	/*Transaction.paymentSuccess =function(req,res){

		res.render('payment-success');
	}

	Transaction.remoteMethod('paymentSuccess',{
		accepts:[
	 {arg: 'req', type: 'object', http: { source: 'req' } },
	 {arg: 'res', type: 'object', http: { source: 'res' } }
	  ],
		//returns :{arg:"success",type :"object"},
		http :{verb:'get',path:"/payment-success"}

	})



	Transaction.paymentFailure =function(req,res){

		res.render('payment-failure');
	}

	Transaction.remoteMethod('paymentFailure',{
		accepts:[
	 {arg: 'req', type: 'object', http: { source: 'req' } },
	 {arg: 'res', type: 'object', http: { source: 'res' } }
	  ],
		//returns :{arg:"success",type :"object"},
		http :{verb:'get',path:"/payment-failure"}

	})*/


};



'use strict';

var msg = require("../messages/vehicle-type-msg.json");
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
//var list  = require('../../node_modules/loopback-datasource-juggler/lib/list.js');
var bodyParser = require("body-parser");
module.exports = function(Vehicletype) {

  disableAllMethods(Vehicletype, ['find']);

	Vehicletype.addOrUpdate = function(req,res,cb){
		function uploadFile() {
	    Vehicletype.app.models.Container.imageUpload(req, res, { container: 'vehicles' }, function(err, success) {
        if (err)
          cb(err, null);
        else{
          if(success.obj){
            success.obj = JSON.parse(success.obj);
        
            createVehicle(success);  
          }else{
            cb(new Error("data not found"),null);
          }
        }
    	})
    }

    function createVehicle(passObj){
    	
    	let obj = {
        name        :     passObj.obj.name,
    		bodyType    :     passObj.obj.bodyType,
        ton         :     passObj.obj.ton,
        type        :     passObj.obj.type == 'vehicle' ? 'vehicle' : 'machinery'
    	}

       // console.log("array saved");
    	if (passObj.files) {
	    	if (passObj.files.image)
	        obj.image = passObj.files.image[0].url;
	    } 

	    Vehicletype.create(obj,function(err,success){
	    	if(err) return cb(err,null);

	    	cb(null,{obj: success, msg: msg.addOrUpdateSuccess})

	    })
    }

    uploadFile();
	}

	Vehicletype.remoteMethod("addOrUpdate",{
		accepts : [
			{arg:"req",type:"object",http:{source:"req"}},
			{arg:"res",type:"object",http:{source:"res"}}
		],
		returns : {arg:"success",type:"object"}
	})

  Vehicletype.getVehicleType = function(req, typeOfJob, cb){
     let filter = {}
     if(typeOfJob){
      filter.where = {jobType:{in:[typeOfJob]}};
     }
      // Vehicletype       

      /*let aggregate = [
      ]*/

      /*Vehicletype.aggregate({aggregate:aggregate},{},function(err,data){
        if(err) return cb(err,null);

        cb(null,{data:data,msg:msg.getVehicleTypeSuccess});
      }) */

      Vehicletype.find(filter,function(err,data){
        if(err) return cb(err,null);
        let obj = {
          vehicle : [],
          machinery : [] 
        }
        data.forEach((value)=>{
          if(value.type == 'Vehicle')
            obj.vehicle.push(value);
          else
            obj.machinery.push(value);
        })
        cb(null,{data:obj,msg:msg.getVehicleTypeSuccess});
      })
  }

  Vehicletype.remoteMethod("getVehicleType",{
    accepts : [
      {arg:"req",type:"object",http:{source:"req"}},
      {arg:"typeOfJob",type:"string"}
    ],
    returns : {arg:"success",type:"object"},
    http:{verb:"get"}
  })


};

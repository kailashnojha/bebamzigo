'use strict';
var gf = require("../services/global-function");
var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var msg = require("../messages/vehicle-msg.json");
var Ipay = require('ipayafrica');
/*var ipayValue = {
  vendor_id : "beba",
  hash_key  : "beba5684uyBHGT5846kgtd",
  live      : true
}*/
var ipayValue = {
  vendor_id : "demo",
  hash_key  : "demo",
  live      : false
}
var ipay = new Ipay.IpayAfrica(ipayValue);

var config = require("../../server/config.json");

module.exports = function(Vehicle) {

  Vehicle.validatesInclusionOf('approveStatus', { in: ['pending', 'reject', 'approved'] });

  disableAllMethods(Vehicle, ['find','deleteById']);

  Vehicle.addVehicle = function(req, res, cb) {
    function uploadFile() {
      Vehicle.app.models.Container.imageUpload(req, res, { container: 'docs' }, function(err, success) {
        if (err)
          cb(err, null);
        else {
          if (success.data) {
            success.data = JSON.parse(success.data);
            addTruck(success);
          } else {
            cb(new Error("data not found"), null);
          }
        }
      })
    }

    function addTruck(passObj) {
      let ownerId = req.accessToken.userId;
      var data = {
        approveStatus      : "pending",
        type               : passObj.data.type,
        vehicleNumber      : passObj.data.vehicleNumber,
        registrationNumber : passObj.data.registrationNumber,
        // vehicleMake        : new Date(passObj.data.vehicleMake),
        vehicleModel       : passObj.data.vehicleModel,
        ton                : passObj.data.ton,
        bodyType           : passObj.data.bodyType,
        ownerId            : ownerId,
        isDriverAlloted    : false
      }

      if(passObj.data.vehicleMake)
        data.vehicleMake = new Date(passObj.data.vehicleMake);

      if (passObj.files) {
        if (passObj.files.logBook)
          data.logBook = gf.getImageArray(passObj.files.logBook);

        /*if (passObj.files.insSticker)
          data.insSticker = gf.getImageArray(passObj.files.insSticker);*/

        if (passObj.files.ntsaIns)
          data.ntsaIns = gf.getImageArray(passObj.files.ntsaIns);

        if (passObj.files.insuranceCopy)
          data.insuranceCopy = gf.getImageArray(passObj.files.insuranceCopy);
      }

      data.createdAt = new Date();

      // this script only for add vehicle type id

      Vehicle.app.models.VehicleType.findOne({
        where:{
          name : data.type
        }
      },function(error,vehicleType){
        if(error) return cb(error,null);
        if(!vehicleType) return cb(new Error("No vehicle type found"),null);
        data.vehicleTypeId = vehicleType.id;

        createVehicle();
      })  

      // createVehicle();

      function createVehicle(){
        Vehicle.create(data, function(err, success) {
          if (err)
            cb(err, null);
          else
            cb(null, { data: success, msg: msg.addingVehicles });
        }) 
      }    

    }

    uploadFile();
  }

  Vehicle.remoteMethod('addVehicle', {
    description: 'registering vehicles',
    accepts: [
      { arg: 'req', type: 'object', http: { source: 'req' } },
      { arg: 'res', type: 'object', http: { source: 'res' } }
    ],
    returns: {
      arg: 'success',
      type: 'object'
    },
    http: { verb: 'post' },
  });

  Vehicle.uploadDocs = function(req, res, cb) {
    function uploadFile() {
      Vehicle.app.models.Container.imageUpload(req, res, { container: 'docs' }, function(err, success) {
        if (err)
          cb(err, null);
        else {
          if (success.data) {
            success.data = JSON.parse(success.data);
            addTruck(success);
          } else {
            cb(new Error("data not found"), null);
          }

        }
      })
    }

    function addTruck(passObj) {

      console.log("this is execute");
      console.log(req.accessToken.userId);
      var data = {
        type: passObj.data.type,
        peopleId: req.accessToken.userId
      }



      if (passObj.files) {
        if (passObj.files.logBook)
          data.logBook = gf.getImageArray(passObj.files.logBook);

        if (passObj.files.insSticker)
          data.insSticker = gf.getImageArray(passObj.files.insSticker);

        if (passObj.files.ntsaIns)
          data.ntsaIns = gf.getImageArray(passObj.files.ntsaIns);

        if (passObj.files.rc)
          data.rc = gf.getImageArray(passObj.files.rc);

      }


      // data.emailVerified = false;
      // data.createdAt = new Date();
      data.updatedAt = new Date();
      // data.mobileVerified = false;

      Vehicle.create(data, function(err, success) {
        if (err)
          cb(err, null);
        else
          // cb(null, success)
          cb(null, { data: success, msg: msg.uploadingDocs })
      })
    }

    uploadFile();
  }

  Vehicle.remoteMethod('uploadDocs', {
    description: 'uploading documents',
    accepts: [
      { arg: 'req', type: 'object', http: { source: 'req' } },
      { arg: 'res', type: 'object', http: { source: 'res' } }
    ],
    returns: {
      arg: 'success',
      type: 'object'
    },
    http: { verb: 'post' }
  });

  Vehicle.getMyVehicles = function(req, approveStatus, cb) {

    let ownerId = req.accessToken.userId;
    let filter = {};
    filter.where = {};


    filter.where.ownerId = ownerId;
    filter.where.approveStatus = approveStatus;

    filter.include = ["vehicleType","driver"];
   /* filter.include = [{
      relation: "owner",
      scope: {
        // fields: { fullName: true, userRating: true, totalUserRat: true }
        fields: { ownerId: true }
      }
    }]*/

    Vehicle.find(filter, function(err, vehicles) {
      if (err) {
        cb(err, null)
      } else {
        cb(null, { data: vehicles, msg: msg.vehiclesDetails });
      }
    })
  }

  Vehicle.remoteMethod('getMyVehicles', {
    description: 'details of all added vehicles',
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'approveStatus', type: 'string' },
    ],
    returns: { arg: "success", type: "object" },
    http: { verb: 'get' }
  })

  Vehicle.getOwnerVehicles = function(filter, ownerId, cb) {
 
    if (!filter)
      filter = {};

    if (!filter.where)
      filter.where = {
        approveStatus:"approved",
        assessmentEnd:{gte:new Date()}
      };

    filter.where.ownerId = ownerId;

    filter.include = ["vehicleType","driver"];

    /*filter.include = [{
      relation: "owner",
      scope: {
        // fields: { fullName: true, userRating: true, totalUserRat: true }
        fields: { ownerId: true }
      }
    }]*/

    Vehicle.find(filter, function(err, vehicles) {
      if (err) {
        cb(err, null)
      } else {
        cb(null, { data: vehicles, msg: msg.vehiclesDetails });
      }
    })
  }

  Vehicle.remoteMethod('getOwnerVehicles', {
    description: 'get all vehicles of a owner',
    accepts: [
      { arg: 'filter', type: 'object' },
      { arg: 'ownerId', type: 'string', required:true }
    ],
    returns: { arg: "success", type: "object" },
    http: { verb: 'get' }
  })


  Vehicle.getVehicleById = function(vehicleId, cb) {
    Vehicle.findById(vehicleId,{"include":["owner","vehicleType","driver"]}, function(err, success) {
      if (err) return cb(err, null);
      cb(null, { data: success, msg: msg.getSingleVehicleSuccess });
    })
  }

  Vehicle.remoteMethod("getVehicleById", {
    accepts: [
      { arg: "vehicleId", type: "string" }
    ],
    returns: { arg: "success", type: "object" },
    http: { verb: "get" }
  })

  Vehicle.getAllVehicles = function(approveStatus,cb) {
    let filter={
      where:{
        approveStatus : approveStatus
      }
    }

    filter.include = ["vehicleType","driver"];
    Vehicle.find(filter,function(err, success) {
      if (err) {
        cb(err, null)
      } else {
        // cb(null,success)
        cb(null, { data: success, msg: msg.getAllVehiclesSuccess });
      }
    })
  }

  Vehicle.remoteMethod("getAllVehicles", {
    accepts : [
      {arg:"approveStatus",type:"string"}
    ],
    returns: { arg: "success", type: "object" },
    http: { verb: "get" }
  })


  Vehicle.assessment = function(req,vehicleId,amount,paymentType,numberOfYear,paymentOptions,cb){

    if( [1,2,3,4,5].indexOf(numberOfYear) == -1 )
      return cb(new Error("Invalid year"),null);

    if( ["wallet","mpesa","card"].indexOf(paymentType) == -1 )
      return cb(new Error("Invalid payment type"),null);

    console.log("payment options = ",paymentOptions)
    if((paymentType == "card") && (!paymentOptions || !paymentOptions.cardNumber || !paymentOptions.cvv || !paymentOptions.month || !paymentOptions.year || !paymentOptions.fName || !paymentOptions.lName))  
      return cb(new Error("invalid card entry"),null);

    if(paymentType == "mpesa" && !paymentOptions.mobileNumber)  
      return cb(new Error("invalid mobile number"),null);

    var peopleId = req.accessToken.userId;
    var driverInst;
    Vehicle.app.models.People.findById(peopleId,function(errorP,peopleInst){
      if(errorP) return cb(errorP,null);

      Vehicle.findById(vehicleId,{
        include : {
          relation:"driver"
        }
      },function(errorV,vehicleInst){
        if(errorV) return cb(errorV,null);

        if(vehicleInst.driverId)
          driverInst = vehicleInst.driver();  
        
        Vehicle.app.models.Setting.findOne({
          where:{
            type:"AssessmentPrice"
          }
        },function(errorS,assesssmentInst){
          if(errorS) return cb(errorS,null);
          if(amount != assesssmentInst.price * numberOfYear) 
            return cb(new Error("Invalid amount"),null);
          Vehicle.app.models.Transaction.create({
            type          : "send",
            method        : paymentType,
            peopleId      : peopleId,
            amount        : amount,
            paymentStatus : "pending",
            reason        : "vehicle assessment",
            vehicleId     : vehicleId,
            createdAt     : new Date(),
            updatedAt     : new Date()  
          },function(errorT,transInst){
            if(errorT) return cb(errorT,null);
            
            if(paymentType == "wallet"){
              paymentByWallet(transInst,vehicleInst,peopleInst);
            }else{
              if(paymentType == "mpesa"){
                paymentByMpesa(transInst,vehicleInst,peopleInst);
              }else{
                paymentByCard(transInst,vehicleInst,peopleInst);
              }
            }
          })
        })
      })
    })

    function paymentByWallet(transInst,vehicleInst,peopleInst){
       Vehicle.app.models.Wallet.getWalletById(peopleId,function(error,walletInst){
          if(error) return cb(error,null);
          console.log(walletInst)
          walletInst = walletInst.data;

          if(walletInst.walletAmount >= amount){
            walletInst.walletAmount -= amount;
            walletInst.save(function(errorU,success){
              if(errorU) return cb(errorU,null);
              Vehicle.compVehicleAssess(numberOfYear,transInst,vehicleInst,peopleInst,cb);
            })
          }else{
            cb(new Error("unsufficient fund"),null);
            transInst.paymentStatus = "incomplete"
            transInst.save(function(){}); 
          }
       }) 
    }

    function paymentByMpesa(transInst,vehicleInst,peopleInst){

        Vehicle.app.models.Transaction.STKPushSimulation(amount,paymentOptions.mobileNumber,"nothing","for add payment","/api/Vehicles/mpesaCallback",function(error,success){
          if(error) return cb(error,null);
          transInst.updateAttributes({
            paymentStatus:"requested",
            responseData:success
          },function(error,updatedTrans){
            if(error) return cb(error,null);
            cb(null,{data:updatedTrans,msg:msg.mpesaAssessRequest});
          })
        })
    }

    function paymentByCard(transInst,vehicleInst,peopleInst){

      var request = { 
        order_id      : transInst.id.toString() ,
        invoice_id    : transInst.id.toString() ,
        amount        : amount ,
        telephone     : peopleInst.mobile ,
        email         : peopleInst.email ,
        currency      : 'KES' ,
        param1        : '' ,
        param2        : '' ,
        param3        : '' ,
        param4        : '' ,
        callback_url  : config.redirectUrl + "/api/Vehicles/cardCallback"
      }; 

      ipay.webRequest(request,function(err,response){
        if(err) return cb(err,null);
        console.log("response ==> ",response);
        cb(null,{data:{url:response},msg:msg.addCardMoney});  
      });
    }

    function updateInformation(transInst,vehicleInst,peopleInst,result){

      if(transInst.method == "card" && transInst.method == "wallet"){
        transInst.updateAttributes({
          paymentStatus:"completed",
          responseData:result
        },function(error,success){
          if(error) return cb(error,null);
          var startDate = new Date();
          var endDate = new Date();
          endDate.setFullYear(endDate.getFullYear()+numberOfYear);
          vehicleInst.updateAttributes({assesmentStart:startDate,assessmentEnd:endDate},function(errorV,vehicleNewInst){
            if(errorV) return cb(errorV,null);
            cb(null,{data:vehicleNewInst,msg:msg.assessment});
          })
        })
      }else{
        transInst.updateAttributes({
          paymentStatus:"requested",
          responseData:result
        },function(error,success){
          if(error) return cb(error,null);
          cb(null,{data:success,msg:msg.assessment});
          /*var startDate = new Date();
          var endDate = new Date();
          endDate.setFullYear(endDate.getFullYear()+numberOfYear);
          vehicleInst.updateAttributes({assesmentStart:startDate,assessmentEnd:endDate},function(errorV,vehicleNewInst){
            if(errorV) return cb(errorV,null);
            cb(null,{data:vehicleNewInst,msg:msg.assessment});
          })*/
        })
      }  
    }
  }

  Vehicle.remoteMethod("assessment",{
    accepts : [
      {arg:"req",type:"object",http:{source:"req"}},
      {arg:"vehicleId",type:"string",required:true,http:{source:"form"}},
      {arg:"amount",type:"number",required:true,http:{source:"form"}},
      {arg:"paymentType",type:"string",required:true,http:{source:"form"}},
      {arg:"numberOfYear",type:"number",required:true,http:{source:"form"}},
      {arg:"paymentOptions",type:"object",http:{source:"form"}}
    ],
    returns :{arg:"success",type:"object"}
  })

  Vehicle.mpesaCallback  = function(req,res,cb){
    let result = req.body.Body.stkCallback;
    Vehicle.app.models.Transaction.findOne({
      where:{
        "responseData.MerchantRequestID":result.MerchantRequestID,
        "responseData.CheckoutRequestID":result.CheckoutRequestID 
      },
      include:["people","vehicle"]
    },function(error,transInst){
      if(error) return cb(error,null);
      if(!transInst) return cb(new Error("No transaction found"),null);

      if(result.ResultCode == 0){
        // transInst,vehicleInst,peopleInst,result
        let peopleInst = transInst.people();
        let vehicleInst = transInst.vehicle();
        Vehicle.compVehicleAssess(numberOfYear,transInst,vehicleInst,peopleInst,result,cb);
      }else{
        transInst.updateAttributes({
          paymentStatus:"incomplete",
          responseData:result
        },function(error,success){
          if(error) return cb(error,null);
          // Wallet.app.models.Notification.addPaymentFail(peopleInst,transInst,function(error,success){});
          cb(null,success);
        })
      }
    })    
  }

  Vehicle.remoteMethod("mpesaCallback",{
    accepts : [
      {arg:"req",type:"object",http:{source:"req"}},
      {arg:"res",type:"object",http:{source:"res"}}
    ],
    returns : {arg:"success",type:"object"}
  })

  Vehicle.cardCallback  = function(req,res,cb){

    let str = req.query;
    let confData = {
      vendor  : ipay.vendor_id,
      id      : str["id"],
      ivm     : str["ivm"],
      qwh     : str["qwh"],
      afd     : str["afd"],
      poi     : str["poi"],
      uyt     : str["uyt"],
      ifd     : str["ifd"]
    }

    Wallet.app.models.Transaction.findById(confData.id,{
      include:["people","vehicle"]
    },function(error,transInst){
      if(error) return cb(error,null);
      if(!transInst) return cb(new Error("No transaction found"),null);

      let peopleInst = transInst.people();
      let vehicleInst = transInst.vehicle();
      let verfyUrl = "https://www.ipayafrica.com/ipn/?"+querystring.stringify(confData);
      request(verfyUrl, function (error, response, body) {
        if (!error && response.statusCode == 200) {
          if(body == "aei7p7yrx4ae34"){
            Vehicle.compVehicleAssess(numberOfYear,transInst,vehicleInst,peopleInst,cb);
          }else{
            transInst.updateAttributes({
            paymentStatus:"incomplete",
            responseData:confData
          },function(error,success){
            if(error) return cb(error,null);
            // Wallet.app.models.Notification.addPaymentFail(peopleInst,transInst,function(error,success){});
            cb(null,success);
          })
          }
        }else{
          cb(error,null);
        }
      });
    

    })
  }

  Vehicle.compVehicleAssess = function(numberOfYear,transInst,vehicleInst,peopleInst,result,cb){
    if(typeof result == "function"){
      cb = result;
      result = undefined;
    }
    
    transInst.updateAttributes({
      paymentStatus:"completed",
      responseData:result
    },function(error,success){
      if(error) return cb(error,null);
      var startDate = new Date();
      var endDate = new Date();
      endDate.setFullYear(endDate.getFullYear()+numberOfYear);
      vehicleInst.updateAttributes({assesmentStart:startDate,assessmentEnd:endDate},function(errorV,vehicleNewInst){
        if(errorV) return cb(errorV,null);
        cb(null,{data:vehicleNewInst,msg:msg.assessment});
      })
    })
  }

  Vehicle.remoteMethod("cardCallback",{
    accepts : [
      {arg:"req",type:"object",http:{source:"req"}},
      {arg:"res",type:"object",http:{source:"res"}}
    ],
    returns : {arg:"success",type:"object"}
  })

  Vehicle.getAssessPrice = function(cb){
    Vehicle.app.models.Setting.findOne({
      where:{
        type:"AssessmentPrice"
      }
    },function(error,priceInst){
      if(error) return cb(error,null);
      cb(null,{data:priceInst,msg:msg.getAssessPrice});
    })
  }

  Vehicle.remoteMethod("getAssessPrice",{
    returns : {arg:"success",type:"object"},
    http:{verb:"get"}
  })


  // Vehicle.validatesInclusionOf('status', { in: ['approved', 'discard'] });

  Vehicle.approveAdmin = function(req, vehicleId, approveStatus,reason, cb) {
    let adminId = req.accessToken.userId;
    /*let data = {
      adminId: req.accessToken.userId,
      vehicleId: vehicleId,
      approveStatus: approveStatus,
      approvedAt: new Date()
    }*/

    Vehicle.findById(vehicleId,function(error,vehicleInst){
      if(error) return cb(error,null);
      if(!vehicleInst) return cb(new Error("No vehicle found"),null);

      vehicleInst.approveStatus = approveStatus;
      vehicleInst.adminId = adminId;
      vehicleInst.reason = reason;
      vehicleInst.save(function(error,success){
        if(error) return cb(error, null);
        cb(null, { data: success, msg: msg.approvedVehicleByAdmin }); 
      })

    })


    /*Vehicle.create(data, function(err, success) {
      if (err) {
        cb(err, null)
      } else {
        
        cb(null, { data: success, msg: msg.approvedVehicleByAdmin });

      }
    })*/
  }

  Vehicle.remoteMethod("approveAdmin", {
    accepts: [
      { arg: 'req', type: 'object', required: true, 'http': { source: 'req' } },
      { arg: 'vehicleId', type: 'string', required: true, 'http': { source: 'form' } },
      { arg: 'approveStatus', type: 'string', required: true, http: { source: 'form' } },
      { arg: 'reason', type: 'string', http: { source: 'form' } }
    ],
    returns: { arg: "success", type: "object" },
    http: { verb: "post" }
  })


  Vehicle.getVehicleByAdmin = function(status,cb){
    Vehicle.find({
      where:{
        approveStatus:status
      },
      include:["vehicleType","driver","owner"]
    },function(error,success){
      if(error) return cb(error,null);
      cb(null,{data:success,msg:msg.getVehicleByAdmin})
    })
  }

  Vehicle.remoteMethod("getVehicleByAdmin",{
    accepts : [
      {arg:"status",type:"string",required:true}
    ],
    returns:{arg:"success",type:"object"},
    http:{verb:"get"}
  })


};

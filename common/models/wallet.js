'use strict';

var disableAllMethods = require('../../server/disableMethods.js').disableAllMethods;
var msg = require("../messages/wallet-msg.json");
var Ipay = require('ipayafrica');
var config = require("../../server/config.json");
const querystring = require('querystring'); 
var request = require("request");
var ipayValue = {
	vendor_id : "beba",
	hash_key  : "beba5684uyBHGT5846kgtd",
	live      : true
}

/*var ipayValue = {
	vendor_id : "demo",
	hash_key  : "demo",
	live      : false
}*/
var ipay = new Ipay.IpayAfrica(ipayValue);



module.exports = function(Wallet) {

    disableAllMethods(Wallet, ['find']);

	Wallet.getWallet = function(req,cb){
		let peopleId = req.accessToken.userId;
		Wallet.getWalletValue(peopleId, function(error,wallet){
			if(error) return cb(error,null);
			cb(null,{data:wallet, msg:msg.getAmount});
		})
	}

	Wallet.remoteMethod("getWallet",{
		accepts : [
			{arg:"req", type:"object", http:{source:"req"}}
		],
		returns : {arg:"success", type:"object"},
		http:{verb:"get"}
	})

	Wallet.getWalletValue = function(peopleId,cb){
		Wallet.findOne({
			where:{
				peopleId:peopleId
			}
		},function(error,wallet){
			if(error) return cb(error,null);
			if(wallet){
				cb(null,wallet);
			}else{
				Wallet.createWallet(peopleId,function(error,success){
					if(error) return cb(error,null);
					cb(null,success);	
				})
			}
		})
	}

	Wallet.getWalletById = function(peopleId,cb){
		Wallet.getWalletValue(peopleId,function(error,wallet){
			if(error) return cb(error,null);
			cb(null,{data:wallet, msg:msg.getAmount});
		})
	}

	Wallet.remoteMethod("getWalletById",{
		accepts : [
			{arg:"peopleId",type:"string",required:true}
		],
		returns : {arg:"success",type:"object"},
		http:{verb:"get"}
	})

	Wallet.createWallet = function(peopleId,cb){
		// let peopleId = req.accessToken.userId;

		Wallet.findOne({
			where:{
				peopleId:peopleId
			}
		},function(err,success){
			if(err) return cb(err,null);
			if(success) return cb(null,success);

			Wallet.create({
				peopleId:peopleId,
				walletAmount : 0,
				createdAt:new Date(),
				updatedAt : new Date()
			},function(error,walletInst){
				if(error) return cb(error,null);

				cb(null,walletInst);
			})
		})
	}

	/*Wallet.addMPesaMoney = function(req,mobileNumber,amount,cb){
		let peopleId = req.accessToken.userId;
		Wallet.app.models.People.findById(peopleId,function(error,peopleInst){
			if(error) return cb(error,null);

		    Wallet.getWallet(req, function(error,success){
				if(error) return cb(error,null);

				let walletInst = success.data;
				Wallet.app.models.Transaction.create({
					method        : "mpesa",
					type          : "received",
					peopleId      : peopleId,
					mobileNumber  : mobileNumber,
					amount        : amount,
					paymentStatus : "pending",
					reason        : "add payment in wallet",
					createdAt     : new Date(),
					updatedAt     : new Date()
				},function(error,transInst){
					if(error) return cb(error,null);
					
					Wallet.app.models.Transaction.STKPushSimulation(amount,mobileNumber,"nothing","for add payment","/api/Wallets/mpesaCallback",function(error,success){
						if(error) return cb(error,null);
						transInst.updateAttributes({
			             	paymentStatus:"requested",
			            	responseData:success
			            },function(error,updatedTrans){
			            	if(error) return cb(error,null);
				          	cb(null,{data:updatedTrans,msg:msg.addMPesaMoney});
			            })
					})
				})
			})	
		})
	}

	Wallet.remoteMethod("addMPesaMoney",{
		accepts : [
			{arg:"req",type:"object", http:{source:"req"}},
			{arg:"mobileNumber",type:"string", required:true, http:{source:"form"}},
			{arg:"amount",type:"number", required:true, http:{source:"form"}}
		],
		returns : {arg:"success",type:"object"}
	})
*/
//*************************************************new code *********************************************************
	Wallet.addMPesaMoney = function(req,res,mobileNumber,amount,cb){
		let peopleId = req.accessToken.userId;
		Wallet.app.models.People.findById(peopleId,function(error,peopleInst){
			if(error) return cb(error,null);

		    Wallet.getWallet(req, function(error,success){
				if(error) return cb(error,null);

				let walletInst = success.data;
				let wAmount   =  walletInst.walletAmount+amount; 
				Wallet.app.models.Transaction.create({
					method        : "mpesa",
					type          : "received",
					peopleId      : peopleId,
					mobileNumber  : mobileNumber,
					amount        : amount,
					paymentStatus : "pending",
					reason        : "add money to wallet",
					createdAt     : new Date(),
					updatedAt     : new Date()
				},function(error,transInst){
					if(error) return cb(error,null);
					cb(null,{data:transInst,msg:msg.addMPesaMoney});
					
					//res.render('mpesa-payment.ejs',{result:});
					console.log("moving forward");
					Wallet.app.models.Transaction.STKPushSimulation(amount,mobileNumber,"add wallet money","for add payment","/api/Wallets/mpesaCallback",function(error,success){
					//Wallet.app.models.Transaction.c2b(mobileNumber,amount,transInst.id,function(error,success){
						if(error) return cb(error,null);
						transInst.updateAttributes({
			             	paymentStatus:"requested",
			            	responseData:success
			            },function(error,updatedTrans){
			            	//if(error) return cb(error,null);
			            	//else{
			            		
			            		//cb(null,{data:updatedTrans,msg:msg.addMPesaMoney});
			            		console.log("payment TXN :>>",success);
			            		//res.render('mpesa-payment', { restdata: success});
			            	//}
				          	
			            })
					})
				})
			})	
		})
	}

	Wallet.remoteMethod("addMPesaMoney",{
		accepts : [
			{arg:"req",type:"object", http:{source:"req"}},
			{arg: 'res', type: 'object', http: { source: 'res'}},
			{arg:"mobileNumber",type:"string", required:true, http:{source:"form"}},
			{arg:"amount",type:"number", required:true, http:{source:"form"}}

		],
		returns : {arg:"success",type:"object"}
	})



	/*Wallet.mpesaPayment =function(req,res){

		res.render('mpesa-payment.ejs',{ result: {}});
	}

	Wallet.remoteMethod('mpesaPayment',{
		accepts:[
	 {arg: 'req', type: 'object', http: { source: 'req' } },
	 {arg: 'res', type: 'object', http: { source: 'res' } }
	  ],
		//returns :{arg:"success",type :"object"},
		http :{verb:'get',path:"/mpesa-payment"}

	})*/


//*************************************************new code *********************************************************



	Wallet.mpesaCallback = function(req,res,cb){
		let result = req.body.Body.stkCallback;
		console.log("result:>>",result);
		Wallet.app.models.Transaction.findOne({
			where:{
				"responseData.MerchantRequestID":result.MerchantRequestID,
				"responseData.CheckoutRequestID":result.CheckoutRequestID	
			},
			include:"people"
		},function(error,transInst){
			if(error) return cb(error,null);
			if(!transInst) return cb(new Error("No transaction found"),null);
			let peopleInst = transInst.people();

			if(result.ResultCode == 0){
				Wallet.getWalletById(peopleInst.id,function(error,walletInst){
					if(error) return cb(error,null);
					transInst.updateAttributes({
						paymentStatus:"completed",
						responseData:result
					},function(error,success){
						if(error) return cb(error,null);
						wamount = walletInst.walletAmount + transInst.amount;
						walletInst.updateAttributes({walletAmount:wamount},function(error,success){
							if(error) return cb(error,null);
							// send notification on succes payment add
							Wallet.app.models.Notification.addPaymentSuccess(peopleInst,transInst,function(error,success){});
							//cb(null,success)
							//return res.redirect('/api/Wallets/payment-success');
							//res.render('mpesa-payment.ejs', { result: result});
						})
					})
				})	
			}else{
				transInst.updateAttributes({
					paymentStatus:"incomplete",
					responseData:result
				},function(error,success){
					if(error) return cb(error,null);

					Wallet.app.models.Notification.addPaymentFail(peopleInst,transInst,function(error,success){});
					//return res.redirect('/api/Wallets/payment-failure');
					//res.render('mpesa-payment.ejs', { result: result})
					//cb(null,success);
				})
			}
		})
	}

	Wallet.remoteMethod("mpesaCallback",{
		accepts : [
			{arg:"req",type:"object",http:{source:"req"}},
			{arg:"res",type:"object",http:{source:"res"}}
		],
		returns : {arg:"success",type:"object"}
	})

	Wallet.paymentSuccess =function(req,res){

		res.render('payment-success');
	}

	Wallet.remoteMethod('paymentSuccess',{
		accepts:[
	 {arg: 'req', type: 'object', http: { source: 'req' } },
	 {arg: 'res', type: 'object', http: { source: 'res' } }
	  ],
		//returns :{arg:"success",type :"object"},
		http :{verb:'get',path:"/payment-success"}

	})

	Wallet.paymentFailure =function(req,res){

		res.render('payment-failure');
	}

	Wallet.remoteMethod('paymentFailure',{
		accepts:[
	 {arg: 'req', type: 'object', http: { source: 'req' } },
	 {arg: 'res', type: 'object', http: { source: 'res' } }
	  ],
		//returns :{arg:"success",type :"object"},
		http :{verb:'get',path:"/payment-failure"}

	})



	

	Wallet.addCardMoney = function(req,amount,cb){	
		let peopleId = req.accessToken.userId;
		Wallet.app.models.People.findById(peopleId,function(error,peopleInst){
			if(error) return cb(error,null);

				Wallet.app.models.Transaction.create({
					method        : "card",
					type          : "received",
					peopleId      : peopleId,
					amount        : amount,
					paymentStatus : "pending",
					reason        : "add payment in wallet",
					createdAt     : new Date(),
					updatedAt     : new Date() 
				},function(error,transInst){
					if(error) return cb(error,null);

					var request = { 
				      order_id      : transInst.id.toString() ,
				      invoice_id    : transInst.id.toString() ,
				      amount        : amount ,
				      telephone     : peopleInst.mobile ,
				      email         : peopleInst.email ,
				      currency      : 'KES' ,
				      param1        : '' ,
				      param2        : '' ,
				      param3        : '' ,
				      param4        : '' ,
				      callback_url  : config.redirectUrl + "/api/Wallets/cardCallback"
				    }; 

					ipay.webRequest(request,function(err,response){
				        if(err) {
				        	console.log("eorr part ********************",err);
				        	return cb(err,null);

				        }

				      	console.log("response ==> ",response);
				    	cb(null,{data:{url:response},msg:msg.addCardMoney});	
				    });
					

				})
		})
	}

	Wallet.remoteMethod("addCardMoney",{
		accepts : [
			{arg:"req",type:"object", http:{source:"req"}},
			/*{arg:"fName",type:"string", required:true, http:{source:"form"}},
			{arg:"lName",type:"string", required:true, http:{source:"form"}},*/
			{arg:"amount",type:"number", required:true, http:{source:"form"}}
			/*{arg:"cardNumber",type:"number", required:true, http:{source:"form"}},
			{arg:"month",type:"number", required:true, http:{source:"form"}},
			{arg:"year",type:"number", required:true, http:{source:"form"}},
			{arg:"cvv",type:"number", required:true, http:{source:"form"}}*/
		],
		returns : {arg:"success", type:"object"}
	})

	Wallet.cardCallback = function(req,res,cb){
		console.log("fdhbfgjkhmfh");
		let str = req.query;
		let confData = {
			vendor  : ipay.vendor_id,
			id      : str["id"],
			ivm     : str["ivm"],
			qwh     : str["qwh"],
			afd     : str["afd"],
			poi     : str["poi"],
			uyt     : str["uyt"],
			ifd     : str["ifd"]
		}


		Wallet.app.models.Transaction.findById(confData.id,{
			include:"people"
		},function(error,transInst){
			if(error) return cb(error,null);
			if(!transInst) return cb(new Error("No transaction found"),null);

			let peopleInst = transInst.people();

			Wallet.getWalletById(peopleInst.id,function(error,walletInst){
				if(error) return cb(error,null);


				let verfyUrl = "https://www.ipayafrica.com/ipn/?"+querystring.stringify(confData);
				request(verfyUrl, function (error, response, body) {
					console.log("error part************",error);
				  if (!error && response.statusCode == 200) {
				  	console.log("success part **************");
				     if(body == "aei7p7yrx4ae34"){
				    //if(body == "fe2707etr5s4wq"){	
				    	Wallet.getWalletById(peopleInst.id,function(error,walletInst){
							if(error) return cb(error,null);
							
							walletInst = walletInst.data;
							transInst.updateAttributes({
								paymentStatus:"completed",
								responseData:confData
							},function(error,success){
								if(error) return cb(error,null);
								walletInst.walletAmount += transInst.amount;
								walletInst.save(function(error,success){
									if(error) return cb(error,null);
									// send notification on succes payment add


									Wallet.app.models.Notification.addPaymentSuccess(peopleInst,transInst,function(error,success){});
									// cb(null,success);
									
									//res.redirect("/payment/wallet/success?txnId="+transInst.id.toString());
								})
							})
						})	
				    }else{
				    	transInst.updateAttributes({
							paymentStatus:"incomplete",
							responseData:confData
						},function(error,success){
							if(error) return cb(error,null);
							Wallet.app.models.Notification.addPaymentFail(peopleInst,transInst,function(error,success){});
							// cb(null,success);

							//res.redirect("/payment/wallet/fail?txnId="+transInst.id.toString());
						})
				    }
				  }else{
				  	cb(error,null);
				  }
				});
			})

		})
	}

	Wallet.remoteMethod("cardCallback",{
		accepts : [
			{arg:"req",type:"object",http:{source:"req"}},
			{arg:"res",type:"object",http:{source:"res"}}
		],
		returns : {arg:"success",type:"object"},
		http:{verb:"get"}
	})


	Wallet.getWebView = function(cb){
		var request = { 
	      order_id      : "48fdsdfsdfsdfsdfdffds1234456" ,
	      invoice_id    : "48fdsdfsdfsdfsdfdffds1234456" ,
	      amount        : 20 ,
	      telephone     : "9649165819" ,
	      email         : "kailash.advocosoft@gmail.com" ,
	      currency      : 'KES' ,
	      param1        : '' ,
	      param2        : '' ,
	      param3        : '' ,
	      param4        : '' ,
	      callback_url  : config.redirectUrl+"/api/Wallets/walletAddCardMoneyGet"
	    }; 

		ipay.webRequest(request,function(err,response){
	        if(err) return cb(err,null);
	      	console.log("response ==> ",response);
	    	cb(null,response)	
	    });
	}

	Wallet.remoteMethod("getWebView",{
		accepts : [],
		returns : {arg:"success",type:"object"}
	})

	Wallet.addMoneyDirect = function(peopleId,amount,cb){
		Wallet.getWalletById(peopleId,function(error,success){
			if(error) return cb(error,null);
			let walletInst = success.data;
			walletInst.walletAmount += amount;
			walletInst.save(function(error,data){
				if(error) return cb(error,null);
				cb(null,data);
			})
		})
	}

	Wallet.remoteMethod("addMoneyDirect",{
		accepts : [
			{arg:"peopleId",type:"string",http:{source : "form"}},
			{arg:"amount",type:"number",http:{source : "form"}}
		],
		returns : {arg:"success",type:"object"}
	})


	//*********************************************** withdrawel from wallet api**********************************************


	Wallet.withdrawMoney = function(req,mobileNumber,amount,cb){
		let peopleId = req.accessToken.userId;
		Wallet.app.models.People.findById(peopleId,function(error,peopleInst){
			if(error) return cb(error,null);

		    Wallet.getWallet(req, function(error,successW){
				if(error) return cb(error,null);
				console.log("wallet detail::",successW);
				let walletInst = successW.data;
				//let wAmount   =  walletInst.walletAmount+amount;
				Wallet.app.models.Transaction.create({
					method        : "mpesa",
					type          : "withdraw_money",
					peopleId      : peopleId,
					mobileNumber  : mobileNumber,
					amount        : amount,
					paymentStatus : "pending",
					reason        : "withdraw money from wallet",
					createdAt     : new Date(),
					updatedAt     : new Date()
				},function(error,transInst){
					if(error) return cb(error,null);
					Wallet.app.models.Transaction.b2c(amount,transInst.mobileNumber,transInst.id,function(error,txn){
						if(error) return cb(error,null);
						console.log("withdrwa transaction success ::",txn);
						transInst.mpesaResponse = txn.data;
						
							if(txn.data.ResponseCode=='0'){
									//let newWAmt = successW.walletAmount-amount;
									//successW.updateAttributes({walletAmount:newWAmt,updatedAt : new Date()}, function(err,updatedW){
										//if(err) return cb(err,null)
										//Wallet.app.models.Notification.withdrawPaymentSuccess(peopleInst,transInst,function(error,success){});
										console.log("success part")
										

										cb(null,{data:transInst,msg:msg.waithdrawMoneySuccess});
									//})
							}else{	
									//Wallet.app.models.Notification.withdrawPaymentFail(peopleInst,transInst,function(error,success){});
									console.log("Error part")
									cb(null,{data:transInst,msg:msg.waithdrawMoneyFail});
							}
					})
				})
			})	
		})
	}

	Wallet.remoteMethod("withdrawMoney",{
		accepts : [
			{arg:"req",type:"object", http:{source:"req"}},
			{arg:"mobileNumber",type:"string", required:true, http:{source:"form"}},
			{arg:"amount",type:"number", required:true, http:{source:"form"}}
		],
		returns : {arg:"success",type:"object"}
	})



};

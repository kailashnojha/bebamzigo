'use strict'
module.exports = {
	getImageArray : function(arr){
		let newArr = [];
		arr.forEach(function(value){
			newArr.push(value.url);
		})
		return newArr;
	},
	getTimeStamp(){
		let today = new Date();
		let timezone = today.getFullYear()+""+this.getTwoDigitNumber(today.getMonth())+this.getTwoDigitNumber(today.getDay());
		timezone += this.getTwoDigitNumber(today.getHours())+""+this.getTwoDigitNumber(today.getMinutes())+""+this.getTwoDigitNumber(today.getSeconds());
		return timezone;
	},
	getTwoDigitNumber(number){
		if(number<10)
			return "0"+number;
		return number;
	},
	getOTP : function(){
	    //var val = Math.floor(1000 + Math.random() * 9000);
	    var val = 1234;
	    return val;
	},
	getLocationObj: function(obj){
		obj = obj || {};
		if(obj.lat && obj.lng){
			return {
				lng : obj.lng,
				lat : obj.lat
			}	
		}else{
			return {
				address   : obj.address,
				location  : {
					lng : obj.location.lng,
					lat : obj.location.lat
				}
			}	
		}
		
	}
}
'use strict'
module.exports = {
	addJobNoty : function(job,driver,ln){
		ln = ln || "en";
		return {
          title    : "New Job",
          body     : "Hi new job posted around you. Be a first person and apply it.",
          "Date"   : new Date().toISOString(),
          "type"   : "new_job",
          "jobId"  : job.id.toString(),
          "badge"  : (driver.newNotification || 0).toString(),
          "sound"  : "default"
        }
	},
	applyJobNoty : function(job,owner,ln){
		ln = ln || "en";
		return {
          title    : "Apply Job",
          body     : "Someone applied on your job",
         "Date"   : new Date().toISOString(),
         "type"   : "apply_job",
          "jobId"  : job.id.toString(),
          "badge"  : (owner.newNotification || 0).toString(),
          "sound"  : "default"
        }
	},
  makeRequestNoty : function(driver,vehicleInst,ownerId,ln){
    
    ln = ln || "en";
    return {
          title         : "Make Request",
          body          : "Hi you have a request from vehicle owner!!!",
          "Date"   : new Date().toISOString(),
          "type"        : "owner_request",
          "driverId"    : driver.id.toString(),
          "ownerId"     : ownerId.toString(),
          "vehicleId"   : vehicleInst.id.toString(),
          "badge"       : (driver.newNotification || 0).toString(),
          "sound"       : "default"
        }
  },

  confirmByDriver : function(owner,ln){
    ln = ln || "en";
    return {
          title    : "Confirm Request",
          body     : "Someone accepted your request!!!",
          "Date"   : new Date().toISOString(),
          "type"   : "driver_confirmation",
          "ownerId"  : owner.id.toString(),
          "badge"  : (owner.newNotification || 0).toString(),
          "sound"  : "default"
        }
  },
  startTripNoty : function(customerInst,driverInst,jobInst,ln){
    ln = ln || "en";
    return {
          title    : "Start Trip",
          body     : "Your trip has been started!!!",
          "Date"   : new Date().toISOString(),
          "type"   : "start_trip",
          "ownerId"  : customerInst.id.toString(),
          "badge"  : (customerInst.newNotification || 0).toString(),
          "sound"  : "default"
        }
  },      
  endTripNoty : function(customerInst,driverInst,jobInst,ln){
    ln = ln || "en";
    return {
          title    : "End Trip",
          body     : "Your trip has been ended!!!",
          "Date"   : new Date().toISOString(),
          "type"   : "end_trip",
          "ownerId"  : customerInst.id.toString(),
          "badge"  : (customerInst.newNotification || 0).toString(),
          "sound"  : "default"
        }
  },
/*  endTripNoty : function(customerInst,driverInst,jobInst,ln){
    ln = ln || "en";
    return {
          title    : "Amount Received",
          body     : "You have received amount",
          "type"   : "trip_complete_trans",
          "ownerId": driverInst.id.toString(),
          "sound"  : "default"
        }
  },*/
  jobCompTransNoty : function(customerInst,driverInst,jobInst,transInst,ln){
    ln = ln || "en";
    driverInst.ownerId = driverInst.ownerId|| "";
    return {
          title    : "Amount Received",
          body     : "You have received amount",
          "Date"   : new Date().toISOString(),
          "type"   : "trip_complete_trans",
          "ownerId": driverInst.ownerId.toString(),
          "badge"  : (driverInst.newNotification || 0).toString(),
          "sound"  : "default"
        }
  },
  addPaymentSuccess: function(peopleInst,transInst,ln){
    ln = ln || "en";
    return {
          title      : "Add payment successfully",
          body       : "Successfully add money in wallet",
          "Date"   : new Date().toISOString(),
          "type"     : "add_payment_success",
          "peopleId" : peopleInst.id.toString(),
          "badge"    : (peopleInst.newNotification || 0).toString(),
          "sound"    : "default"
        }
  },
  addPaymentFail: function(peopleInst,transInst,ln){
    ln = ln || "en";
    peopleInst.id = peopleInst.id|| "";
    return {
          title      : "Add payment failed",
          body       : "Unable to add money in wallet",
          "Date"   : new Date().toISOString(),
          "type"     : "add_payment_fail",
          "peopleId" : peopleInst.id.toString(),
          "badge"    : (peopleInst.newNotification || 0).toString(),
          "sound"    : "default"
        }
  },
  withdrawPaymentSuccess: function(peopleInst,transInst,ln){
    ln = ln || "en";
    return {
          title      : "withdrawel done successfully",
          body       : "Successfully withdrawel the money",
          "Date"   : new Date().toISOString(),
          "type"     : "withdrawel_success",
          "peopleId" : peopleInst.id.toString(),
          "badge"    : (peopleInst.newNotification || 0).toString(),
          "sound"    : "default"
        }
  },
  withdrawPaymentFail: function(peopleInst,transInst,ln){
    ln = ln || "en";
    peopleInst.id = peopleInst.id|| "";
    return {
          title      : "withdrawel failed",
          body       : "Failed to withdraw money",
          "Date"   : new Date().toISOString(),
          "type"     : "withdrawel_fail",
          "peopleId" : peopleInst.id.toString(),
          "badge"    : (peopleInst.newNotification || 0).toString(),
          "sound"    : "default"
        }
  },
  userApprovalNoty: function(peopleInst,adminInst,notyInst,ln){
    ln = ln || "en";
    return {
          title      : notyInst.type == "user_approved" ? "Profile Approved":"Profile Rejected",
          body       : notyInst.type == "user_approved" ? "Your profile has been approved":"Your profile has been rejected",
          "Date"   : new Date().toISOString(),
          "type"     : notyInst.type,
          "peopleId" : peopleInst.id.toString(),
          "adminId"  : adminInst.id.toString(),
          "badge"    : (peopleInst.newNotification || 0).toString(),
          "sound"    : "default"
        }
  },
  jobApprovalNoty: function(jobInst,peopleInst,adminInst,notyInst,ln){
    ln = ln || "en";
    return {
          title      : notyInst.type == "job_approved" ? "Job Approved":"Job Rejected",
          body       : notyInst.type == "job_approved" ? "Your job has been approved":"Your job has been rejected",
           "Date"   : new Date().toISOString(),
          "type"     : notyInst.type,
          "peopleId" : peopleInst.id.toString(),
          "jobId"    : jobInst.id.toString(),
          "adminId"  : adminInst.id.toString(),
          "badge"    : (peopleInst.newNotification || 0).toString(),
          "sound"    : "default"
        }
  } ,
  lessAmountFuel : function(job,owner,ln){
    ln = ln || "en";
    return {
          title    : "Insufficient balance",
          body     : "Fuel request declined due to insufficient balance in wallet.",
          "Date"   : new Date().toISOString(),
          "type"   : "fuel_request_low_balance",
          "jobId"  : job.id.toString(),
          "badge"  : (owner.newNotification || 0).toString(),
          "sound"  : "default"
        }
  },                                 
}
'use strict';

module.exports = function(server) {
  // Install a `/` route that returns server status
  var People = server.models.People;
  var Transaction = server.models.Transaction;

  var router = server.loopback.Router();
  router.get('/', server.loopback.status());

  router.get("/payment/wallet/success",function(req,res){
    var txnId = req.query.txnId;
    Transaction.findById(txnId,function(error,success){
    	if(error) return cb(error,null);
    	res.render("transaction-success.ejs",{
    		data:success
    	});
    })
  })

  router.get("/payment/wallet/fail",function(req,res){
    var txnId = req.query.txnId;
    Transaction.findById(txnId,function(error,success){
    	if(error) return cb(error,null);
    	res.render("transaction-success.ejs",{
    		data:success
    	});
    })
  })

  router.get("/payment/assessment/success",function(req,res){
    console.log("assessment success");
    console.log("pass data ==> ",req.body);
    console.log(req.query);



  })

  router.get("/payment/assessment/fail",function(req,res){
    console.log("assessment fail");
    console.log("pass data ==> ",req.body);
    console.log(req.query);
  })  

  server.use(router);
};

module.exports = function(app) {
 /* app.remotes().phases
    .addBefore('invoke', 'options-from-request')
    .use(function(ctx, next) {
      console.log("**********",ctx.args.options)
      if (!ctx.args.options.accessToken) return next();
      const User = app.models.User;
      app.models.User.findById(ctx.args.options.accessToken.userId, function(err, user) {
        if (err) return next(err);
        ctx.args.options.currentUser = user;
        next();
      });
    });*/
  var People = app.models.People;
  var Role = app.models.Role;
  var RoleMapping = app.models.RoleMapping;
  
  RoleMapping.belongsTo(People);
  People.hasMany(RoleMapping, {foreignKey: 'principalId'});
  Role.hasMany(People, {through: RoleMapping, foreignKey: 'roleId'});

function createRoles(){

  Role.count(function(error,count){
     if(error) throw error;
     if(count == 0){
        Role.create([
          {name:'super_admin'},
          {name:'admin'},
          {name:'customer'},
          {name:'driver'},
          {name:'owner'}
        ],function(err,success){
          if(err) throw err;
          createSuperAdmin();
        })
     }else{
      createSuperAdmin();
     }

  })

}

createRoles();

function createSuperAdmin(){
  People.count({realm:"super_admin"},function(error,count){
    if(error) throw error;
    if(count == 0){
      People.create({
        realm:"super_admin",
        email: 'kailash.advocosoft@gmail.com', 
        mobile:'+919649165819', 
        password: '123456',
        isProfileComplete:true,
        emailVerified:true,
        mobileVerified:true,
        adminVerifiedStatus: "approved"
      }, function(err, user) {
        if (err) throw err;
        Role.findOne({
          name: 'super_admin'
        }, function(err, role) {
          if (err) throw err;
          role.principals.create({
            principalType: RoleMapping.USER,
            principalId: user.id
          }, function(err, principal) {
            if (err) throw err;
          });
        });
      });
    }
  })
}


/*  People.findOne({where:{email:"kailash.advocosft@gmail.com"}},function(err,user){
    if(err) throw err;

    Role.findOne({
      where:{
        name: 'super_admin'
      }
    }, function(err, role) {
      if (err) throw err;

      console.log('Created role:', role);
      role.principals.create({
        principalType: RoleMapping.USER,
        principalId: user.id
      }, function(err, principal) {
        if (err) throw err;

        console.log('Created principal:', principal);
      });
    });
  })*/



   
};
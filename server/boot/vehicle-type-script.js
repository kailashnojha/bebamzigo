module.exports = function(app) {
  var VehicleType = app.models.VehicleType;
  var Setting = app.models.Setting;
  var Vehicle = app.models.Vehicle;
  var Job = app.models.Job;
  let arr = [
    {
      name     : "Van",
      type     : "Vehicle",
      ton      : [],
      bodyType : [],
      make     : false,
      model    : false,
      jobType  : ["on demand","standard rate"],
      imgUrl   : "/api/Containers/vehicles/download/van.png"
    },  
    {
      name     : "Lorry",
      type     : "Vehicle",
      ton      : [
                   {
                      value:3,
                      unit :"tonnes"
                   },
                   {
                      value:7,
                      unit :"tonnes"
                   },
                   {
                      value:10,
                      unit :"tonnes"
                   },
                   {
                      value:14,
                      unit :"tonnes"
                   }
                 ],
      bodyType : [],           
      make     : false,
      model    : false,
      jobType  : ["on demand","standard rate"],
      imgUrl   : "/api/Containers/vehicles/download/lorry.png"
    },
    {
      name     : "Pick Up truck",
      type     : "Vehicle",
      ton      : [],
      bodyType : [],
      make     : false,
      model    : false,
      jobType  : ["on demand","standard rate"],
      imgUrl   : "/api/Containers/vehicles/download/pick-up-truck.png"
    },
    {
      name     : "Trailer",
      type     : "Vehicle",
      ton      : [
                    {
                      value:28,
                      unit :"tonnes"
                    }
                 ],
      bodyType : [
        {
          name : "Flatbed"
        },
        {
          name : "Enclosed"
        },
        {
          name : "Open body"
        },
        {
          name : "Tipper"
        }
      ],
      make     : false,
      model    : false,
      jobType  : ["on demand","standard rate"],
      imgUrl   : "/api/Containers/vehicles/download/trailer.png"

    },
    {
      name     : "Excavator",
      type     : "Machinery",
      ton      : [],
      bodyType : [],
      make     : true,
      model    : true,
      jobType  : ["standard rate"],
      imgUrl   : "/api/Containers/vehicles/download/excavator.png"
    },
    {
      name     : "Roller",
      type     : "Machinery",
      ton      : [],
      bodyType : [],
      make     : true,
      model    : true,
      jobType  : ["standard rate"],
      imgUrl   : "/api/Containers/vehicles/download/roller.png"
    },
    {
      name     : "Grader",
      type     : "Machinery",
      ton      : [],
      bodyType : [],
      make     : true,
      model    : true,
      jobType  : ["standard rate"],
      imgUrl   : "/api/Containers/vehicles/download/grader.png"
    },
    {
      name     : "Bulldozer",
      type     : "Machinery",
      ton      : [],
      bodyType : [],
      make     : true,
      model    : true,
      jobType  : ["standard rate"],
      imgUrl   : "/api/Containers/vehicles/download/bulldozer.png"
    },
    {
      name     : "Crane",
      type     : "Machinery",
      ton      : [
                   {
                      value:3,
                      unit :"tonnes"
                   },
                   {
                      value:4,
                      unit :"tonnes"
                   },
                   {
                      value:8,
                      unit :"tonnes"
                   },
                   {
                      value:10,
                      unit :"tonnes"
                   },
                   {
                      value:20,
                      unit :"tonnes"
                   },
                   {
                      value:50,
                      unit :"tonnes"
                   },
                   {
                      value:70,
                      unit :"tonnes"
                   },
                   {
                      value:100,
                      unit :"tonnes"
                   }
                 ],
      bodyType : [],
      make     : true,
      model    : true,
      jobType  : ["standard rate"],
      imgUrl   : "/api/Containers/vehicles/download/crane.png"
    },
    {
      name     : "Tractor",
      type     : "Machinery",
      ton      : [],
      bodyType : [],
      make     : true,
      model    : true,
      jobType  : ["standard rate"],
      imgUrl   : "/api/Containers/vehicles/download/tractor.png"
    },
    {
      name     : "Backhoe",
      type     : "Machinery",
      ton      : [],
      bodyType : [],
      make     : true,
      model    : true,
      jobType  : ["standard rate"],
      imgUrl   : "/api/Containers/vehicles/download/backhoe.png"
    },
    {
      name     : "Wheel loader",
      type     : "Machinery",
      ton      : [],
      bodyType : [],
      make     : true,
      model    : true,
      jobType  : ["standard rate"],
      imgUrl   : "/api/Containers/vehicles/download/backhoe.png"
    }                                                        
  ]

  VehicleType.count(function(err,count){
    if(count==0){
       VehicleType.create(arr,function(err,success){
          console.log(err);
          console.log(success);
       })        
    }
  })

  let pricingArr = [
    {
      name              : "28 T",
      standardCharge    : 15120,
      baseMileage       : {
                            value:30,
                            unit:"KM"
                          },
      extraCharge       : {
                            value:182,
                            unit:"KM"
                          },
      waitingTimeCharge : {
                            charge        : 100,
                            waitingPeriod : 2,
                            unit          : "Hrs"
                          },
      bebaCommission    : {
                            value : 10,
                            type  : "%"
                          }                                      
    },
    {
      name              : "14 T",
      standardCharge    : 10500,
      baseMileage       : {
                            value:25,
                            unit:"KM"
                          },
      extraCharge       : {
                            value:70,
                            unit:"KM"
                          },
      waitingTimeCharge : {
                            charge        : 100,
                            waitingPeriod : 2,
                            unit          : "Hrs"
                          },
      bebaCommission    : {
                            value : 10,
                            type  : "%"
                          }                                      
    },
    {
      name              : "10 T",
      standardCharge    : 6250,
      baseMileage       : {
                            value:25,
                            unit:"KM"
                          },
      extraCharge       : {
                            value:65,
                            unit:"KM"
                          },
      waitingTimeCharge : {
                            charge        : 150,
                            waitingPeriod : 1,
                            unit          : "Hrs"
                          },
      bebaCommission    : {
                            value : 10,
                            type  : "%"
                          }                                      
    },    
    {
      name              : "7 T",
      standardCharge    : 5960,
      baseMileage       : {
                            value:25,
                            unit:"KM"
                          },
      extraCharge       : {
                            value:56,
                            unit:"KM"
                          },
      waitingTimeCharge : {
                            charge        : 150,
                            waitingPeriod : 1,
                            unit          : "Hrs"
                          },
      bebaCommission    : {
                            value : 10,
                            type  : "%"
                          }                                      
    },        
    {
      name              : "3 T",
      standardCharge    : 4300,
      baseMileage       : {
                            value:20,
                            unit:"KM"
                          },
      extraCharge       : {
                            value:47,
                            unit:"KM"
                          },
      waitingTimeCharge : {
                            charge        : 100,
                            waitingPeriod : 30,
                            unit          : "Mins"
                          },
      bebaCommission    : {
                            value : 10,
                            type  : "%"
                          }                                      
    },            
    {
      name              : "Pick Up truck",
      standardCharge    : 2300,
      baseMileage       : {
                            value:10,
                            unit:"KM"
                          },
      extraCharge       : {
                            value:45,
                            unit:"KM"
                          },
      waitingTimeCharge : {
                            charge        : 100,
                            waitingPeriod : 30,
                            unit          : "Mins"
                          },
      bebaCommission    : {
                            value : 10,
                            type  : "%"
                          }                                      
    },
    {
      name              : "Van",
      standardCharge    : 2300,
      baseMileage       : {
                            value:10,
                            unit:"KM"
                          },
      extraCharge       : {
                            value:45,
                            unit:"KM"
                          },
      waitingTimeCharge : {
                            charge        : 100,
                            waitingPeriod : 30,
                            unit          : "Mins"
                          },
      bebaCommission    : {
                            value : 10,
                            type  : "%"
                          }                                      
    },                    
  ]

  let pricingSetting = {
    pricing : pricingArr,
    type    : "Pricing",
  }

  Setting.count({type:"Pricing"},function(err,count){
    if(count==0){
      Setting.create(pricingSetting,function(err,success){});
    }
  })
  // Setting.create()

  let assessmentSetting = {
    type   : "AssessmentPrice",
    price  : 3000,
    unit   : "year",
    value  : 1
  }

  Setting.count({type:"AssessmentPrice"},function(err,count){
    if(count == 0){
      Setting.create(assessmentSetting,function(err,success){});
    }
  })


  // update vehicleTypeId in vehicles

  /*Vehicle.find(function(error,success){
    if(success){
      
      success.forEach(function(vehicleInst){
        VehicleType.findOne({where:{name:vehicleInst.type}},function(error,vehicleTypeInst){
          if(vehicleTypeInst){
            vehicleInst.vehicleTypeId = vehicleTypeInst.id;
            vehicleInst.save(function(){});
          }
        })
        
      })
    }
  })*/


  // update vehicleTypeId in job

/*  Job.find(function(error,success){
    if(success){
      
      success.forEach(function(jobInst){
        VehicleType.findOne({where:{name:jobInst.typeOfVehicle}},function(error,vehicleTypeInst){
          if(vehicleTypeInst){
            jobInst.vehicleTypeId = vehicleTypeInst.id;
            jobInst.save(function(){});
          }
        })
        
      })
    }
  })*/

};

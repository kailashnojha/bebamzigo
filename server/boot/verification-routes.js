module.exports = function(app) {
  var router = app.loopback.Router();

  router.get('/user-verify/verified', function(req, res) {
    res.render('verified.ejs',{
      msg:"You have verified successfully"
    });
  });

  app.use(router);
}
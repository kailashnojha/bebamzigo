function process_mpesa() {
    document.getElementById("mpesa_response").innerHTML = "<div id='alert'> Processing ... </div>";
    document.getElementById("alert_info_kali").innerHTML = "Checking payment .... ";
    mpesa_txn = document.getElementById("mpesa_txn").value;
    if (mpesa_txn.length < 5) {
        document.getElementById("mpesa_response").innerHTML = " <div id='alert' style='color:red'> Invalid Mpesa Code </div>";
        reload_payments();
    } else {
        $.ajax({
                type: "POST",
                url: "https://app.sendyit.com/biz/coporate/process_mpesa",
                data: {
                    txn: mpesa_txn
                }
            })
            .done(function(msg) {
                msg = msg.replace(/(\r\n|\n|\r)/gm, "");
                if (msg == "okay") {
                    document.getElementById("mpesa_response").innerHTML = " <div id='alert' style='color:green'> Payment Sucessfull.   </div>";
                } else {
                    document.getElementById("mpesa_response").innerHTML = " <div id='alert'> Payment failed. Call 0728561783 for help</div>";
                }
                reload_payments();
            });
    }
}

function get_running_balance() {
    $.ajax({
            type: "POST",
            url: "https://app.sendyit.com/biz/coporate/running_balance",
            data: {
                cop_id: 0,
                user_phone: '9039625995',
            }
        })
        .done(function(msg) {
            return msg;
        });
}

function reload_payments() {
    document.getElementById("alert_info_kali").innerHTML = "Checking running balance ... ";
    $.ajax({
            type: "POST",
            url: "https://app.sendyit.com/biz/coporate/running_balance",
            data: {
                cop_id: 0,
                user_phone: "9039625995",
            }
        })
        .done(function(msg) {
            var running_balance = parseInt(msg);
            document.getElementById("rb").value = running_balance;
            amount_to_pay = parseInt(document.getElementById("current_cost_show").innerHTML);
            if (running_balance >= amount_to_pay) {
                document.getElementById("alert_info_kali").innerHTML = " Completing your order and redirecting you to the tracking page ";
                process_first();
            } else {
                run_dupper(running_balance);
            }
        });
}

function start_process() {
    $('#contain_supper_btns').addClass('hidden');
    $('#supper_button_alter2').removeClass('hidden');
    document.getElementById('cost_box').className = 'cost_box hidden';
    document.getElementById('rerequest_box').className = 'rerequest_box';
    document.getElementById("supper_button_alter2").innerHTML = "COMPLETE REQUEST";
}

function end_process() {
    document.getElementById('cost_box').className = 'cost_box';
    document.getElementById('rerequest_box').className = 'rerequest_box hidden';
    document.getElementById("supper_button_alter2").innerHTML = "PROCESSING ... ";
    $('#contain_supper_btns').removeClass('hidden');
    $('#supper_button_alter2').addClass('hidden');
}


function get_order_no() {

    return document.getElementById("order_no").value;

}

function initiate_mpesa() {
    start_mpesa_process();
    var mpesa_phone = document.getElementById('mpesa_phone').value;
    var mpesa_amount = (document.getElementById('mpesa_amount').value);
    console.log('mpesa_phone ' + mpesa_phone.length);
    console.log('mpesa_amount ' + mpesa_amount);
    if ((mpesa_phone.length < 3) || (mpesa_amount.length < 1)) {
        console.log('failed')
        document.getElementById("alert_info_kali").innerHTML = " <div id='' style='color:orange'> Put in the Phone Number or Mpesa Ref No.  </div>";
        end_mpesa_process();
    } else {
        document.getElementById("alert_info_kali").innerHTML = " <div id='' style=''> Initiating ...  </div>";
        $.ajax({
                type: "POST",
                url: "https://app.sendyit.com/biz/sendy/initite_payment",
                dataType: 'json',
                data: {
                    mpesa_phone: mpesa_phone,
                    mpesa_amount: mpesa_amount,
                    mpesa_account: get_order_no(),

                }
            })
            .done(function(msg) {
                console.log(msg);
                if (msg.status === true) {
                   if (msg.type === 'new') {
                        document.getElementById("alert_info_kali").innerHTML = " <div id='' style=''> A confirmation has been sent to phone no. " + mpesa_phone + ". Authorise to proceed.  </div>";
                        wait_for_payment(msg.traceNumber, mpesa_phone, mpesa_amount);
                    } else {
                        document.getElementById("mx_paymethod").value = 'M-Pesa';
                        document.getElementById("alert_info_kali").innerHTML = " <div id='' style=''> Payment Successfull. Your account has been credited.  </div>";
                        end_mpesa_process();
                        window.setTimeout(reload_payments, 5000); // 5 seconds
                    }
                } else {
                    if (msg.type === 'new') {
                        document.getElementById("alert_info_kali").innerHTML = " <div id='' style=''> Payment failed, please try again or go to M-Pesa Pay bill to 848450 account number 9039625995 amount KES " + mpesa_amount + ". Fill the mpesa transaction number below and click PAY. </div>";
                    } else {
                        document.getElementById("alert_info_kali").innerHTML = " <div id='' style=''>Payment failed. Try again or Call 0728561783 for help. M-PESA takes 2-3 minutes to process payments. </div>";
                    }
                    end_mpesa_process();
                    window.setTimeout(reload_payments, 5000); // 5 seconds
                }
            });
    }
}


function wait_for_payment(trace_numberr, mpesa_phone, mpesa_amount) {
    document.getElementById("alert_info_kali").innerHTML = "   A confirmation has been sent to phone no. " + mpesa_phone + ". Please authorise. ";
    $.ajax({
        type: "POST",
        url: "https://app.sendyit.com/biz/sendy/wait_for_pay",
        dataType: 'json',
        data: {
            trace_number: trace_numberr,
            old_rb: document.getElementById("old_rb").value
        }
    })
    .done(function(msg) {
        console.log(msg);
        if (msg.status === true) {
            document.getElementById("alert_info_kali").innerHTML = " Payment Successfull. Your account has been credited with KES " + msg.amount + ". ";
            end_mpesa_process();
            window.setTimeout(reload_payments, 3000); // 5 seconds
        } else {
            end_mpesa_process(); //reload_payments();
            document.getElementById("alert_info_kali").innerHTML = " <div id='' style=''> Payment failed,  try again or go to M-Pesa Pay bill: 848450 account number: 9039625995 amount KES " + mpesa_amount + ". Fill the mpesa transaction number below and click PAY. </div>";
            $("#mpesa_phone").val('');
            $("#mpesa_phone").attr("placeholder", "Mpesa Confirmation code");
        }
    });
}

function wait_for_payment(trace_numberr, mpesa_phone, mpesa_amount) {

    document.getElementById("alert_info_kali").innerHTML = "   A confirmation has been sent to phone no. " + mpesa_phone + ". Please authorise. ";

    $.ajax({
            type: "POST",
            url: "https://app.sendyit.com/biz/sendy/wait_for_pay",
            dataType: 'json',
            data: {
                trace_number: trace_numberr,
                old_rb: document.getElementById("old_rb").value
            }
        })
        .done(function(msg) {

            console.log(msg);

            if (msg.status === true) {

                document.getElementById("alert_info_kali").innerHTML = " Payment Successfull. Your account has been credited with KES " + msg.amount + ". ";
                end_mpesa_process();
                window.setTimeout(reload_payments, 3000); // 5 seconds

            } else {


                end_mpesa_process(); //reload_payments();


                document.getElementById("alert_info_kali").innerHTML = " <div id='' style=''> Payment failed,  try again or go to M-Pesa Pay bill: 848450 account number: 9039625995 amount KES " + mpesa_amount + ". Fill the mpesa transaction number below and click PAY. </div>";



                $("#mpesa_phone").val('');
                $("#mpesa_phone").attr("placeholder", "Mpesa Confirmation code");

            }

            //finally try to complet order
            //reload_payments();


        });
}

function start_mpesa_process() {
    console.log('start mpesa');
    document.getElementById('my_btny_2').setAttribute('onclick', '');
}

function end_mpesa_process() {
    console.log('end mpesa');
    document.getElementById('my_btny_2').setAttribute('onclick', 'initiate_mpesa()');
}